//
//  UINavigationController.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2015/10/28.
//  Copyright © 2015年 大口 尚紀. All rights reserved.
//

extension UINavigationController {
    override open var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return visibleViewController!.supportedInterfaceOrientations
    }
    open override var shouldAutorotate : Bool {
        return visibleViewController!.shouldAutorotate
    }
}
