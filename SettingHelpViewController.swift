//
//  SettingHelpViewController.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2016/03/06.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import UIKit
import GoogleMobileAds

class SettingHelpViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate,UIAlertViewDelegate, GADBannerViewDelegate {
    
    @IBOutlet var SettingTable: UITableView!
    
    var helpMasterCollection = HelpMasterManager.sharedInstance
    var helpMaster: Array<HelpMaster> = []

    var helpCollection = HelpManager.sharedInstance
    var help:Dictionary<Int, Array<Help>> = [:]
    var nextTitle: String!
    var nextMessage: String!

    var table_data = Array<TableData>()
    let ref = Firebase(url: backendUrl)
    
    struct TableData
    {
        var section:Bool = false
        var title = String()
        var message = String()
        init(){}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SVProgressHUD.show(withStatus: "Loading...", maskType: SVProgressHUDMaskType.black)
        
        let admobView = GADBannerView(adSize:kGADAdSizeSmartBannerPortrait)
        addHeight = (self.view.frame.width * admobView.frame.height)/admobView.frame.width
        admobView.frame.size = CGSize(width: self.view.frame.width, height: addHeight)
        admobView.frame.origin = CGPoint(x: 0, y: self.view.frame.height-addHeight)
        
        admobView.adUnitID = "ca-app-pub-8256083710740545/3348041715"
        admobView.delegate = self
        admobView.rootViewController = self
        let admobRequest:GADRequest = GADRequest()
        
        if AdMobTest {
            if SimulatorTest {
                admobRequest.testDevices = [kGADSimulatorID]
            }
            else {
                admobRequest.testDevices = [TEST_DEVICE_ID]
            }
            
        }
        admobView.load(admobRequest)

        let v:UIView = UIView(frame: CGRect.zero)
        v.backgroundColor = UIColor.clear
        self.SettingTable.tableFooterView = v
        
        self.SettingTable.separatorInset = UIEdgeInsets.zero;
        SettingTable.dataSource = self
        SettingTable.delegate = self
        
        self.table_data = Array<TableData>()
        var new_elements:TableData = TableData()
        
        let callbackHelpMaster = { () -> Void in
            self.helpMaster = self.helpMasterCollection.helpMasters
            
            var masters: Array<Int> = []
            for helpMaster in self.helpMaster {
                masters.append(helpMaster.id)
            }
            

            let callbackHelp = { () -> Void in
                self.help = self.helpCollection.helps
                let lanArr = Locale.preferredLanguages[0].components(separatedBy: "-")
                var indexNo = 0;
                for (_, help) in self.help {
                    new_elements = TableData()
                    if(Locale.preferredLanguages[0] == "ja-JP" || lanArr.first == "ja"){
                        new_elements.title = self.helpMaster[indexNo].title
                    }else{
                        new_elements.title = self.helpMaster[indexNo].title_en
                    }
                    new_elements.section = true
                    self.table_data.append(new_elements)

                    
                    for sub_help in help{
                        new_elements = TableData()
                        if(Locale.preferredLanguages[0] == "ja-JP" || lanArr.first == "ja"){
                            new_elements.title = sub_help.title
                            new_elements.message = sub_help.message
                        }else{
                            new_elements.title = sub_help.title_en
                            new_elements.message = sub_help.message_en
                        }
                        new_elements.section = false
                        self.table_data.append(new_elements)
                    }
                    indexNo += 1
                }
                self.view.addSubview(admobView)
                self.SettingTable.reloadData()
                SVProgressHUD.dismiss()
            }
            self.helpCollection.getList(callbackHelp)
        }
        self.helpMasterCollection.getList(callbackHelpMaster)
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(table_data[indexPath.row].section == false){
            let cell = SettingTable.dequeueReusableCell(withIdentifier: "HelpTableViewCell", for: indexPath) as! HelpTableViewCell
            cell.TitleLabel.text = table_data[indexPath.row].title
            cell.TitleLabel.textColor = UIColor(hex: "#222831")
            return cell
        }else{
            let cell = SettingTable.dequeueReusableCell(withIdentifier: "CustomHeaderCell", for: indexPath) as! CustomHeaderCell
            cell.headerLabel.text = table_data[indexPath.row].title
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.headerLabel.textColor = UIColor(hex: "#222831")
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection    section: Int) -> Int
    {
        return table_data.count
    }
    
    func tableView(_ table: UITableView, didSelectRowAt indexPath:IndexPath) {
        SettingTable?.deselectRow(at: indexPath, animated: true)
        self.nextTitle = self.table_data[indexPath.row].title
        self.nextMessage = self.table_data[indexPath.row].message
        performSegue(withIdentifier: "helpDetailSegue", sender: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "helpDetailSegue") {
            let nextVC: HelpDetailViewController = segue.destination as! HelpDetailViewController
            nextVC.helpTitle = self.nextTitle
            nextVC.helpMessage = self.nextMessage
        }
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        if(table_data[indexPath.row].section == false){
        //            return
        //        }else{
        //            return 60
        //        }
        return 55
    }
    
    //    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    //    {
    //        return table_data.count
    //    }
    
    
    //    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //        return 70.0
    //    }
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        let orientation: UIInterfaceOrientationMask = [UIInterfaceOrientationMask.portrait]
        return orientation
    }
    
    //指定方向に自動的に変更するか？
    override var shouldAutorotate : Bool{
        return true
    }
    
    @IBAction func closeView(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: {})
    }
    override func viewDidAppear(_ animated: Bool) {
        self.SettingTable.reloadData()
        self.SettingTable.separatorInset = UIEdgeInsets.zero;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let nv = navigationController {
            // 表示
            nv.setNavigationBarHidden(false, animated: true)
            // タイトルを設定する.
            self.navigationItem.title = NSLocalizedString("Help",comment: "")
            //            navHeight = (self.navigationController?.navigationBar.frame.size.height)!
        }
    }

}
