//
//  FavoriteViewController.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2016/02/21.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import UIKit

protocol FavoriteViewControllerDelegate {
    func completeLogin()
}

class FavoriteViewController: UIViewController, FavoriteViewControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
    @IBOutlet weak var favoriteImage: UIImageView!
    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var expLabel: UILabel!
    
    let subCateogryCollection = SubCategoryManager.sharedInstance
    var myCollectionView : UICollectionView!
    var subCategory: Array<SubCategory> = []
    
    var favoriteCollection = FavoriteManager.sharedInstance
    var favorite: Array<Favorite> = []
    var Ids: Array<Int> = []
    var navHeight:CGFloat = 0.0
    let ref = Firebase(url: backendUrl)
    var selSubCategoryId: Int!
    var selSubCategoryName:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.favoriteImage.image = UIImage(named: "fav3.jpeg")! as UIImage
        self.startLabel.isUserInteractionEnabled=true

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        for view in self.tabBarController!.view.subviews {
            if(view.tag == 123456789){
                view.frame.origin = CGPoint(x: 0, y: self.view.frame.height-view.frame.height-self.tabBarController!.tabBar.frame.size.height)
            }
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    override func viewWillAppear(_ animated: Bool) {
        let nv = navigationController
        if nv != nil{
            navHeight = (self.navigationController?.navigationBar.frame.size.height)!
        }

//        if(self.ref.authData != nil){
            SVProgressHUD.show(withStatus: "Loading...", maskType: SVProgressHUDMaskType.black)
            let callbackFavorite = { () -> Void in
                self.favorite = self.favoriteCollection.favorites
                if(self.favorite.count < 1){
                    if(nv != nil){
                        nv!.setNavigationBarHidden(true, animated: true)
                    }
                    self.dispNoFavorite()
                }else{
                    for favorite in self.favorite{
                        self.Ids = favorite.sub
                    }
                    if(nv != nil){
                        nv!.setNavigationBarHidden(false, animated: true)
                        // タイトルを設定する.
                        self.navigationItem.title = NSLocalizedString("wishlists",comment: "")
                    }
                    self.dispFavorite()
                }
                SVProgressHUD.dismiss()
            }
            self.favoriteCollection = FavoriteManager.sharedInstance
            self.favoriteCollection.getFavlist(callbackFavorite,uid:guuid)
//        }else{
//            self.dispLogin()
//            // 表示
//            if(nv != nil){
//                nv!.setNavigationBarHidden(true, animated: true)
//            }
//        }
    }
    
    
    /*
    Cellが選択された際に呼び出される
    */
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(self.subCategory[indexPath.row].state == 2){
            
            var name = self.subCategory[indexPath.row].name_sub
            let lanArr = Locale.preferredLanguages[0].components(separatedBy: "-")
            if(Locale.preferredLanguages[0] == "ja-JP" || lanArr.first == "ja"){
                name = self.subCategory[indexPath.row].name
            }
            self.selSubCategoryId = self.subCategory[indexPath.row].sub_ctegory_id
            self.selSubCategoryName = name
            performSegue(withIdentifier: "favoriteListViewSegue", sender: nil)
        }
    }
    
    /*
    Cellの総数を返す
    */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.subCategory.count
    }
    
    /*
    Cellに値を設定する
    */
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : CustomUICollectionViewFavoriteCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCell" + String(indexPath.row), for: indexPath) as! CustomUICollectionViewFavoriteCell
        let url = strageUrl + self.subCategory[indexPath.row].image
        if(cell.textLabel.attributedText == nil){
            loadImage(url, forImageView: cell.imageView)
            
            if(self.subCategory[indexPath.row].state == 1){
                cell.imageView.alpha = 0.5
                cell.textLabel.alpha = 0.2
                cell.readyLabel.text =  NSLocalizedString("UnderConstruction",comment: "")
                cell.readyLabel.textColor = UIColor.white
                cell.readyLabel.font = UIFont.systemFont(ofSize: 25)
                cell.readyLabel.alpha = 1.0
                cell.readyLabel.textAlignment = NSTextAlignment.center
            }

            var categoryName = NSMutableAttributedString(string: self.subCategory[indexPath.row].name_sub)
            let lanArr = Locale.preferredLanguages[0].components(separatedBy: "-")
            if(Locale.preferredLanguages[0] == "ja-JP" || lanArr.first == "ja"){
                categoryName = NSMutableAttributedString(string: self.subCategory[indexPath.row].name)
            }
            //categoryName.addAttributes([NSKernAttributeName:  -2], range: NSMakeRange(0, countStr))
            
            cell.textLabel.attributedText = categoryName
            
            if((self.subCategory[indexPath.row].type) != nil){
                if self.subCategory[indexPath.row].type == 1 {
                    cell.typeLabel.text = NSLocalizedString("basic",comment: "")
                    cell.typeLabel.backgroundColor = UIColor(red: 238.0/255.0, green: 238.0/255.0, blue: 238.0/255.0, alpha:0.50)
                    cell.typeLabel.textColor = UIColor(hex: "#222831")
                }else if self.subCategory[indexPath.row].type == 2 {
                    cell.typeLabel.text = NSLocalizedString("new",comment: "")
                    cell.typeLabel.backgroundColor = UIColor(red: 238.0/255.0, green: 238.0/255.0, blue: 238.0/255.0, alpha:0.50)
                }
            }
            
            if(self.subCategory[indexPath.row].licence != nil){
                let licence = NSMutableAttributedString(string: self.subCategory[indexPath.row].licence)
                cell.licenceLabel.attributedText = licence
                if let text = cell.textLabel.text {
                    let line:CGFloat = CGFloat(2-self.lineNumber(cell.textLabel, text: text))
                    cell.licenceLabel.layer.position.y = cell.licenceLabel.layer.position.y-(line*14)
                }
            }
        }else{
            
            var categoryName = NSMutableAttributedString(string: self.subCategory[indexPath.row].name_sub)
            let lanArr = Locale.preferredLanguages[0].components(separatedBy: "-")
            if(Locale.preferredLanguages[0] == "ja-JP" || lanArr.first == "ja"){
                categoryName = NSMutableAttributedString(string: self.subCategory[indexPath.row].name)
            }
            if(cell.textLabel.attributedText != categoryName){
                loadImage(url, forImageView: cell.imageView)
                cell.textLabel.attributedText = categoryName
                
                if((self.subCategory[indexPath.row].type) != nil){
                    if self.subCategory[indexPath.row].type == 1 {
                        cell.typeLabel.text = NSLocalizedString("basic",comment: "")
                        cell.typeLabel.backgroundColor = UIColor(red: 238.0/255.0, green: 238.0/255.0, blue: 238.0/255.0, alpha:0.50)
                        cell.typeLabel.textColor = UIColor(hex: "#222831")
                    }else if self.subCategory[indexPath.row].type == 2 {
                        cell.typeLabel.text = NSLocalizedString("new",comment: "")
                        cell.typeLabel.backgroundColor = UIColor(red: 238.0/255.0, green: 238.0/255.0, blue: 238.0/255.0, alpha:0.50)
                    }
                }
                
                if(self.subCategory[indexPath.row].licence != nil){
                    let imageWidth =  self.view.bounds.width
                    let imageHeight =  self.view.bounds.width/3.3
                    cell.licenceLabel.frame = CGRect(x: imageWidth/28.2, y: cell.textLabel.layer.position.y+25, width: imageWidth-(imageWidth/28.2), height: imageHeight/13)
                    
                    let licence = NSMutableAttributedString(string: self.subCategory[indexPath.row].licence)
                    cell.licenceLabel.attributedText = licence
                    if let text = cell.textLabel.text {
                        let line:CGFloat = CGFloat(2-self.lineNumber(cell.textLabel, text: text))
                        cell.licenceLabel.layer.position.y = cell.licenceLabel.layer.position.y-(line*14)
                    }
                }

            }
        }
        //        let countStr = self.subCategory[indexPath.row].name.characters.count
        return cell
    }
    
    func lineNumber(_ label: UILabel, text: String) -> Int {
        let oneLineRect  =  "a".boundingRect(with: label.bounds.size, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: label.font], context: nil)
        let boundingRect = text.boundingRect(with: label.bounds.size, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: label.font], context: nil)
        
        return Int(boundingRect.height / oneLineRect.height)
    }

    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        for touch: UITouch in touches {
            let tag = touch.view!.tag
            switch tag {
            case 100004:
                self.tapStartLabel()
            default:
                break
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "LoginSegue") {
            let vc = segue.destination as! LoginViewController
            vc.delegate = self
        }else if(segue.identifier == "favoriteListViewSegue") {
            let nextVC: FavoriteListViewController = segue.destination as! FavoriteListViewController
            nextVC.subCategoryId = self.selSubCategoryId
            nextVC.tabHeight = self.tabBarController!.tabBar.frame.size.height
            nextVC.subCategoryName = self.selSubCategoryName
        }
    }
    
    func changeTab(){
        let topColor = UIColor(red:1.0, green:1.0, blue:1.0, alpha:1)
        //グラデーションの開始色
        let bottomColor = UIColor(red:1.0, green:1.0, blue:1.0, alpha:0.8)
        //        let bottomColor = UIColor(red:0.54, green:0.74, blue:0.74, alpha:1)
        
        //グラデーションの色を配列で管理
        let gradientColors: [CGColor] = [topColor.cgColor, bottomColor.cgColor]
        //グラデーションレイヤーを作成
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        
        //グラデーションの色をレイヤーに割り当てる
        gradientLayer.colors = gradientColors
        //グラデーションレイヤーをスクリーンサイズにする
        gradientLayer.frame = self.view.bounds
        
        //グラデーションレイヤーをビューの一番下に配置
        
        self.view.layer.insertSublayer(gradientLayer, at: 0)
        let toView = (self.tabBarController?.childViewControllers[0])!.view as UIView
        
        toView.alpha = 0
        UIView.animate(
            withDuration: 0.27,
            delay:0,
            options:UIViewAnimationOptions.curveEaseOut,
            animations: {() -> Void in
                toView.alpha = 1
                //self.arrow.center = CGPoint(x:pt.x, y:pt.y - height/2)
            },
            completion:{ _ in
                gradientLayer.removeFromSuperlayer()
        })
        self.tabBarController?.selectedViewController = (self.tabBarController?.childViewControllers[0])! as UIViewController
    }

    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        let orientation: UIInterfaceOrientationMask = [UIInterfaceOrientationMask.portrait]
        return orientation
    }
    
    //指定方向に自動的に変更するか？
    override var shouldAutorotate : Bool{
        return true
    }

    func dispFavorite(){
        self.favoriteImage.isHidden = true
        self.messageLabel.isHidden = true
        self.startLabel.isHidden = true
        
        let callbackCategory = { () -> Void in
            self.subCategory = []
            for id in self.Ids{
                for tempSub in self.subCateogryCollection.subCategories{
                    if(id == tempSub.sub_ctegory_id){
                        self.subCategory.append(tempSub)
                        break;
                    }
                }
            }
            
            if(self.view.viewWithTag(99999) == nil) {
                // CollectionViewを生成.
                let tabHeight = self.tabBarController!.tabBar.frame.size.height
                let staHeight = UIApplication.shared.statusBarFrame.height
                
                let imageWidth = self.view.bounds.width
                let imageHeight = self.view.bounds.width/3.3
                
                
                // CollectionViewのレイアウトを生成.
                let layout = UICollectionViewFlowLayout()
                
                
                // Cell一つ一つの大きさ.
                layout.itemSize = CGSize(width: imageWidth, height: imageHeight-5)
                
                // Cellのマージン.
                let navBarHeight = self.navigationController?.navigationBar.frame.size.height
                layout.sectionInset = UIEdgeInsetsMake(0, 0, navBarHeight! + tabHeight + staHeight + addHeight,0)
                //アイテム同士のマージン
                layout.minimumInteritemSpacing = 0.0
                //セクションとアイテムのマージン
                layout.minimumLineSpacing = 0.0
                // セクション毎のヘッダーサイズ.
                layout.headerReferenceSize = CGSize(width: 0,height: 0)
                self.myCollectionView = UICollectionView(frame: CGRect(x: 0, y: UIApplication.shared.statusBarFrame.height+self.navHeight, width: self.view.bounds.width,height: self.view.bounds.height), collectionViewLayout: layout)
                
                //for(var i=0; i < self.subCategory.count; i++){
                
                if(self.subCategory.count > 0 ){
                    for i in 0...self.subCategory.count-1 {
                        self.myCollectionView.register(CustomUICollectionViewFavoriteCell.self, forCellWithReuseIdentifier: "MyCell" + String(i))
                    }
                }
                
                self.myCollectionView.delegate = self
                self.myCollectionView.dataSource = self
                self.myCollectionView.setContentOffset(CGPoint(x: 0, y: self.myCollectionView.contentSize.height - self.myCollectionView.frame.size.height+20), animated: false)
                let colorKey = UIColor(hex: "#EEEEEE")
                
                self.myCollectionView.backgroundColor = colorKey
                self.myCollectionView.tag = 99999
                self.view.addSubview(self.myCollectionView)
            }else{
                //for(var i=0; i < self.subCategory.count; i++){
                for i in 0...self.subCategory.count-1 {
                    self.myCollectionView.register(CustomUICollectionViewFavoriteCell.self, forCellWithReuseIdentifier: "MyCell" + String(i))
                }
                self.myCollectionView.reloadData()
            }
        }
        
        self.subCateogryCollection.getSubCategoriesFromIds(callbackCategory, Ids: self.Ids);
    }

    func dispNoFavorite(){
        self.favoriteImage.isHidden = false
        self.messageLabel.isHidden = false
        self.startLabel.isHidden = false
        self.favoriteImage.image = UIImage(named: "fav3.jpeg")! as UIImage
        
        self.expLabel.text = NSLocalizedString("WishListsexp",comment: "")
        self.messageLabel.text = NSLocalizedString("NoWishLists",comment: "")
        self.startLabel.text = NSLocalizedString("Startmettrow",comment: "")
        if(self.view.viewWithTag(99999) != nil) {
            let tempCol = self.view.viewWithTag(99999)
            tempCol!.removeFromSuperview()
        }
    }

    func dispLogin(){
        self.favoriteImage.isHidden = false
        self.messageLabel.isHidden = false
        self.startLabel.isHidden = false
        self.favoriteImage.image = UIImage(named: "fav2.jpeg")! as UIImage
        self.messageLabel.text = NSLocalizedString("CreateAccountorLogin",comment: "")
        self.startLabel.text = NSLocalizedString("Login2",comment: "")
        if(self.view.viewWithTag(99999) != nil) {
            let tempCol = self.view.viewWithTag(99999)
            tempCol!.removeFromSuperview()
        }
    }
    
    

    func tapStartLabel(){
//        if(self.ref.authData != nil){
            self.changeTab()
//        }else{
//            performSegueWithIdentifier("LoginSegue", sender: nil)
//        }
    }
    func completeLogin() {
        let callbackFavorite = { () -> Void in
            self.favorite = self.favoriteCollection.favorites
            if(self.favorite.count < 1){
                self.dispNoFavorite()
            }else{
                self.dispFavorite()
            }
        }
        
        self.favoriteCollection = FavoriteManager.sharedInstance
        self.favoriteCollection.getFavlist(callbackFavorite,uid:(self.ref?.authData.uid)!)
    }
    
    func loadImage(_ url:String, forImageView: UIImageView) {
        // load image
        let image_url:String = url
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
            let url:URL = URL(string:image_url)!
            let data:Data = try! Data(contentsOf: url)
            let placeholder = UIImage(data: data)!
            
            let categoryImage = ImageTool.cropThumbnailImage(placeholder, w: Int(self.view.bounds.width), h:Int(self.view.bounds.width/3.3+2))
            let ciImage:CIImage = CIImage(image:categoryImage)!
            let ciFilter:CIFilter = CIFilter(name: "CIGammaAdjust")!
            ciFilter.setValue(ciImage, forKey: kCIInputImageKey)
            ciFilter.setValue(1.3, forKey: "inputPower")
            let ciContext:CIContext = ObjcUtility.cicontext(options: nil)
            let cgimg:CGImage = ciContext.createCGImage(ciFilter.outputImage!, from:ciFilter.outputImage!.extent)!
            
            //image2に加工後のUIImage
            let newImage:UIImage = UIImage(cgImage: cgimg, scale: 1.0, orientation:UIImageOrientation.up)
            
            // update ui
            DispatchQueue.main.async {
                forImageView.image = newImage
                forImageView.fadeIn(0.15)
            }
        }
    }
}
