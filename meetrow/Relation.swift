//
//  line.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2015/09/01.
//  Copyright (c) 2015年 大口 尚紀. All rights reserved.
//

import Foundation
import UIKit

class Relation: NSObject {
    var from_id: Int!
    var from_jp_name: String!
    var from_en_name: String!
    var to_id: Int!
    var to_jp_name: String!
    var to_en_name: String!
    var direct: Int!
    var color: String!
    var jp: Bool!
    
    init(from_id: Int, from_jp_name: String, from_en_name: String, to_id: Int, to_jp_name: String, to_en_name: String, direct: Int, color: String, jp: Bool) {
        self.from_id=from_id
        self.from_jp_name=from_jp_name
        self.from_en_name=from_en_name
        self.to_id=to_id
        self.to_jp_name=to_jp_name
        self.to_en_name=to_en_name
        self.direct=direct
        self.color=color
        self.jp=jp
    }
    
    func saveTweet() {
        //後ほどParseのdbに保存する処理を書く
    }
}