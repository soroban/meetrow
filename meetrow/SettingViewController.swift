//
//  SettingViewController.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2016/02/05.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit
import Fabric
import TwitterKit

class SettingViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate,UIAlertViewDelegate/*, TabBarDelegate*/ {

    @IBOutlet var SettingTable: UITableView!
    var table_data = Array<TableData>()
    let ref = Firebase(url: backendUrl)
    
    struct TableData
    {
        var section:Bool = false
        var data = String()
        var icon = String()
        init(){}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // CollectionViewのレイアウトを生成.
        let tabHeight = self.tabBarController!.tabBar.frame.size.height
        let staHeight = UIApplication.shared.statusBarFrame.height
        // Cellのマージン.
        SettingTable.contentInset = UIEdgeInsetsMake(0, 0, tabHeight + staHeight + addHeight-(self.navigationController?.navigationBar.frame.size.height)!,0)
        SettingTable.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, tabHeight + staHeight + addHeight-(self.navigationController?.navigationBar.frame.size.height)!,0)
        
        if(fromNotification){
            performSegue(withIdentifier: "SettingNotificationSegue",sender: nil)
            fromNotification = false
        }

        // Setup delegates
        // Attempt to sign in silently, this will succeed if
        // the user has recently been authenticated
        
        //self.SettingTable.contentOffset = CGPointMake(0, -20);
        self.SettingTable.separatorInset = UIEdgeInsets.zero;
        let v:UIView = UIView(frame: CGRect.zero)
        v.backgroundColor = UIColor.clear
        self.SettingTable.tableFooterView = v

        SettingTable.dataSource = self
        SettingTable.delegate = self
        self.setData()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(indexPath.row == 0){
            let cell = SettingTable.dequeueReusableCell(withIdentifier: "MainSettingCell", for: indexPath) as! MainSettingCell
            cell.mainLabel.text = table_data[indexPath.row].data
            cell.mainLabel.textColor = UIColor(hex: "#222831")
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }else if(table_data[indexPath.row].section == false){
            let cell = SettingTable.dequeueReusableCell(withIdentifier: "SetingTableViewCell", for: indexPath) as! SettingTableViewCell
            cell.TitleLabel.text = table_data[indexPath.row].data
            cell.TitleLabel.textColor = UIColor(hex: "#222831")
            cell.settingImage.image = UIImage(named:table_data[indexPath.row].icon)
            return cell
        }else{
            let cell = SettingTable.dequeueReusableCell(withIdentifier: "CustomHeaderCell", for: indexPath) as! CustomHeaderCell
            cell.headerLabel.text = table_data[indexPath.row].data
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.headerLabel.textColor = UIColor(hex: "#222831")
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection    section: Int) -> Int
    {
        return table_data.count
    }
    
    func tableView(_ table: UITableView, didSelectRowAt indexPath:IndexPath) {
        
        SettingTable?.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 2 {
            // SubViewController へ遷移するために Segue を呼び出す
            performSegue(withIdentifier: "SettingNotificationSegue",sender: nil)
        }else if indexPath.row == 4 {
            // SubViewController へ遷移するために Segue を呼び出す
            performSegue(withIdentifier: "SettingBackgroundSegue",sender: nil)
        }
//        else if indexPath.row == 10 {
//            // SubViewController へ遷移するために Segue を呼び出す
//            performSegueWithIdentifier("SettingPushSegue",sender: nil)
//        }
        else if indexPath.row == 6 {
            let itunesURL:String = "itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=1099070037"
            let url = URL(string:itunesURL)
            let app:UIApplication = UIApplication.shared
            if #available(iOS 10.0, *) {
                app.open(url!, options: [:], completionHandler: nil)
            } else {
                app.openURL(url!)
            }
        }
        else if indexPath.row == 7 {
            // SubViewController へ遷移するために Segue を呼び出す
            performSegue(withIdentifier: "SettingHelpSegue",sender: nil)
        }else if indexPath.row == 8 {
            // SubViewController へ遷移するために Segue を呼び出す
            performSegue(withIdentifier: "SettingLicenceSegue",sender: nil)
        }else if indexPath.row == 10 {
//            if objc_getClass("UIAlertController") != nil {
                let alertController:MyUIAlertController = MyUIAlertController()
//                alertController.preferredStyle =  UIAlertControllerStyle.ActionSheet
                
                let logoutAction = UIAlertAction(title: NSLocalizedString("LogOut",comment: ""), style: .destructive) {
                    action in self.logOut()
                }
                
                let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel",comment: ""), style: .cancel, handler:nil)
                alertController.addAction(logoutAction)
                alertController.addAction(cancelAction)
            
                alertController.popoverPresentationController?.sourceView = self.view
                alertController.popoverPresentationController?.sourceRect =  CGRect(x: self.view.bounds.size.width, y: self.view.bounds.size.height, width: 1.0, height: 1.0)
                //alertController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.Up
            
                present(alertController, animated: true, completion: nil)
//            }else{
//                let av = UIAlertView(title: NSLocalizedString("id00011",comment: ""), message: "", delegate: self, cancelButtonTitle: nil,otherButtonTitles: "OK")
//                av.show()
//            }
        }
    }
    
    func logOut(){
        let store = Twitter.sharedInstance().sessionStore
        if(GIDSignIn.sharedInstance().hasAuthInKeychain() == true){
            GIDSignIn.sharedInstance().signOut()
            self.ref?.unauth()
        }else if(FBSDKAccessToken.current() != nil){
            let facebookLogin = FBSDKLoginManager()
            facebookLogin.logOut()
            self.ref?.unauth()
        }else if Twitter.sharedInstance().sessionStore.session() != nil{
            let userID = store.session()!.userID
            store.logOutUserID(userID)
            self.ref?.unauth()
        }else{
            self.ref?.unauth()
        }
        
        self.setData()
        self.SettingTable.reloadData()
        self.SettingTable.separatorInset = UIEdgeInsets.zero;
        
        
        let topColor = UIColor(red:1.0, green:1.0, blue:1.0, alpha:1)
        //グラデーションの開始色
        let bottomColor = UIColor(red:1.0, green:1.0, blue:1.0, alpha:0.8)
        //        let bottomColor = UIColor(red:0.54, green:0.74, blue:0.74, alpha:1)
        
        //グラデーションの色を配列で管理
        let gradientColors: [CGColor] = [topColor.cgColor, bottomColor.cgColor]
        //グラデーションレイヤーを作成
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        
        //グラデーションの色をレイヤーに割り当てる
        gradientLayer.colors = gradientColors
        //グラデーションレイヤーをスクリーンサイズにする
        gradientLayer.frame = self.view.bounds
        
        //グラデーションレイヤーをビューの一番下に配置
        
        self.view.layer.insertSublayer(gradientLayer, at: 0)

        let toView = (self.tabBarController?.childViewControllers[0])!.view as UIView
        
        toView.alpha = 0
        UIView.animate(
            withDuration: 0.27,
            delay:0,
            options:UIViewAnimationOptions.curveEaseOut,
            animations: {() -> Void in
                toView.alpha = 1
                //self.arrow.center = CGPoint(x:pt.x, y:pt.y - height/2)
            },
            completion:{ _ in
                gradientLayer.removeFromSuperlayer()
        })
        self.tabBarController?.selectedViewController = (self.tabBarController?.childViewControllers[0])! as UIViewController
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if(table_data[indexPath.row].section == false){
//            return
//        }else{
//            return 60
//        }
        return 55
    }
    
//    func numberOfSectionsInTableView(tableView: UITableView) -> Int
//    {
//        return table_data.count
//    }
    
    
//    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 70.0
//    }
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        let orientation: UIInterfaceOrientationMask = [UIInterfaceOrientationMask.portrait]
        return orientation
    }
    
    //指定方向に自動的に変更するか？
    override var shouldAutorotate : Bool{
        return true
    }
    override func viewWillAppear(_ animated: Bool) {
        if let nv = navigationController {
            // 隠す
            nv.setNavigationBarHidden(true, animated: animated)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
//        self.SettingTable.layer.position = CGPoint(x: self.view.center.x, y:self.view.center.y-20)
        if let nv = navigationController {
            // 隠す
            nv.setNavigationBarHidden(false, animated: animated)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.setData()
        self.SettingTable.reloadData()
        self.SettingTable.separatorInset = UIEdgeInsets.zero;
    }
    func setData(){
        self.table_data = Array<TableData>()
        var new_elements:TableData
        
        new_elements = TableData()
        new_elements.data = NSLocalizedString("Settings",comment: "")
        new_elements.section = true
        self.table_data.append(new_elements)

        new_elements = TableData()
        new_elements.data = NSLocalizedString("Notifications",comment: "")
        new_elements.section = true
        new_elements.icon = ""
        self.table_data.append(new_elements)
        
        new_elements = TableData()
        new_elements.data = NSLocalizedString("Notifications",comment: "")
        new_elements.section = false
        new_elements.icon = "st_about"
        self.table_data.append(new_elements)
        
        new_elements = TableData()
        new_elements.data = NSLocalizedString("YoutubePlayear",comment: "")
        new_elements.section = true
        self.table_data.append(new_elements)
        
//        new_elements = TableData()
//        new_elements.data = NSLocalizedString("AutoPlay",comment: "")
//        new_elements.section = false
//        new_elements.icon = "st_music"
//        self.table_data.append(new_elements)
        
        new_elements = TableData()
        new_elements.data = NSLocalizedString("PlayYouTubeinBackground",comment: "")
        new_elements.section = false
        new_elements.icon = "st_script"
        self.table_data.append(new_elements)
        
        
        
        new_elements = TableData()
        new_elements.data = NSLocalizedString("About",comment: "")
        new_elements.section = true
        self.table_data.append(new_elements)
        
        //        new_elements = TableData()
        //        new_elements.data = NSLocalizedString("Writeareview",comment: "")
        //        new_elements.icon = "st_good"
        //        new_elements.section = false
        //        table_data.append(new_elements)
        
        new_elements = TableData()
        new_elements.data = NSLocalizedString("Writeareview",comment: "")
        new_elements.section = false
        new_elements.icon = "st_good.png"
        self.table_data.append(new_elements)

        new_elements = TableData()
        new_elements.data = NSLocalizedString("Help",comment: "")
        new_elements.section = false
        new_elements.icon = "st_question"
        self.table_data.append(new_elements)
        
        new_elements = TableData()
        new_elements.data = NSLocalizedString("License",comment: "")
        new_elements.section = false
        new_elements.icon = "st_licence"
        self.table_data.append(new_elements)
        
        //        print(FBSDKAccessToken.currentAccessToken())
        

//        new_elements = TableData()
//        new_elements.data = NSLocalizedString("PushNotificationSettings",comment: "")
//        new_elements.section = false
//        new_elements.icon = "st_push"
//        self.table_data.append(new_elements)

        if(self.ref?.authData != nil){
        //if (FBSDKAccessToken.currentAccessToken() != nil || GIDSignIn.sharedInstance().hasAuthInKeychain() == true || Twitter.sharedInstance().sessionStore.session() != nil){
            //        new_elements = TableData()
            //        new_elements.data = NSLocalizedString("Facebook",comment: "")
            //        new_elements.section = false
            //        new_elements.icon = "st_facebook"
            //        table_data.append(new_elements)
            //
            //        new_elements = TableData()
            //        new_elements.data = NSLocalizedString("Twitter",comment: "")
            //        new_elements.section = false
            //        new_elements.icon = "st_twitter"
            //        table_data.append(new_elements)
            //
            //        new_elements = TableData()
            //        new_elements.data = NSLocalizedString("Google",comment: "")
            //        new_elements.section = false
            //        new_elements.icon = "st_google"
            //        table_data.append(new_elements)
            new_elements = TableData()
            new_elements.data = NSLocalizedString("Account",comment: "")
            new_elements.section = true
            self.table_data.append(new_elements)
            
            new_elements = TableData()
            new_elements.data = NSLocalizedString("LogOut",comment: "")
            new_elements.section = false
            new_elements.icon = "st_logout"
            self.table_data.append(new_elements)
        }
    }
    
//    func didSelectTab(myTabBarController: MyTabBarController) {
//        viewDidLoad()
//    }
    
    
    func signIn(_ signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!,
        withError error: NSError!) {
    }
    // Implement the required GIDSignInDelegate methods
    // Unauth when disconnected from Google
    func signIn(_ signIn: GIDSignIn!, didDisconnectWithUser user:GIDGoogleUser!,
        withError error: NSError!) {
    }
    
    
}

class MyUIAlertController : UIAlertController {
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return [UIInterfaceOrientationMask.portrait,UIInterfaceOrientationMask.portraitUpsideDown]
    }
    
    override var shouldAutorotate : Bool {
        return false
    }
}
