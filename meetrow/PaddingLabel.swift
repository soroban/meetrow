//
//  PaddingLabel.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2015/09/01.
//  Copyright (c) 2015年 大口 尚紀. All rights reserved.
//

class PaddingLabel: UILabel {
    
    // paddingの値
    let padding = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
    
    override func drawText(in rect: CGRect) {
        let newRect = UIEdgeInsetsInsetRect(rect, padding)
        super.drawText(in: newRect)
    }
    
    override var intrinsicContentSize : CGSize {
        var intrinsicContentSize = super.intrinsicContentSize
        intrinsicContentSize.height += padding.top + padding.bottom
        intrinsicContentSize.width += padding.left + padding.right
        return intrinsicContentSize
    }
}
