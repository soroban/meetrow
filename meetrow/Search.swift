//
//  Search.swift
//  Meetrow
//
//  Created by 大口 尚紀 on 2016/05/03.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import Foundation
import UIKit

class Search: NSObject {
    var id: Int!
    var keyword: Array<String>!
    
    init(id:Int, keyword: Array<String>) {
        self.id=id
        self.keyword=keyword
        
    }
}