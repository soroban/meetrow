//
//  HelpMaster.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2016/03/06.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import Foundation

class HelpMaster: NSObject {
    var id: Int!
    var title: String!
    var title_en: String!
    
    init(id: Int, title: String, title_en: String) {
        self.id=id
        self.title=title
        self.title_en=title_en
    }
    
    func saveTweet() {
        //後ほどParseのdbに保存する処理を書く
    }
}
