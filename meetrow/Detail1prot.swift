//
//  Detail1prot.swift
//  Meetrow
//
//  Created by 大口 尚紀 on 2016/05/20.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import Foundation
import UIKit

class Detail1prot: NSObject {
    var id: Int!
    var jp_name: String!
    var en_name: String!
    var day: Array<String>!
    var time: Array<String>!
    var stage: Array<String>!
    var jp: Bool!
    
    init(id:Int, jp_name: String, en_name: String, day: Array<String>?, time: Array<String>?, stage: Array<String>?, jp:Bool) {
        self.id = id
        self.jp_name = jp_name
        self.en_name = en_name
        self.day = day
        self.time = time
        self.stage = stage
        self.jp = jp
    }
}