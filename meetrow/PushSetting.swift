//
//  UserPushSetting.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2016/02/26.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import Foundation
import UIKit

class PushSetting: NSObject {
    var devicetoken: String!
    var uid: String!
    var language: String!
    var hourDiff: Int!
    var setting:Dictionary<String, Bool> = [:]
    
    init(devicetoken: String, uid: String,language:String, hourDiff:Int, setting:Dictionary<String, Bool>) {
        self.devicetoken=devicetoken
        self.uid=uid
        self.language=language
        self.hourDiff=hourDiff
        self.setting = setting
    }
    
    func saveTweet() {
        //後ほどParseのdbに保存する処理を書く
    }
}