//
//  RelationManager.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2016/01/26.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import UIKit

class RelationManager: NSObject {
    
    var relations: Array<Relation> = []
    static let sharedInstance = RelationManager()
    var myRootRef: Firebase!
    
    func getRelationFromId(_ callback: @escaping () -> Void, subCategoryId:Int, id:Int) {
        // Firebaseへ接続
        self.myRootRef = Firebase(url: backendUrl + "relation/relation_" + String(subCategoryId))
        self.myRootRef.queryOrdered(byChild: "from_id").queryEqual(toValue: id).observeSingleEvent(of: FEventType.value, with: { relations in
            self.relations = []
            for relation in relations?.children.allObjects as! [FDataSnapshot] {
                let value = relation.value as? NSDictionary
                if let from_id = value!["from_id"] as? Int,
                    let from_jp_name = value!["from_jp_name"] as? String,
                    let from_en_name = value!["from_en_name"] as? String,
                    let to_id = value!["to_id"] as? Int,
                    let to_jp_name = value!["to_jp_name"] as? String,
                    let to_en_name = value!["to_en_name"] as? String,
                    let direct = value!["direct"] as? Int,
                    let color = value!["color"] as? String,
                    let jp = value!["jp"] as? Bool
                {
                    let relation = Relation(from_id: from_id, from_jp_name: from_jp_name, from_en_name: from_en_name, to_id: to_id, to_jp_name: to_jp_name, to_en_name: to_en_name, direct: direct, color: color, jp:jp)
                    self.relations.append(relation)
                }
            }
            callback()
        })
    }
    
    func getRelationColor(_ callback: @escaping () -> Void, subCategoryId:Int, color:String) {
        // Firebaseへ接続
        self.myRootRef = Firebase(url: backendUrl + "relation/relation_" + String(subCategoryId))
        self.myRootRef.queryOrdered(byChild: "color").queryEqual(toValue: color).observeSingleEvent(of: FEventType.value, with: { relations in
            self.relations = []
            for relation in relations?.children.allObjects as! [FDataSnapshot] {
                let value = relation.value as? NSDictionary
                if let from_id = value!["from_id"] as? Int,
                    let from_jp_name = value!["from_jp_name"] as? String,
                    let from_en_name = value!["from_en_name"] as? String,
                    let to_id = value!["to_id"] as? Int,
                    let to_jp_name = value!["to_jp_name"] as? String,
                    let to_en_name = value!["to_en_name"] as? String,
                    let direct = value!["direct"] as? Int,
                    let color = value!["color"] as? String,
                    let jp = value!["jp"] as? Bool
                {
                    let relation = Relation(from_id: from_id, from_jp_name: from_jp_name, from_en_name: from_en_name, to_id: to_id, to_jp_name: to_jp_name, to_en_name: to_en_name, direct: direct, color: color, jp:jp)
                    self.relations.append(relation)
                }
            }
            callback()
        })
    }
    
}

