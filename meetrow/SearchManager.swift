//
//  SearchManager.swift
//  Meetrow
//
//  Created by 大口 尚紀 on 2016/05/03.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import UIKit

class SearchManager: NSObject {
    
    var  searches: Array<Search> = []
    static let sharedInstance = SearchManager()
    var myRootRef: Firebase!
    
    func getSearches(_ callback: @escaping () -> Void, subCategoryId:Int) {
        // Firebaseへ接続
        searches = []
        self.myRootRef = Firebase(url: backendUrl + "search/search_" + String(subCategoryId))
        self.myRootRef.observeSingleEvent(of: FEventType.value, with: { searches in
            for search in searches?.children.allObjects as! [FDataSnapshot] {
                let value = search.value as? NSDictionary
                if let keyword = value?["keyword"] as? Array<String>{
                        let lanArr = Locale.preferredLanguages[0].components(separatedBy: "-")
                        if(Locale.preferredLanguages[0] == "ja-JP" || lanArr.first == "ja"){
                            let search = Search(id: Int(search.key)!, keyword:keyword)
                            self.searches.append(search)
                        }else{
                            var newKeyword: [String] = []
                            for word in keyword {
                                var changeCheck = false
                                for code in word.unicodeScalars {
                                    if(self.checkKana(code) == true || self.checkHira(code) == true){
                                        changeCheck = true
                                    }
                                }
                                
//                                let hankaku = CharacterSet(range: NSMakeRange(0x20, 0x7e))
//                                let titleUniStr = word
                                if(changeCheck == false){
                                    for tStr in word.characters {
                                        let matched = self.matches(for: "^[a-zA-Z0-9!-/:-@¥[-`{-~]+$", in: String(tStr))
//                                        if !hankaku.contains(UnicodeScalar(tStr.value)) {
                                            changeCheck = true
//                                        }
                                    }
                                }
                                
                                if(changeCheck == false){
                                    newKeyword.append(word)
                                }
                            }
                            
                            let search = Search(id: Int(search.key)!, keyword:newKeyword)
                            self.searches.append(search)
                        }
                    }
            }
            callback()
        })
    }
    
    func checkHira(_ code: UnicodeScalar) -> Bool {
        switch code.value {
        case 0x3040...0x309f: return true
        default: return false
        }
    }
    
    func checkKana(_ code: UnicodeScalar) -> Bool {
        switch code.value {
        case 0x30a0...0x30ff: return true
        default: return false
        }
    }
    func matches(for regex: String, in text: String) -> [String] {
        
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let nsString = text as NSString
            let results = regex.matches(in: text, range: NSRange(location: 0, length: nsString.length))
            return results.map { nsString.substring(with: $0.range)}
        } catch let error {
            //print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
}
