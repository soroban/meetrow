//
//  UIColor.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2015/09/05.
//  Copyright (c) 2015年 大口 尚紀. All rights reserved.
//

import UIKit
import Foundation


func hexFromRGB(_ hex:String) -> (CGFloat,CGFloat,CGFloat,CGFloat) {
    var red: CGFloat   = 0.0
    var green: CGFloat = 0.0
    var blue: CGFloat  = 0.0
    var alpha: CGFloat = 1.0
    
    let index = hex.characters.index(hex.startIndex, offsetBy: 1)
    let hexCode = hex.substring(from: index)
    let scanner = Scanner(string: hexCode)
    var hexValue: CUnsignedLongLong = 0
    if scanner.scanHexInt64(&hexValue) {
        if hexCode.characters.count == 6 {
            red   = CGFloat((hexValue & 0xFF0000) >> 16) / 255.0
            green = CGFloat((hexValue & 0x00FF00) >> 8)  / 255.0
            blue  = CGFloat(hexValue & 0x0000FF) / 255.0
        } else if hexCode.characters.count == 8 {
            red   = CGFloat((hexValue & 0xFF000000) >> 24) / 255.0
            green = CGFloat((hexValue & 0x00FF0000) >> 16) / 255.0
            blue  = CGFloat((hexValue & 0x0000FF00) >> 8)  / 255.0
            alpha = CGFloat(hexValue & 0x000000FF)         / 255.0
        } else {
            print("err")
        }
    }
    
    return (red,green,blue,alpha)
}

extension UIColor {
    
    
    convenience init(hex: String = "") {
        var red: CGFloat  = 0.0
        var green: CGFloat = 0.0
        var blue: CGFloat  = 0.0
        var alpha: CGFloat = 1.0
        
        
        (red,green,blue,alpha) = hexFromRGB(hex)
        
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
}
