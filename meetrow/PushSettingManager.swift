//
//  UserPushSettingManager.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2016/02/26.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import Foundation

class PushSettingManager: NSObject {
    
    var pushSettings: Array<PushSetting> = []
    static let sharedInstance = PushSettingManager()
    
    var myRootRef: Firebase!
    var myRootRefSubC: Firebase!
    
    func getFromTokenAndUuid(_ callback: @escaping () -> Void, device_token:String, uuid:String) {
        // Firebaseへ接続
        self.myRootRef = Firebase(url: backendUrl + "push_setting")
        self.myRootRef.queryOrdered(byChild: "uuid").queryEqual(toValue: uuid).observeSingleEvent(of: FEventType.value, with: { push_settings in
            self.pushSettings = []
            if((push_settings?.children.allObjects.count)! > 0){
                for push_setting in push_settings?.children.allObjects as! [FDataSnapshot] {
                    let value = push_setting.value as? NSDictionary
                    let set_device_token: String = (value?["device_token"] as? String)!
                    if set_device_token == device_token {
                        if let uuid = value?["uuid"] as? String,
                            let device_token = value?["device_token"] as? String,
                            let language = value?["language"] as? String,
                            let hourDiff = value?["hourDiff"] as? Int,
                            let locsetting = value?["settings"] as? Dictionary<String, Bool>
                        {
                            let pushSetting = PushSetting(devicetoken: device_token, uid: uuid, language:language, hourDiff: hourDiff, setting: locsetting)
                            self.pushSettings.append(pushSetting)
                        }
                    }
                }
            }
            callback()
        })
    }

    func save(_ callback: @escaping () -> Void, device_token:String, uuid:String, updateSetting:Dictionary<String, Bool>) {
        self.myRootRef = Firebase(url: backendUrl + "push_setting")
        self.myRootRef.queryOrdered(byChild: "uuid").queryEqual(toValue: uuid).observeSingleEvent(of: FEventType.value, with: { push_settings in
            if((push_settings?.children.allObjects.count)! > 0){
                for push_setting in push_settings?.children.allObjects as! [FDataSnapshot] {
                    let value = push_setting.value as? NSDictionary
                    let set_device_token: String = (value?["device_token"] as? String)!
                    if set_device_token == device_token {
                        if let uuid = value?["uuid"] as? String,
                            let device_token = value?["device_token"] as? String
                            {
                                let newPostKey = push_setting.key!
                                // Create the data we want to update
                                
                                let languages = Locale.preferredLanguages
                                let language = languages.first! as String

                                let now = Date()
                                let formatter = DateFormatter()
                                formatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
                                let nowSt = formatter.string(from: now)
                                
                                let updatedUserData = [ "/\(newPostKey)": ["uuid": uuid , "device_token": device_token, "language": language, "hourDiff":self.getDateDiff(), "settings":updateSetting, "lastTime": nowSt]]
                                // Do a deep-path update
                                self.myRootRef.updateChildValues(updatedUserData, withCompletionBlock: { (error, ref) -> Void in
                                    if ((error) != nil) {
                                        print("Error updating data: \(error.debugDescription)")
                                    }
                                })
                            }
                        }else{
                            let newPostRef = self.myRootRef.child(byAppendingPath: "/").childByAutoId()
                            let newPostKey = newPostRef?.key!
                            // Create the data we want to update
                        
                            let languages = Locale.preferredLanguages
                            let language = languages.first! as String

                            let now = Date()
                            let formatter = DateFormatter()
                            formatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
                            let nowSt = formatter.string(from: now)

                            let updatedUserData = [ "/\(newPostKey!)": ["uuid": uuid , "device_token": device_token, "language": language, "hourDiff":self.getDateDiff(), "settings":updateSetting, "lastTime": nowSt]]
                            // Do a deep-path update
                            self.myRootRef.updateChildValues(updatedUserData, withCompletionBlock: { (error, ref) -> Void in
                            if ((error) != nil) {
                                print("Error updating data: \(error.debugDescription)")
                            }
                        })
                    }
                    break;
                }
            }else{
                let newPostRef = self.myRootRef.child(byAppendingPath: "/").childByAutoId()
                let newPostKey = newPostRef?.key!
                
                let languages = Locale.preferredLanguages
                let language = languages.first! as String

                let now = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
                let nowSt = formatter.string(from: now)

                // Create the data we want to update
                let updatedUserData = [ "/\(newPostKey!)": ["uuid": uuid , "device_token": device_token, "language": language, "hourDiff":self.getDateDiff(), "settings":updateSetting, "lastTime": nowSt]]
                // Do a deep-path update
                self.myRootRef.updateChildValues(updatedUserData, withCompletionBlock: { (error, ref) -> Void in
                    if ((error) != nil) {
                        print("Error updating data: \(error.debugDescription)")
                    }
                })
            }
            callback()
        })
    }
    
    func new(_ callback: @escaping () -> Void, device_token:String, uuid:String, set:Bool) {
        self.myRootRefSubC = Firebase(url: backendUrl + "sub_category")
        var updateSetting = [String: Bool]()
        self.myRootRefSubC.queryOrdered(byChild: "sub_category_id").observeSingleEvent(of: FEventType.value, with: { subCategories in
            for subCategory in subCategories?.children.allObjects as! [FDataSnapshot] {
                let state = [2]
                let value = subCategory.value as? NSDictionary
                let filtered = state.filter { $0 == value?["state"] as? Int }
                if(filtered.count > 0){
                    if let sub_ctegory_id = value?["sub_category_id"] as? Int{
                        updateSetting["s_" + String(sub_ctegory_id)] = false
                    }
                }
            }
            
            self.myRootRef = Firebase(url: backendUrl + "push_setting")
            self.myRootRef.queryOrdered(byChild: "uuid").queryEqual(toValue: uuid).observeSingleEvent(of: FEventType.value, with: { push_settings in
                if((push_settings?.children.allObjects.count)! > 0){
                    for push_setting in push_settings?.children.allObjects as! [FDataSnapshot] {
                        let value = push_setting.value as? NSDictionary
                        let set_device_token: String = (value!["device_token"] as? String)!
                        if set_device_token == device_token {
                            if let uuid = value!["uuid"] as? String,
                                let device_token = value!["device_token"] as? String,
                                let locsetting = value!["settings"] as? Dictionary<String, Bool>
                            {
                                let newPostKey = push_setting.key!
                                // Create the data we want to update
                                
                                for (key, value) in locsetting{
                                    updateSetting.updateValue(value, forKey: String(key))
                                }
                                
                                let languages = Locale.preferredLanguages
                                let language = languages.first! as String

                                let now = Date()
                                let formatter = DateFormatter()
                                formatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
                                let nowSt = formatter.string(from: now)
                                
                                let updatedUserData = [ "/\(newPostKey)": ["uuid": uuid , "device_token": device_token, "language": language, "hourDiff":self.getDateDiff(), "settings":updateSetting, "lastTime": nowSt]]
                                // Do a deep-path update
                                self.myRootRef.updateChildValues(updatedUserData, withCompletionBlock: { (error, ref) -> Void in
                                    if ((error) != nil) {
                                        print("Error updating data: \(error.debugDescription)")
                                    }
                                })
                            }
                        }
                        else{
                            for (key, _) in updateSetting{
                                updateSetting.updateValue(set, forKey: String(key))
                            }
                            
                            let newPostRef = self.myRootRef.child(byAppendingPath: "/").childByAutoId()
                            let newPostKey = newPostRef?.key!
                            // Create the data we want to update
                            let languages = Locale.preferredLanguages
                            let language = languages.first! as String
                            
                            let now = Date()
                            let formatter = DateFormatter()
                            formatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
                            let nowSt = formatter.string(from: now)
                            
                            let updatedUserData = [ "/\(newPostKey!)": ["uuid": uuid , "device_token": device_token, "language": language, "hourDiff":self.getDateDiff(), "settings":updateSetting, "lastTime": nowSt]]
                            // Do a deep-path update
                            self.myRootRef.updateChildValues(updatedUserData, withCompletionBlock: { (error, ref) -> Void in
                                if ((error) != nil) {
                                    print("Error updating data: \(error.debugDescription)")
                                }
                            })
                        }
                        break;
                    }
                }else{
                    let newPostRef = self.myRootRef.child(byAppendingPath: "/").childByAutoId()
                    let newPostKey = newPostRef?.key!
                    
                    for (key, _) in updateSetting{
                        updateSetting.updateValue(set, forKey: String(key))
                    }

                    // Create the data we want to update
                    let languages = Locale.preferredLanguages
                    let language = languages.first! as String
                    
                    let now = Date()
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
                    let nowSt = formatter.string(from: now)

                    let updatedUserData = [ "/\(newPostKey!)": ["uuid": uuid , "device_token": device_token, "language": language, "hourDiff":self.getDateDiff(), "settings":updateSetting, "lastTime": nowSt]]
                    // Do a deep-path update
                    self.myRootRef.updateChildValues(updatedUserData, withCompletionBlock: { (error, ref) -> Void in
                        if ((error) != nil) {
                            print("Error updating data: \(error.debugDescription)")
                        }
                    })
                }
                callback()
            })
        })
    }
    
    func getDateDiff() -> Int{
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        
        let jdateFormatter = DateFormatter()
        jdateFormatter.timeZone = TimeZone(identifier: "JST")// フォーマットの取得
        jdateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        let jDateStr:String = jdateFormatter.string(from: Date())
        let startDate:Date? = formatter.date(from: jDateStr)
        
        let udateFormatter = DateFormatter()
        udateFormatter.timeZone = TimeZone.current
        udateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
        let uDateStr:String = udateFormatter.string(from: Date())
        let endDate:Date? = formatter.date(from: uDateStr)
        
        let cal = Calendar.current
        let unitFlags: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second]
        let components = (cal as NSCalendar).components(unitFlags, from: startDate!, to: endDate!, options: NSCalendar.Options())
        
        return components.hour!

    }
}
