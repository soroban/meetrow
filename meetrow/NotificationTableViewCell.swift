//
//  NotificationTableViewCell.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2016/03/05.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var TitleTextView: UITextView!
    @IBOutlet weak var TLabel: UILabel!
    @IBOutlet weak var TitleLabel: UILabel!
    @IBOutlet weak var NewIcon: UIImageView!
    @IBOutlet weak var DateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
