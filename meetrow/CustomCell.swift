//
//  CustomCell.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2015/11/12.
//  Copyright © 2015年 大口 尚紀. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {
    var cellImageView: UIImageView!
    var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setCell(_ title : String, thumbnail : String, splBt:UIButton, spuBt:UIButton, addFlg:Bool) {
        // UIImageViewを作成する.
        loadImage(title, url:thumbnail, splBt:splBt, spuBt:spuBt, addFlg:addFlg)
        let imageHeight = self.contentView.frame.height-20
        //let imageWidth = imageHeight*(imageHeight*1.5)/self.contentView.frame.height
        let imageWidth = imageHeight*120/90
        
        let labeHeight = self.contentView.frame.height-20
        let labelWidth = self.contentView.frame.width-imageWidth-30
        
        self.titleLabel = UILabel(frame: CGRect(x:imageWidth+20, y: 10, width: labelWidth, height: labeHeight))
        self.titleLabel.numberOfLines = 0
        self.titleLabel.text = title
        self.titleLabel.textColor = UIColor(hex: "#222831")
        self.titleLabel.font = UIFont.boldSystemFont(ofSize: 12.0)
        self.contentView.addSubview(self.titleLabel)
    }
    
    func loadImage(_ title:String, url:String, splBt:UIButton, spuBt:UIButton, addFlg:Bool) {
        // load image
        let image_url:String = url
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
            let url:URL = URL(string:image_url)!
            
            if let data = try? Data(contentsOf: url)
            {
                if let image = UIImage(data: data)
                {
                    let imageHeight = self.contentView.frame.height-20
                    //let imageWidth = imageHeight*image.size.width/self.contentView.frame.height
                    let imageWidth = imageHeight*120/90
                    self.cellImageView = UIImageView(frame: CGRect(x: 10,y: 10,width: imageWidth, height: imageHeight))
                    
                    // update ui
                    DispatchQueue.main.async {
                        self.cellImageView.image = image
                        self.contentView.addSubview(self.cellImageView)
                        if(addFlg == true){
                            splBt.layer.position = CGPoint(x: self.cellImageView.center.x, y:self.cellImageView.center.y)
                            spuBt.layer.position = CGPoint(x: self.cellImageView.center.x, y:self.cellImageView.center.y)
                            self.contentView.addSubview(splBt)
                            self.contentView.addSubview(spuBt)
                        }
                    }

                }
            }
            else{
                let image = UIImage(named: "no_image.png")! as UIImage
                
                let imageHeight = self.contentView.frame.height-20
                let imageWidth = imageHeight*image.size.width/self.contentView.frame.height
                self.cellImageView = UIImageView(frame: CGRect(x: 10,y: 10,width: imageWidth, height: imageHeight))
                self.cellImageView.contentMode = UIViewContentMode.scaleAspectFill
                
                // update ui
                DispatchQueue.main.async {
                    self.cellImageView.image = image
                    self.contentView.addSubview(self.cellImageView)
                    if(addFlg == true){
                        splBt.layer.position = CGPoint(x: self.cellImageView.center.x, y:self.cellImageView.center.y)
                        spuBt.layer.position = CGPoint(x: self.cellImageView.center.x, y:self.cellImageView.center.y)
                        self.contentView.addSubview(splBt)
                        self.contentView.addSubview(spuBt)
                    }
                }

            }

        }
    }
}
