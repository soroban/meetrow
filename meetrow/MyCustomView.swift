//
//  MyCustomView.swift
//  Meetrow
//
//  Created by 大口 尚紀 on 2016/04/21.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import UIKit

class MyCustomView: UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

    init() {
        super.init(frame: CGRect.zero)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
