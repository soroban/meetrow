//
//  SubCategoryManager.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2015/10/15.
//  Copyright © 2015年 大口 尚紀. All rights reserved.
//

import Foundation

class SubCategoryManager: NSObject {
    
    var subCategories: Array<SubCategory> = []
    static let sharedInstance = SubCategoryManager()
    
    var myRootRef: Firebase!
    
    func getSubCategories(_ callback: @escaping () -> Void, categoryId:Int, state:Array<Int>) {
        var order = "order_en"
        // Firebaseへ接続
        let lanArr = Locale.preferredLanguages[0].components(separatedBy: "-")
        print(Locale.preferredLanguages[0])
        if((Locale.preferredLanguages[0] == "ja-JP" || lanArr.first == "ja")){
            order = "order"
        }
        
        self.myRootRef = Firebase(url: backendUrl + "sub_category")
        self.myRootRef.queryOrdered(byChild: order).observeSingleEvent(of: FEventType.value, with: { subCategories in
            self.subCategories = []
            for subCategory in subCategories?.children.allObjects as! [FDataSnapshot] {
                let value = subCategory.value as? NSDictionary
                if(value!["category_id"] as? Int == categoryId){
                    let filtered = state.filter { $0 == value!["state"] as? Int }
                    if(filtered.count > 0){
                        if let sub_ctegory_id = value!["sub_category_id"] as? Int,
                            let ctegory_id = value!["category_id"] as? Int,
                            let name = value!["name"] as? String,
                            let name_sub = value!["name_en"] as? String,
                            let image = value!["image"] as? String,
                            let state = value!["state"] as? Int,
                            let type = value!["type"] as? Int,
                            let center_id = value!["center_id"] as? Int
                        {
                            let licence = value!["licence"] as? String
                            let licencelink = value!["licencelink"] as? String
                            let day = value!["day"] as? Array<String>
                            let ticket = value!["ticket"] as? Array<Dictionary<String,Any>>
                            let lrAdd = value!["lrAdd"] as? Dictionary<String, Any>
                            let subCategory = SubCategory(sub_ctegory_id: sub_ctegory_id, ctegory_id:ctegory_id, image:image, name:name, name_sub:name_sub, state:state,type:type, licence:licence, licencelink:licencelink, day:day, center_id:center_id, ticket:ticket, lrAdd:lrAdd)
                            self.subCategories.append(subCategory)
                        }
                    }
                }
            }
            callback()
        })
    }
    
    func getSubCategoriesFromIds(_ callback: @escaping () -> Void, Ids:Array<Int>) {
        // Firebaseへ接続
        self.myRootRef = Firebase(url: backendUrl + "sub_category")
        self.myRootRef.queryOrdered(byChild: "sub_category_id").observeSingleEvent(of: FEventType.value, with: { subCategories in
            self.subCategories = []
            for subCategory in subCategories?.children.allObjects as! [FDataSnapshot] {
                let value = subCategory.value as? NSDictionary
                let filtered = Ids.filter { $0 == value!["sub_category_id"] as? Int }
                if(filtered.count > 0){
                if let sub_ctegory_id = value!["sub_category_id"] as? Int,
                    let ctegory_id = value!["category_id"] as? Int,
                    let name = value!["name"] as? String,
                    let name_sub = value!["name_en"] as? String,
                    let image = value!["image"] as? String,
                    let state = value!["state"] as? Int,
                    let type = value!["type"] as? Int,
                    let center_id = value!["center_id"] as? Int
                {
                    let licence = value!["licence"] as? String
                    let licencelink = value!["licencelink"] as? String
                    let day = value!["day"] as? Array<String>
                    let ticket = value!["ticket"] as? Array<Dictionary<String, Any>>
                    let lrAdd = value!["lrAdd"] as? Dictionary<String, Any>
                    let subCategory = SubCategory(sub_ctegory_id: sub_ctegory_id, ctegory_id:ctegory_id, image:image, name:name, name_sub:name_sub, state:state, type:type, licence:licence, licencelink:licencelink, day:day, center_id:center_id, ticket:ticket, lrAdd:lrAdd)
                    self.subCategories.append(subCategory)
                }
                }
            }
            callback()
        })
    }

}

