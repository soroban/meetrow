//
//  HelpMasterManager.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2016/03/06.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import Foundation

class HelpMasterManager: NSObject {
    
    var helpMasters =  Array<HelpMaster>()
    static let sharedInstance = HelpMasterManager()
    
    var myRootRef: Firebase!
    
    func getList(_ callback: @escaping () -> Void) {
        // Firebaseへ接続
        self.myRootRef = Firebase(url: backendUrl + "help_master")
        self.helpMasters = []
        self.myRootRef.queryOrdered(byChild: "id").observeSingleEvent(of: FEventType.value, with: { helpMasters in
            for helpMaster in helpMasters?.children.allObjects as! [FDataSnapshot] {
                let value = helpMaster.value as? NSDictionary
                if let title = value?["title"] as? String,
                    let title_en = value?["title_en"] as? String,
                    let id = value?["id"] as? Int
                {
                    let helpMaster = HelpMaster(id:id, title: title, title_en: title_en)
                    self.helpMasters.append(helpMaster)
                }
            }
            callback()
        })
    }
}
