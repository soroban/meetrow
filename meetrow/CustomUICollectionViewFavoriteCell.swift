//
//  CustomUICollectionViewFavoriteCell.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2016/03/24.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import UIKit

class CustomUICollectionViewFavoriteCell: UICollectionViewCell {
    var imageView:UIImageView = UIImageView()
    var backTextLabel:UILabel = UILabel()
    var textLabel:UILabel = UILabel()
    var licenceLabel:UILabel = UILabel()
    var readyLabel:UILabel = UILabel()
    var typeLabel:UILabel = UILabel()
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.contentView.alpha = 0
        
        let imageWidth =  self.bounds.width
        let imageHeight =  self.bounds.width/3.3
        
        imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: imageWidth, height: imageHeight))
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        
        // UILabelを生成.
        textLabel = UILabel(frame: CGRect(x: imageWidth/28.2, y: imageHeight*2/5, width: imageWidth-(imageWidth*2/28.2), height: imageHeight*2/4))
        textLabel.textColor = UIColor.white
        textLabel.numberOfLines = 2
        //textLabel.layer.borderWidth = 1
        //単語の途中で改行されないようにする
        textLabel.font = UIFont.boldSystemFont(ofSize: 21)
        // 行数無制限
        // サイズを自動調整
        //        textLabel.sizeToFit()
        // 文字を詰めて改行する
        textLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        textLabel.textAlignment = NSTextAlignment.left
        
        // UILabelを生成.
        licenceLabel = UILabel(frame: CGRect(x: imageWidth/28.2, y: textLabel.layer.position.y+25, width: imageWidth-(imageWidth/28.2), height: imageHeight/13))
        licenceLabel.textColor = UIColor.white
        licenceLabel.numberOfLines = 1
        //単語の途中で改行されないようにする
        licenceLabel.font = UIFont.boldSystemFont(ofSize: 9)
        // サイズを自動調整
        //        textLabel.sizeToFit()
        // 文字を詰めて改行する
        licenceLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        licenceLabel.textAlignment = NSTextAlignment.left
        
        // UILabelを生成.
        typeLabel = UILabel(frame: CGRect(x: 7, y: 10, width: 65, height: 28))
        typeLabel.textColor = UIColor.white
        typeLabel.numberOfLines = 1
        //単語の途中で改行されないようにする
        typeLabel.font = UIFont.boldSystemFont(ofSize: 18)
        
        typeLabel.layer.cornerRadius = 5
        typeLabel.clipsToBounds = true
        // サイズを自動調整
        //        textLabel.sizeToFit()
        // 文字を詰めて改行する
        typeLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        typeLabel.textAlignment = NSTextAlignment.center
        
        readyLabel = UILabel(frame: CGRect(x: 0,y: 0,width: imageWidth,height: imageHeight))
        
        backTextLabel = UILabel(frame: CGRect(x: 0, y: 0, width: imageWidth, height: imageHeight))
        backTextLabel.textColor = UIColor.black
        backTextLabel.backgroundColor? =  UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha:0.38)
        
        self.contentView.addSubview(imageView)
        self.contentView.addSubview(backTextLabel)
        self.contentView.addSubview(textLabel)
        self.contentView.addSubview(licenceLabel)
        self.contentView.addSubview(typeLabel)
        self.backgroundColor = UIColor.white
        
        self.contentView.addSubview(readyLabel)
        self.contentView.bringSubview(toFront: readyLabel)
        self.contentView.fadeIn(0.15)
    }
}
