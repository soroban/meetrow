//
//  StationManager.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2015/09/01.
//  Copyright (c) 2015年 大口 尚紀. All rights reserved.
//

import UIKit

class Detail1Manager: NSObject {
    
    var detail:Detail1!
    var detailList:Array<Detail1prot>!
    static let sharedInstance = Detail1Manager()
    
    var myRootRef: Firebase!
    
    func getDetailFromId(callback: @escaping () -> Void, subCategoryId:Int, id:Int) {
        // Firebaseへ接続
        self.myRootRef = Firebase(url: backendUrl + "detail/detail_" + String(subCategoryId))
        self.myRootRef.queryOrdered(byChild: "music_id").queryEqual(toValue: id).observeSingleEvent(of: FEventType.value, with: { details in
            for detail in details?.children.allObjects as! [FDataSnapshot] {
            let value = detail.value as? NSDictionary
            if let jp_name = value?["jp_name"] as? String,
                let en_name = value?["en_name"] as? String
                {
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSSSSS"
                    let jp_wiki_descript = value?["jp_wiki_descript"] as? String
                    let jp_wiki_title = value?["jp_wiki_title"] as? String
                    
                    let temp_jp_wiki = value?["jp_wiki_update_at"] as? NSDictionary
                    let temp_en_wiki = value?["en_wiki_update_at"] as? NSDictionary
                    let jp_wiki_update_at = temp_jp_wiki?["date"] as? String
                    let en_wiki_update_at = temp_en_wiki?["date"] as? String
                    
                    let en_wiki_descript = value?["en_wiki_descript"] as? String
                    let en_wiki_title = value?["en_wiki_title"] as? String
                    var jp_date = NSDate()
                    if(jp_wiki_update_at != nil){
                        jp_date = formatter.date(from: jp_wiki_update_at!)! as NSDate
                    }
                    let en_date = NSDate()
                    if(en_wiki_update_at != nil){
                        jp_date = formatter.date(from: en_wiki_update_at!)! as NSDate
                    }
                    let video_id = value?["video_id"] as? Array<String>
                    let video_title = value?["video_title"] as? Array<String>
                    let video_thumbnail = value?["video_thumbnail"] as? Array<String>
                    
                    let day = value?["day"] as? Array<String>
                    let time = value?["time"] as? Array<String>
                    let stage = value?["stage"] as? Array<String>
                    let jp = value?["jp"] as? Bool
                    
                    let detail = Detail1(id: id, jp_name:jp_name, en_name:en_name, video_id:video_id, video_title:video_title, video_thumbnail:video_thumbnail, jp_wiki_title:jp_wiki_title, jp_wiki_descript:jp_wiki_descript, jp_wiki_update_at:jp_date as Date, en_wiki_title:en_wiki_title, en_wiki_descript:en_wiki_descript, en_wiki_update_at:en_date as Date, day:day, time:time, stage:stage, jp:jp!)
                  self.detail = detail
                }
            }
            callback()
        })
    }

    func getDetails(callback: @escaping () -> Void, subCategoryId:Int, ids:Array<Int>) {
        self.myRootRef = Firebase(url: backendUrl + "detail/detail_" + String(subCategoryId))
        self.detailList = []
        var count=0
        for id in ids{
            self.myRootRef.queryOrdered(byChild: "music_id").queryEqual(toValue: id).observeSingleEvent(of: FEventType.value, with: { details in
                for detail in details?.children.allObjects as! [FDataSnapshot] {
                    let value = detail.value as? NSDictionary
                    if let jp_name = value?["jp_name"] as? String,
                        let en_name = value?["en_name"] as? String
                    {
                        let music_id = Int(detail.key)
                        let day = value?["day"] as? Array<String>
                        let time = value?["time"] as? Array<String>
                        let stage = value?["stage"] as? Array<String>
                        let jp = value?["jp"] as? Bool
                        
                        let detail1prot = Detail1prot(id: music_id!, jp_name:jp_name, en_name:en_name, day:day, time:time, stage:stage, jp:jp!)
                        self.detailList.append(detail1prot)
                    }
                }
                count += 1
                if(count == ids.count){
                    callback()           
                }
            })
            
        }
    }
}


