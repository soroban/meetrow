//
//  FavoriteManager.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2016/02/16.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import Foundation

class FavoriteManager: NSObject {
    
    var favorites: Array<Favorite> = []
    static let sharedInstance = FavoriteManager()
    
    var myRootRef: Firebase!
    
    func getFavorite(_ callback: @escaping () -> Void, uid:String, SubCategoryId:Int) {
        // Firebaseへ接続
        self.myRootRef = Firebase(url: backendUrl + "favorite")
        self.myRootRef.queryOrdered(byChild: "uid").queryEqual(toValue: uid).observeSingleEvent(of: FEventType.value, with: { favorites in
            self.favorites = []
            for favorite in favorites?.children.allObjects as! [FDataSnapshot] {
                let value = favorite.value as? NSDictionary
                if let favlist = value!["favlist"] as? Dictionary<String, Array<Int>>,
                    let title = value!["title"] as? String,
                    let uid = value!["uid"] as? String
                {
                    var fav:Array<Int> = []
                    var sub:Array<Int> = []
                    for (key, value) in favlist {
                        if(key == "sub_" + String(SubCategoryId)){
                            fav = value
                        }
                        var str = key
                        str = str.replacingOccurrences(of: "sub_", with: "")
                        sub.append(Int(str)!)
                    }

                    let favorite = Favorite(fav: fav, title:title, uid:uid,sub: sub)
                    self.favorites.append(favorite)
                }
            }
            callback()
        })
    }
    
    func getFavlist(_ callback: @escaping () -> Void, uid:String) {
        // Firebaseへ接続
        self.myRootRef = Firebase(url: backendUrl + "favorite")
        self.myRootRef.queryOrdered(byChild: "uid").queryEqual(toValue: uid).observeSingleEvent(of: FEventType.value, with: { favorites in
            self.favorites = []
            for favorite in favorites?.children.allObjects as! [FDataSnapshot] {
                let value = favorite.value as? NSDictionary
                if let favlist = value!["favlist"] as? Dictionary<String, Array<Int>>,
                    let title = value!["title"] as? String,
                    let uid = value!["uid"] as? String
                {
                    let fav:Array<Int> = []
                    var sub:Array<Int> = []
                    for (key, _) in favlist {
                        var str = key
                        str = str.replacingOccurrences(of: "sub_", with: "")
                        sub.append(Int(str)!)
                    }
                    
                    let favorite = Favorite(fav: fav, title:title, uid:uid, sub: sub)
                    self.favorites.append(favorite)
                }
            }
            callback()
        })
    }

    func addFavorite(_ callback: @escaping () -> Void, uid:String, SubCategoryId:Int, musicId:Int) {
        self.myRootRef = Firebase(url: backendUrl + "favorite")
        self.myRootRef.queryOrdered(byChild: "uid").queryEqual(toValue: uid).observeSingleEvent(of: FEventType.value, with: { favorites in
            if((favorites?.children.allObjects.count)! > 0){
                for favorite in favorites?.children.allObjects as! [FDataSnapshot] {
                    let value = favorite.value as? NSDictionary
                    if let uid = value!["uid"]
                    {
                        let newPostKey:String! = favorite.key!
                        let favlist = value!["favlist"] as? Dictionary<String, Array<Int>>
                        var updateFavlist:Dictionary<String, Array<Int>> = [:]
                        var check = false
                        
                        if(favlist != nil){
                            for (key, value) in favlist! {
                                var temp = value
                                if(key == "sub_" + String(SubCategoryId)){
                                    var overCheck = false
                                    for tempMusicId in value{
                                        if(tempMusicId == musicId){
                                            overCheck = true
                                        }
                                    }
                                    if(overCheck == false){
                                        temp.append(musicId)
                                        updateFavlist[key] = temp
                                    }
                                    check = true;
                                }else{
                                    updateFavlist[key] = temp
                                }
                            }
                        }
                    
                        if(!check){
                            updateFavlist["sub_" + String(SubCategoryId)] = [musicId]
                        }
                        
                        let now = Date()
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
                        let nowSt = formatter.string(from: now)

                        
                        // Create the data we want to update
                        let updatedUserData = [ "/\(newPostKey!)": ["favlist": updateFavlist , "title": "wishlist", "uid":uid, "lastTime": nowSt]]
                        // Do a deep-path update
                        self.myRootRef.updateChildValues(updatedUserData, withCompletionBlock: { (error, ref) -> Void in
                            if ((error) != nil) {
                                print("Error updating data: \(error.debugDescription)")
                            }
                        })
                    }
                }
            }else{
                let newPostRef = self.myRootRef.child(byAppendingPath: "/").childByAutoId()
                let newPostKey = newPostRef?.key!
                
                let key = "sub_" + String(SubCategoryId);
                
                let now = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
                let nowSt = formatter.string(from: now)

                // Create the data we want to update
                let updatedUserData = [ "/\(newPostKey!)": ["favlist":[key: ["0":musicId]] , "title": "wishlist", "uid":uid, "lastTime": nowSt]]
                // Do a deep-path update
                self.myRootRef.updateChildValues(updatedUserData, withCompletionBlock: { (error, ref) -> Void in
                    if ((error) != nil) {
                        print("Error updating data: \(error.debugDescription)")
                    }
                })
            }
            callback()
        })
    }
    
    func subFavorite(_ callback: @escaping () -> Void,uid:String, SubCategoryId:Int, musicId:Int) {
        self.myRootRef = Firebase(url: backendUrl + "favorite")
        self.myRootRef.queryOrdered(byChild: "uid").queryEqual(toValue: uid).observeSingleEvent(of: FEventType.value, with: { favorites in
            if((favorites?.children.allObjects.count)! > 0){
                for favorite in favorites?.children.allObjects as! [FDataSnapshot] {
                    let value = favorite.value as? NSDictionary
                    if let uid = value!["uid"] as? String
                    {
                        let newPostKey = favorite.key!
                        let favlist = value!["favlist"] as? Dictionary<String, Array<Int>>
                        var updateFavlist:Dictionary<String, Array<Int>> = [:]
                        
                        for (key, value) in favlist! {
                            var temp = value
                            if(key == "sub_" + String(SubCategoryId)){
                                if let index = temp.index(of: musicId) {
                                    temp.remove(at: index)
                                }
                                updateFavlist[key] = temp
                            }else{
                                updateFavlist[key] = temp
                            }
                        }
                        
                        let now = Date()
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
                        let nowSt = formatter.string(from: now)
                        
                        // Create the data we want to update
                        let updatedUserData = [ "/\(newPostKey)": ["favlist": updateFavlist , "title": "wishlist", "uid":uid, "lastTime": nowSt]]
                        // Do a deep-path update
                        self.myRootRef.updateChildValues(updatedUserData, withCompletionBlock: { (error, ref) -> Void in
                            if ((error) != nil) {
                                print("Error updating data: \(error.debugDescription)")
                            }
                        })
                    }
                }
            }
            callback()
        })
    }
    
    
    func saveFavorite(_ callback: @escaping () -> Void, uid:String, SubCategoryId:Int, musicIds:Array<Int>) {
        self.myRootRef = Firebase(url: backendUrl + "favorite")
        self.myRootRef.queryOrdered(byChild: "uid").queryEqual(toValue: uid).observeSingleEvent(of: FEventType.value, with: { favorites in
            if((favorites?.children.allObjects.count)! > 0){
                for favorite in favorites?.children.allObjects as! [FDataSnapshot] {
                    let value = favorite.value as? NSDictionary
                    if let uid = value!["uid"] as? String
                    {
                        let newPostKey = favorite.key!
                        let favlist = value!["favlist"] as? Dictionary<String, Array<Int>>
                        var updateFavlist:Dictionary<String, Array<Int>> = [:]
                        var check = false
                        for (key, value) in favlist! {
                            let temp = value
                            if(key == "sub_" + String(SubCategoryId)){
                                updateFavlist[key] = musicIds
                                check = true;
                            }else{
                                updateFavlist[key] = temp
                            }
                        }
                        
                        if(!check){
                            updateFavlist["sub_" + String(SubCategoryId)] = musicIds
                        }
                        
                        let now = Date()
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
                        let nowSt = formatter.string(from: now)
                        
                        // Create the data we want to update
                        let updatedUserData = [ "/\(newPostKey)": ["favlist": updateFavlist , "title": "wishlist", "uid":uid, "lastTime": nowSt]]
                        // Do a deep-path update
                        self.myRootRef.updateChildValues(updatedUserData, withCompletionBlock: { (error, ref) -> Void in
                            if ((error) != nil) {
                                print("Error updating data: \(error.debugDescription)")
                            }
                        })
                    }
                }
            }else{
                let newPostRef = self.myRootRef.child(byAppendingPath: "/").childByAutoId()
                let newPostKey = newPostRef?.key!
                
                let key = "sub_" + String(SubCategoryId);
                
                let now = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
                let nowSt = formatter.string(from: now)
                
                // Create the data we want to update
                let updatedUserData = [ "/\(newPostKey!)": ["favlist":[key: musicIds] , "title": "wishlist", "uid":uid, "lastTime": nowSt]]
                // Do a deep-path update
                self.myRootRef.updateChildValues(updatedUserData, withCompletionBlock: { (error, ref) -> Void in
                    if ((error) != nil) {
                        print("Error updating data: \(error.debugDescription)")
                    }
                })
            }
            callback()
        })
    }
}
