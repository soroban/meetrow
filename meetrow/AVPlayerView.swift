import UIKit
import AVFoundation

class AVPlayerView : UIView {
    
    // UIViewのサブクラスを作りlayerClassメソッドをオーバーライドしてAVPlayerLayerに差し替える
    override class var layerClass : AnyClass {
        return AVPlayerLayer.self
    }
    
    func player() -> AVPlayer {
        let layer: AVPlayerLayer = self.layer as! AVPlayerLayer
        return layer.player!
    }
    
    func setPlayer(_ player: AVPlayer) {
        let layer: AVPlayerLayer = self.layer as! AVPlayerLayer
        layer.player = player
    }
    
    func setVideoFillMode(_ fillMode: NSString) {
        let layer: AVPlayerLayer = self.layer as! AVPlayerLayer
        layer.videoGravity = fillMode as String
    }
    
    func videoFillMode() -> NSString {
        let layer: AVPlayerLayer = self.layer as! AVPlayerLayer
        return layer.videoGravity as NSString
    }
}
