//
//  StationManager.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2015/09/01.
//  Copyright (c) 2015年 大口 尚紀. All rights reserved.
//

import UIKit

class StationManager: NSObject {
    
    var stations: Array<Station> = []
    static let sharedInstance = StationManager()
    
    var myRootRef: Firebase!
    
    func getStations(callback: @escaping () -> Void, subCategoryId:Int) {
        // Firebaseへ接続
        stations = []
        self.myRootRef = Firebase(url: backendUrl + "station/station_" + String(subCategoryId))
        self.myRootRef.observeSingleEvent(of: FEventType.value, with: { stations in
            for station in stations?.children.allObjects as! [FDataSnapshot] {
                let value = station.value as? NSDictionary
                if let m = value?["m"] as? Int,
                    let l = value?["l"] as? Int,
                    let music_id = value?["music_id"] as? Int,
                    let jp_name = value?["jp_name"] as? String,
                    let en_name = value?["en_name"] as? String,
                    let full = value?["full"] as? Array<String>,
                    let jp = value?["jp"] as? Bool
                {
                    let day = value?["day"] as? Array<String>
                    let no_disp = value?["no_disp"] as? Bool
                    let station = Station(l:l, m:m, music_id:music_id, jp_name:jp_name, en_name:en_name, full: full, jp:jp, day:day, no_disp:no_disp)
                    self.stations.append(station)
                }
            }
            callback()
        })
    }
}

