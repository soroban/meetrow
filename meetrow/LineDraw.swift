//
//  LineDraw.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2015/09/01.
//  Copyright (c) 2015年 大口 尚紀. All rights reserved.
//

import UIKit

class LineDraw: UIView {
    override func draw(_ rect: CGRect) {
        self.backgroundColor = UIColor.clear
        
        // 描画する
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 50, y: 100));
        path.addLine(to: CGPoint(x: 150, y: 100))
        UIColor.orange.setStroke()
        path.lineWidth = 2
        path.stroke()
    }
    
}
