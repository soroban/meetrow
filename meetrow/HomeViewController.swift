//
//  HomeViewController.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2015/10/13.
//  Copyright © 2015年 大口 尚紀. All rights reserved.
//

import UIKit
import Foundation
import WatchKit

class HomeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIAlertViewDelegate {
    let cateogryCollection = CategoryManager.sharedInstance
    var myCollectionView : UICollectionView!
    var nextCategoryId : Int!
    var nextTitle : String!
    var category: Array<Category> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.isUserInteractionEnabled = false
        
        self.tabBarController!.tabBar.isHidden = true
        let view:UIView = UINib(nibName: "SLaunchScreen", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = self.view.frame
        view.isUserInteractionEnabled = false
        self.view.addSubview(view)
        //NSNotificationCenter.defaultCenter().addObserver(self, selector: "setCenter:", name:"applicationWillEnterForeground", object: nil)
        let backButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backButtonItem

        let callbackCategory = { () -> Void in
            self.category = self.cateogryCollection.categories
            let firstLaunch = UserDefaults.standard.bool(forKey: "firstLaunch")
            if firstLaunch != true {
                var mapImage = UIImage(named: "expmap_mini")
                var exp2Image = UIImage(named: "exp2_mini")
                var exp3Image = UIImage(named: "exp3_mini")
                var exp3 = NSLocalizedString("exp3_mini",comment: "")
                var exp2 = NSLocalizedString("exp2",comment: "")
                
                if(self.view.frame.height > 568){
                    if(ipad){
                        mapImage = UIImage(named: "expmap_ipad")
                        exp2Image = UIImage(named: "exp2_ipad")
                        exp3Image = UIImage(named: "exp3_ipad")
                        exp2 = NSLocalizedString("exp2",comment: "") + "\n\n"
                        exp3 = NSLocalizedString("exp3",comment: "") + "\n\n\n"
                    }else{
                        mapImage = UIImage(named: "expmap")
                        exp2Image = UIImage(named: "exp2")
                        exp3Image = UIImage(named: "exp3")
                        exp3 = NSLocalizedString("exp3",comment: "")
                    }
                    
                }
                
                let content1 = OnboardingContentViewController(
                    title: NSLocalizedString("exp1_title",comment: ""),
                    body: NSLocalizedString("exp1",comment: ""),
                    image: mapImage,
                    buttonText: NSLocalizedString("next",comment: ""),
                    action: nil
                )
                
                let content2 = OnboardingContentViewController(
                    title: "",
                    body: exp2,
                    image: exp2Image,
                    buttonText: NSLocalizedString("next",comment: ""),
                    action: nil
                )
                let content3 = OnboardingContentViewController(
                    title: "",
                    body: exp3,
                    image: exp3Image,
                    buttonText: NSLocalizedString("exp3_button",comment: ""),
                    action: {
                        self.dismiss(animated: true, completion: nil)
                        //self.myCollectionView.fadeIn(0)
                        self.tabBarController!.tabBar.isHidden = false;
                        
                        //self.myCollectionView.userInteractionEnabled = true
                        self.view.isUserInteractionEnabled = true
                        
                        for view in self.tabBarController!.view.subviews {
                            if(view.tag == 123456789){
                                view.isHidden = false
                            }
                        }

                    }
                )
                let vc = OnboardingViewController(
                    backgroundImage: nil,
                    contents: [content1, content2, content3]
                )
                //vc.allowSkipping = true
                vc?.skipHandler = { _ in
                    self.dismiss(animated: true, completion: nil)
                    self.myCollectionView.fadeIn(0)
                    self.tabBarController!.tabBar.isHidden = false;
                    
                    self.myCollectionView.isUserInteractionEnabled = true
                    self.view.isUserInteractionEnabled = true
                    
                    for view in self.tabBarController!.view.subviews {
                        if(view.tag == 123456789){
                            view.isHidden = false
                        }
                    }

                }
                
                vc?.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
                self.present(vc!, animated: true, completion: nil)
                
                //最初は飛ばす
                self.nextCategoryId = 1
                let lanArr = Locale.preferredLanguages[0].components(separatedBy: "-")
                if(Locale.preferredLanguages[0] == "ja-JP" || lanArr.first == "ja"){
                    self.nextTitle = self.category[0].name
                }else{
                    self.nextTitle = self.category[0].name_en
                }
                self.performSegue(withIdentifier: "SubViewSegue", sender: nil)
                UserDefaults.standard.set(true, forKey: "firstLaunch")
                
                self.initView()
                self.tabBarController!.tabBar.isHidden = false;
                self.view.isUserInteractionEnabled = true                
                for view in self.tabBarController!.view.subviews {
                    if(view.tag == 123456789){
                        view.isHidden = false
                    }
                }

//                self.view.addSubview(self.myCollectionView)
            }else{
                if(fromNotification){
                    UIApplication.shared.keyWindow?.rootViewController is MyTabBarController
                    let myTab = UIApplication.shared.keyWindow?.rootViewController as! MyTabBarController
                    myTab.selectedViewController = (myTab.childViewControllers[2]) as! UINavigationController
                    
                    let nc = myTab.viewControllers![2] as! UINavigationController
                    let tempVc = nc.viewControllers[0]
                    tempVc.performSegue(withIdentifier: "SettingNotificationSegue",sender: nil)
                    
                }

                //最初は飛ばす
                self.nextCategoryId = 1
                let lanArr = Locale.preferredLanguages[0].components(separatedBy: "-")
                if(Locale.preferredLanguages[0] == "ja-JP" || lanArr.first == "ja"){
                    self.nextTitle = self.category[0].name
                }else{
                    self.nextTitle = self.category[0].name_en
                }
                
                self.performSegue(withIdentifier: "SubViewSegue", sender: nil)

                self.initView()
                
//                self.view.addSubview(self.myCollectionView)
                            view.fadeOut(0.15)
//                self.myCollectionView.fadeIn(0.15)
                self.tabBarController!.tabBar.isHidden = false;
                
//                self.myCollectionView.userInteractionEnabled = true
                self.view.isUserInteractionEnabled = true
                
                for view in self.tabBarController!.view.subviews {
                    if(view.tag == 123456789){
                        view.isHidden = false
                    }
                }

            }
            
        }
        self.cateogryCollection.getCategories(callbackCategory, state:[1,2])
//        self.cateogryCollection.fetchCategories(callbackCategory);
    }
        /*
    Cellが選択された際に呼び出される
    */
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(self.category[indexPath.row].state == 2){
            nextCategoryId = self.category[indexPath.row].category_id
            let lanArr = Locale.preferredLanguages[0].components(separatedBy: "-")
            if(Locale.preferredLanguages[0] == "ja-JP" || lanArr.first == "ja"){
                nextTitle = self.category[indexPath.row].name
            }else{
                nextTitle = self.category[indexPath.row].name_en
            }
            performSegue(withIdentifier: "SubViewSegue", sender: nil)
        }
    }
    
    func initView(){
//        let tabHeight = self.tabBarController!.tabBar.frame.size.height
//        let staHeight = UIApplication.sharedApplication().statusBarFrame.height
//
//        let imageWidth = self.view.bounds.width
//        let imageHeight = self.view.bounds.width/1.618
//        
//        // CollectionViewのレイアウトを生成.
//        let layout = UICollectionViewFlowLayout()
//        // Cell一つ一つの大きさ.
//        layout.itemSize = CGSizeMake(imageWidth, imageHeight)
//        
//        // Cellのマージン.
//        layout.sectionInset = UIEdgeInsetsMake(0, 0, tabHeight + staHeight + addHeight,0)
//        //アイテム同士のマージン
//        layout.minimumInteritemSpacing = 0.0
//        //セクションとアイテムのマージン
//        layout.minimumLineSpacing = 0.0
//        // セクション毎のヘッダーサイズ.
//        layout.headerReferenceSize = CGSizeMake(0,0)
//        
//        // CollectionViewを生成.
//        self.myCollectionView = UICollectionView(frame: CGRect(x: 0, y: UIApplication.sharedApplication().statusBarFrame.height ,width: self.view.bounds.width,height: self.view.bounds.height), collectionViewLayout: layout)
//        self.myCollectionView.userInteractionEnabled = false
//        // Cellに使われるクラスを登録.
//        
//        for i in 0 ..< self.category.count {
//            self.myCollectionView.registerClass(CustomUICollectionViewCell.self, forCellWithReuseIdentifier: "MyCell" + String(i))
//        }
//        self.myCollectionView.alpha = 0
//        self.myCollectionView.delegate = self
//        self.myCollectionView.dataSource = self
//        let colorKey = UIColor(hex: "#EEEEEE")
//        
//        self.myCollectionView.backgroundColor = colorKey
        
        // バッジ、サウンド、アラートをリモート通知対象として登録する
        let settings = UIUserNotificationSettings(types: [.badge, .sound, .alert], categories:nil)
        UIApplication.shared.registerUserNotificationSettings(settings)
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    /*
    Cellの総数を返す
    */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.category.count
    }
    
    /*
    Cellに値を設定する
    */
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell : CustomUICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCell" + String(indexPath.row), for: indexPath) as! CustomUICollectionViewCell
        let url = strageUrl + self.category[indexPath.row].image
        if(cell.textLabel.attributedText == nil){
        if(indexPath.row < 3){
            let url:URL = URL(string:url)!
            var placeholder = UIImage(named: "noimage.png")! as UIImage
            if((try? Data(contentsOf: url)) != nil){
                let data:Data = try! Data(contentsOf: url)
                placeholder = UIImage(data: data)!
            }
            
            let categoryImage = ImageTool.cropThumbnailImage(placeholder, w: Int(self.view.bounds.width), h:Int(self.view.bounds.width/1.618))
            let ciImage:CIImage = CIImage(image:categoryImage)!
            let ciFilter:CIFilter = CIFilter(name: "CIGammaAdjust")!
            ciFilter.setValue(ciImage, forKey: kCIInputImageKey)
            ciFilter.setValue(1.3, forKey: "inputPower")
            let ciContext:CIContext = ObjcUtility.cicontext(options: nil)
            let cgimg:CGImage = ciContext.createCGImage(ciFilter.outputImage!, from:ciFilter.outputImage!.extent)!
            
            //image2に加工後のUIImage
            let newImage:UIImage = UIImage(cgImage: cgimg, scale: 1.0, orientation:UIImageOrientation.up)
            cell.imageView.image = newImage
            // update ui
        }else{
            loadImageSync(url, forImageView: cell.imageView)
        }
        }

        let countStr = self.category[indexPath.row].name.characters.count
        let countStrSub = self.category[indexPath.row].name_en.characters.count
        let subCategory =  self.category[indexPath.row].name + self.category[indexPath.row].name_en

        let categoryName = NSMutableAttributedString(string: subCategory)
        categoryName.addAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 29)], range: NSMakeRange(countStr, countStrSub))
        categoryName.addAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 76)], range: NSMakeRange(0, countStr))
        
        let kanji    = "漢字"
        if self.category[indexPath.row].name < kanji{
            //文字間(NSKernAttributeName)を設定する
            categoryName.addAttributes([NSKernAttributeName:  -10], range: NSMakeRange(0, countStr))
        }

        if(self.category[indexPath.row].state == 1){
            cell.textLabel.alpha = 0.3
            cell.readyLabel.text = NSLocalizedString("UnderConstruction",comment: "")
            cell.readyLabel.textColor = UIColor.white
            cell.readyLabel.font = UIFont.systemFont(ofSize: 29)
            cell.readyLabel.textAlignment = NSTextAlignment.center
            cell.readyLabel.alpha = 1.0
            
            cell.backTextLabel.textColor = UIColor.white
            cell.backTextLabel.backgroundColor? =  UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha:0.5)
            cell.textLabel.alpha = 0
        }
        
        cell.textLabel.attributedText = categoryName
        return cell
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        let orientation: UIInterfaceOrientationMask = [UIInterfaceOrientationMask.portrait]
        return orientation
    }
    
    //指定方向に自動的に変更するか？
    override var shouldAutorotate : Bool{
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "SubViewSegue") {
            let nextVC: SubViewController = segue.destination as! SubViewController
            nextVC.cateogryId = nextCategoryId
            nextVC.naviTitle = nextTitle
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let nv = navigationController {
            // 隠す
            nv.setNavigationBarHidden(true, animated: animated)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //self.myCollectionView.layer.position = CGPoint(x: self.view.center.x, y:self.view.center.y)
        if let nv = navigationController {
            // 隠す
            nv.setNavigationBarHidden(false, animated: animated)
        }
    }
    
    func setCenter(_ notification: Foundation.Notification){
        self.myCollectionView.layer.position = CGPoint(x: self.view.center.x, y:self.view.center.y)
    }
    
    
    
    func loadImageSync(_ url:String, forImageView: UIImageView) {
        // load image
        let image_url:String = url
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
            let url:URL = URL(string:image_url)!
            let data:Data = try! Data(contentsOf: url)
            let placeholder = UIImage(data: data)!
            
            let categoryImage = ImageTool.cropThumbnailImage(placeholder, w: Int(self.view.bounds.width), h:Int(self.view.bounds.width/1.618))
            let ciImage:CIImage = CIImage(image:categoryImage)!
            let ciFilter:CIFilter = CIFilter(name: "CIGammaAdjust")!
            ciFilter.setValue(ciImage, forKey: kCIInputImageKey)
            ciFilter.setValue(1.3, forKey: "inputPower")
            let ciContext:CIContext = ObjcUtility.cicontext(options: nil)
            let cgimg:CGImage = ciContext.createCGImage(ciFilter.outputImage!, from:ciFilter.outputImage!.extent)!
            
            //image2に加工後のUIImage
            let newImage:UIImage = UIImage(cgImage: cgimg, scale: 1.0, orientation:UIImageOrientation.up)
            // update ui
            DispatchQueue.main.async {
                forImageView.image = newImage
                forImageView.fadeIn(0.15)
            }
        }
    }
}

public func openUrl(_ url:String!) {
    let targetURL=URL(string: url)
    let application=UIApplication.shared
    application.openURL(targetURL!)
}


public extension UIImageView {
    
    /**
     Fade in a view with a duration
     
     - parameter duration: custom animation duration
     */
    func fadeIn(duration: TimeInterval = 1.0) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 1.0
        })
    }
    
    func fadeOut(duration: TimeInterval = 1.0) {
        UIView.animate(withDuration: duration, animations: {
            self.alpha = 0
        })
    }

}

extension String {
    func katakana() -> String {
        var str = ""
        
        // 文字列を表現するUInt32
        for c in unicodeScalars {
            if c.value >= 0x3041 && c.value <= 0x3096 {
                str.append(String(describing: UnicodeScalar(c.value+96)))
            } else {
                str.append(String(c))
            }
        }
        
        return str
    }
    
    func hiragana() -> String {
        var str = ""
        for c in unicodeScalars {
            if c.value >= 0x30A1 && c.value <= 0x30F6 {
                str.append(String(describing: UnicodeScalar(c.value-96)))
            } else {
                str.append(String(c))
            }
        }
        
        return str
    }
}
