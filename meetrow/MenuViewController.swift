//
//  MenuViewController.swift
//  Meetrow
//
//  Created by 大口 尚紀 on 2016/04/10.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import UIKit
import GoogleMobileAds

class MenuViewController:  UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, GADBannerViewDelegate {

    @IBOutlet weak var SettingTable: UITableView!
    var table_data = Array<TableData>()
    var items = Array<Item>()
    var searches = Array<Search>()
    var dayArr:Array<String>!
    var selectedCells:Array<Bool>!
    var delegate: ViewControllerDelegate!
    var searchTableView: UITableView!
    var menuRecBanner: GADBannerView!
    
    @IBOutlet weak var mySearchBar: UISearchBar!
    
    struct TableData
    {
        var section:Bool = false
        var title = String()
        var message = String()
        init(){}
    }

    struct Item
    {
        var id:Int = Int()
        var name = String()
        init(){}
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let mWidth = CGFloat(250)
        let mHeight = CGFloat(209)
        let searchHeight = CGFloat(50)
//        if(self.view.bounds.height > 568){
//            mWidth = 300
//            mHeight = 250
//            searchHeight = CGFloat(50)
//        }
        
        self.menuRecBanner.frame = CGRect(x: 0, y:UIApplication.shared.statusBarFrame.height, width:self.menuRecBanner.frame.width , height:self.menuRecBanner.frame.height)
        self.searchTableView = UITableView(frame: CGRect(x: 0, y: mHeight+50+UIApplication.shared.statusBarFrame.height-6, width: mWidth+20, height:self.view.bounds.height-(mHeight+searchHeight+UIApplication.shared.statusBarFrame.height)))
        self.searchTableView.isHidden = true
        self.searchTableView.tag = 9999998
        self.searchTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        // DataSourceの設定をする.
        self.searchTableView.dataSource = self
        // Delegateを設定する.
        self.searchTableView.delegate = self
        self.view.addSubview(self.searchTableView)
        
        //サーチバー作成
//        self.mySearchBar = UISearchBar(frame:CGRect(x:0, y: mHeight+UIApplication.sharedApplication().statusBarFrame.height, width: mWidth, height: searchHeight))
        self.mySearchBar.delegate = self
        self.mySearchBar.showsCancelButton = true
        self.mySearchBar.placeholder = NSLocalizedString("placefolder",comment: "")
        self.view.addSubview(mySearchBar)

        
        let v:UIView = UIView(frame: CGRect.zero)
        v.backgroundColor = UIColor.clear
        
        if(dayArr.count > 0){
            let tableHeight = self.view.bounds.height-(mHeight+searchHeight+50)
            self.SettingTable.tableFooterView = v
            self.SettingTable.frame = CGRect(x: 0, y: 0,width: mWidth,height: tableHeight)
            self.SettingTable.layer.position = CGPoint(x:self.view.bounds.width/2 , y:mHeight+50+UIApplication.shared.statusBarFrame.height+(tableHeight/2))
            self.SettingTable.separatorInset = UIEdgeInsets.zero;
            self.SettingTable.isScrollEnabled = false
            SettingTable.dataSource = self
            SettingTable.delegate = self

            self.table_data = Array<TableData>()
            var new_elements:TableData = TableData()

            new_elements = TableData()
            new_elements.section = true
            new_elements.title = "By Day"
            table_data.append(new_elements)

            let weekdays = ["日","月","火","水","木","金","土"]
            for day in dayArr{
                new_elements = TableData()
                new_elements.section = false
            
                let areaArr = day.components(separatedBy: "-")
                var day = day
                var area = ""
                if(areaArr.count > 1){
                    area = " - " + areaArr[0]
                    day = areaArr[1]
                }
            
                let date_formatter: DateFormatter = DateFormatter()
                date_formatter.dateFormat = "yyyy/MM/dd"
                let date = date_formatter.date(from: day)
                let cal: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
                let comp: DateComponents = (cal as NSCalendar).components(
                    [NSCalendar.Unit.weekday],
                    from: date!
                )
                let weekday: Int = comp.weekday!
                let weekdaySymbolIndex: Int = weekday - 1

            
                let out_formatter: DateFormatter = DateFormatter()
                out_formatter.locale = Locale(identifier: "en_US")
                let lanArr = Locale.preferredLanguages[0].components(separatedBy: "-")
                if(Locale.preferredLanguages[0] == "ja-JP" || lanArr.first == "ja"){
                    out_formatter.dateFormat = "MM.dd [\(weekdays[weekdaySymbolIndex])]"
                }else{
                    out_formatter.dateFormat = "E, MMM d"
                }
            
                new_elements.title = out_formatter.string(from: date!) + area
                table_data.append(new_elements)
            }
        }else{
            SettingTable.isHidden = true
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        if let nv = navigationController {
            // 隠す
            //nv.setNavigationBarHidden(true, animated: animated)
//        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(tableView.tag == 9999999){
            if(table_data[indexPath.row].section == false){
                let cell = SettingTable.dequeueReusableCell(withIdentifier: "HelpTableViewCell", for: indexPath) as! HelpTableViewCell
                cell.TitleLabel.text = table_data[indexPath.row].title
                if(selectedCells[indexPath.row-1] == true){
                    cell.accessoryType=UITableViewCellAccessoryType.checkmark
                }else{
                    cell.accessoryType=UITableViewCellAccessoryType.none
                }
                cell.tintColor = UIColor.lightGray
                cell.TitleLabel.textColor = UIColor(hex: "#222831")
                return cell
            }else{
                let cell = SettingTable.dequeueReusableCell(withIdentifier: "CustomHeaderCell", for: indexPath) as! CustomHeaderCell
                cell.headerLabel.text = table_data[indexPath.row].title
                cell.tintColor = UIColor.lightGray
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.headerLabel.textColor = UIColor(hex: "#222831")
                return cell
            }
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            cell.textLabel?.text = self.items[indexPath.row].name
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection    section: Int) -> Int
    {
        if(tableView.tag == 9999999){
            return table_data.count
        }
        else{
            return self.items.count
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath:IndexPath) {
        if(tableView.tag == 9999999){
            SettingTable?.deselectRow(at: indexPath, animated: true)
            let cell = self.SettingTable.cellForRow(at: indexPath)

            if(self.table_data[indexPath.row].section == false){
                if cell!.accessoryType == UITableViewCellAccessoryType.none{
                    cell!.accessoryType=UITableViewCellAccessoryType.checkmark
                    self.selectedCells[indexPath.row-1] = true
                }else{
                    cell!.accessoryType=UITableViewCellAccessoryType.none
                    self.selectedCells[indexPath.row-1] = false

                }
            }
        }else{
            self.delegate.transBackFromMenu(self.items[indexPath.row].id)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.mySearchBar.text = ""
        self.delegate.backFromMenu(self.selectedCells)
        self.items = Array<Item>()
        self.searchTableView.isHidden = true
        self.searchTableView.reloadData()

        //self.dismissViewControllerAnimated(true, completion: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        if(table_data[indexPath.row].section == false){
        //            return
        //        }else{
        //            return 60
        //        }
        return 55
    }

    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        let orientation: UIInterfaceOrientationMask = [UIInterfaceOrientationMask.portrait]
        return orientation
    }
    
    //指定方向に自動的に変更するか？
    override var shouldAutorotate : Bool{
        return true
    }

    //サーチバー更新時(UISearchBarDelegateを関連づけておく必要があります）
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.mySearchBar.showsCancelButton = true
        if(searchBar.text == "" || searchBar.text == " " || searchBar.text == "　"){
            self.items = Array<Item>()
            self.searchTableView.isHidden = true
        }
        else{
            self.searchTableView.isHidden = false
            var addArr = Array<Int>()
            self.items = Array<Item>()
            var new_item:Item = Item()
        
            var searchWord = searchBar.text
            var changeCheck = false
            for code in searchWord!.unicodeScalars {
                if(checkKana(code) == true || checkHira(code) == true){
                    changeCheck = true
                }
            }
            if(changeCheck){
                searchWord = searchWord?.katakana()
            }
            
            var pattern = "^" + searchWord! + ".*"
            for search in searches{
                for keyword in search.keyword{
                    let keyword = keyword.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                    
                    var newkeyword : String = keyword.replacingOccurrences(of: "The ", with:"", options:NSString.CompareOptions.caseInsensitive, range: nil)
                    newkeyword = newkeyword.replacingOccurrences(of: "ザ・", with:"", options:NSString.CompareOptions.caseInsensitive, range: nil)
                    
                    var changeCheck = false
                    for code in keyword.unicodeScalars {
                        if(checkKana(code) == true || checkHira(code) == true){
                            changeCheck = true
                        }
                    }
                    if(changeCheck){
                        newkeyword = newkeyword.katakana()
                    }
                    
                    var check = true
                    for add in addArr {
                        if(add == search.id){
                            check = false
                        }
                    }
                    
                    if(Regexp(pattern).isMatch(newkeyword) && check == true){
                        new_item.id = search.id
                        new_item.name = keyword
                        self.items.append(new_item)
                        addArr.append(search.id)
                    }
                }
            }

            pattern = "^" + searchWord! + ".*"
            for search in searches{
                for keyword in search.keyword{
                    let keyword = keyword.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                    
                    var newkeyword = keyword
                    var changeCheck = false
                    for code in newkeyword.unicodeScalars {
                        if(checkKana(code) == true || checkHira(code) == true){
                            changeCheck = true
                        }
                    }
                    if(changeCheck){
                        newkeyword = newkeyword.katakana()
                    }
                    
                    var check = true
                    for add in addArr {
                        if(add == search.id){
                            check = false
                        }
                    }

                    if(Regexp(pattern).isMatch(newkeyword) && check == true){
                        new_item.id = search.id
                        new_item.name = keyword
                        self.items.append(new_item)
                        addArr.append(search.id)
                    }
                }
            }

            pattern = ".*" + searchWord! + ".*"
            for search in searches{
                for keyword in search.keyword{
                    let keyword = keyword.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                    
                    var newkeyword = keyword
                    var changeCheck = false
                    for code in newkeyword.unicodeScalars {
                        if(checkKana(code) == true || checkHira(code) == true){
                            changeCheck = true
                        }
                    }
                    if(changeCheck){
                        newkeyword = newkeyword.katakana()
                    }

                    var check = true
                    for add in addArr {
                        if(add == search.id){
                            check = false
                        }
                    }

                    if(Regexp(pattern).isMatch(newkeyword) && check == true){
                        new_item.id = search.id
                        new_item.name = keyword
                        self.items.append(new_item)
                        addArr.append(search.id)
                    }
                }
            }
        }
        self.searchTableView.reloadData()
    }
    
    func checkHira(_ code: UnicodeScalar) -> Bool {
        switch code.value {
            case 0x3040...0x309f: return true
        default: return false
        }
    }

    func checkKana(_ code: UnicodeScalar) -> Bool {
        switch code.value {
        case 0x30a0...0x30ff: return true
        default: return false
        }
    }
    
    //キャンセルクリック時(UISearchBarDelegateを関連づけておく必要があります）
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        //myLabel.text = ""
        self.mySearchBar.showsCancelButton = false
        self.mySearchBar.text = ""
        self.items = Array<Item>()
        self.searchTableView.reloadData()
        self.searchTableView.isHidden = true
        self.view.endEditing(true)
    }
    
    //サーチボタンクリック時(UISearchBarDelegateを関連づけておく必要があります）
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        //myLabel.text = "社内に同じ意見があるか検索中..."
//        self.mySearchBar.text = ""
        self.mySearchBar.showsCancelButton = true
        self.mySearchBar.resignFirstResponder()
        //self.view.endEditing(true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
