enum FadeType: TimeInterval {
    case
    fast = 0.2,
    normal = 0.5,
    slow = 1.0
}

extension UIView {
    func fadeIn(_ type: FadeType = .normal, completed: (() -> ())? = nil) {
        fadeIn(type.rawValue, completed: completed)
    }
    
    /** For typical purpose, use "public func fadeIn(type: FadeType = .Normal, completed: (() -> ())? = nil)" instead of this */
    func fadeIn(_ duration: TimeInterval = FadeType.slow.rawValue, completed: (() -> ())? = nil) {
        alpha = 0
        isHidden = false
        UIView.animate(withDuration: duration,
            animations: {
                self.alpha = 1
            }, completion: { finished in
                completed?()
        }) 
    }
    func fadeOut(_ type: FadeType = .normal, completed: (() -> ())? = nil) {
        fadeOut(type.rawValue, completed: completed)
    }
    /** For typical purpose, use "public func fadeOut(type: FadeType = .Normal, completed: (() -> ())? = nil)" instead of this */
    func fadeOut(_ duration: TimeInterval = FadeType.slow.rawValue, completed: (() -> ())? = nil) {
        UIView.animate(withDuration: duration
            , animations: {
                self.alpha = 0
            }, completion: { [weak self] finished in
                self?.isHidden = true
                self?.alpha = 1
                completed?()
        }) 
    }
}
