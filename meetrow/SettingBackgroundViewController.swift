//
//  SettingBackgroundViewController.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2016/03/18.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import UIKit
import GoogleMobileAds

class SettingBackgroundViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate,UIAlertViewDelegate, GADBannerViewDelegate {
    
    @IBOutlet weak var pushSwitch: UISwitch!
    @IBOutlet weak var SettingTable: UITableView!
    
    var table_data = Array<TableData>()
    
    struct TableData
    {
        var section:Bool = false
        var data = String()
        var subCategorId = Int()
        var switchOn = Bool()
        init(){}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let staHeight = UIApplication.shared.statusBarFrame.height
        // Cellのマージン.
        SettingTable.contentInset = UIEdgeInsetsMake(0, 0, staHeight + addHeight + (self.navigationController?.navigationBar.frame.size.height)!,0)
        SettingTable.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, staHeight + addHeight + (self.navigationController?.navigationBar.frame.size.height)!,0)
        
        let admobView = GADBannerView(adSize:kGADAdSizeSmartBannerPortrait)
        addHeight = (self.view.frame.width * admobView.frame.height)/admobView.frame.width
        admobView.frame.size = CGSize(width: self.view.frame.width, height: addHeight)
        admobView.frame.origin = CGPoint(x: 0, y: self.view.frame.height-addHeight)
        
        admobView.adUnitID = "ca-app-pub-8256083710740545/3348041715"
        admobView.delegate = self
        admobView.rootViewController = self
        let admobRequest:GADRequest = GADRequest()
        
        if AdMobTest {
            if SimulatorTest {
                admobRequest.testDevices = [kGADSimulatorID]
            }
            else {
                admobRequest.testDevices = [TEST_DEVICE_ID]
            }
            
        }
        admobView.load(admobRequest)
        
        self.view.addSubview(admobView)

        let v:UIView = UIView(frame: CGRect.zero)
        v.backgroundColor = UIColor.clear
        self.SettingTable.tableFooterView = v
        
        self.SettingTable.separatorInset = UIEdgeInsets.zero;
        SettingTable.dataSource = self
        SettingTable.delegate = self
        
        var new_elements:TableData
        
        new_elements = TableData()
        new_elements.data = NSLocalizedString("backgroundmes",comment: "")
        new_elements.section = true
        table_data.append(new_elements)
        
        new_elements = TableData()
        new_elements.data = NSLocalizedString("PlayYouTubeinBackground",comment: "")
        let defaults = UserDefaults.standard
        new_elements.switchOn = defaults.bool(forKey: "background")
        new_elements.section = false
        table_data.append(new_elements)


        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(table_data[indexPath.row].section == false){
            let cell = SettingTable.dequeueReusableCell(withIdentifier: "PushTableViewCell", for: indexPath) as! PushTableViewCell
            cell.TitleLabel.text = table_data[indexPath.row].data
            cell.TitleLabel.textColor = UIColor(hex: "#222831")
            if  table_data[indexPath.row].switchOn == true{
                cell.PushSwitch.isOn = true
            }
            else{
                cell.PushSwitch.isOn = false
            }
            cell.PushSwitch.addTarget(self, action: #selector(SettingBackgroundViewController.onClickSwicth(_:)), for: UIControlEvents.valueChanged)
            return cell
        }else{
            let cell = SettingTable.dequeueReusableCell(withIdentifier: "CustomHeaderCell", for: indexPath) as! CustomHeaderCell
            cell.headerLabel.text = table_data[indexPath.row].data
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.headerLabel.textColor = UIColor(hex: "#222831")
            return cell
        }
        
    }
    
    func onClickSwicth(_ sender: UISwitch) {
        let defaults = UserDefaults.standard
        defaults.set(sender.isOn, forKey: "background")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection    section: Int) -> Int
    {
        return table_data.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(table_data[indexPath.row].section == false){
            return 55
        }else{
            return 55
        }
    }
    
    //    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    //    {
    //        return table_data.count
    //    }
    
    
    //    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //        return 70.0
    //    }
    //    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
    //        let orientation: UIInterfaceOrientationMask = [UIInterfaceOrientationMask.Portrait]
    //        return orientation
    //    }
    
    //指定方向に自動的に変更するか？
    //    override func shouldAutorotate() -> Bool{
    //        return true
    //    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let nv = navigationController {
            // 表示
            nv.setNavigationBarHidden(false, animated: true)
            // タイトルを設定する.
            self.navigationItem.title = NSLocalizedString("PlayYouTubeinBackground",comment: "")
            //            navHeight = (self.navigationController?.navigationBar.frame.size.height)!
        }
    }
    @IBAction func close(_ sender: AnyObject) {
        let newRootVC = UIViewController()
        let navigationController = UINavigationController(rootViewController: newRootVC)
        navigationController.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ table: UITableView, didSelectRowAt indexPath:IndexPath) {
        
        SettingTable?.deselectRow(at: indexPath, animated: true)
        
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        let orientation: UIInterfaceOrientationMask = [UIInterfaceOrientationMask.portrait]
        return orientation
    }
    
    //指定方向に自動的に変更するか？
    override var shouldAutorotate : Bool{
        return true
    }
    
    @IBAction func closeView(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: {})
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
