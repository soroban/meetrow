//
//  Favorite.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2016/02/16.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import Foundation
import UIKit

class Favorite: NSObject {
    var fav: Array<Int>!
    var title: String!
    var uid: String!
    var sub: Array<Int>!
    
    init(fav: Array<Int>, title: String, uid: String, sub: Array<Int>) {
        self.fav = fav
        self.title = title
        self.uid = uid
        self.sub = sub
    }
}
