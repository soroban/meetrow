//
//  TabBarDelegate.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2016/02/16.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import Foundation

@objc protocol TabBarDelegate {
    
    func didSelectTab(_ MyTabBarController: MyTabBarController)
}
