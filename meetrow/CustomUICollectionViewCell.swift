//
//  CustomUICollectionViewCell.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2015/10/02.
//  Copyright © 2015年 大口 尚紀. All rights reserved.
//


import UIKit

class CustomUICollectionViewCell: UICollectionViewCell {
    var imageView:UIImageView = UIImageView()
    var backTextLabel:UILabel = UILabel()
    var textLabel:UILabel = UILabel()
    var readyLabel:UILabel = UILabel()
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.contentView.alpha = 0
        imageView.alpha = 0
        
        let imageWidth =  self.bounds.width
        let imageHeight =  self.bounds.width/1.618
        
        imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: imageWidth, height: imageHeight))
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        
        // UILabelを生成.
        textLabel = UILabel(frame: CGRect(x: 0, y: imageHeight/2.618*1.618, width: imageWidth, height: imageHeight/2.618))
        textLabel.textColor = UIColor.white
        textLabel.numberOfLines = 0
        
        //単語の途中で改行されないようにする
        textLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        textLabel.attributedText = nil
        
        readyLabel = UILabel(frame: CGRect(x: 0,y: 0,width: imageWidth,height: imageHeight))
        backTextLabel = UILabel(frame: CGRect(x: 0, y: 0, width: imageWidth, height: imageHeight))
        
        self.contentView.addSubview(imageView)
        self.contentView.addSubview(backTextLabel)
        self.contentView.addSubview(textLabel)
        self.contentView.addSubview(readyLabel)
        self.contentView.bringSubview(toFront: readyLabel)
        self.contentView.fadeIn(0.15)
    }
}
