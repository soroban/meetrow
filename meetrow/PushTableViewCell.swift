//
//  PushTableViewCell.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2016/03/02.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

//
//  SetingTableViewCell.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2016/02/05.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import UIKit

class PushTableViewCell: UITableViewCell {
    
    
    @IBOutlet var TitleLabel: UILabel!
    @IBOutlet weak var PushSwitch: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
