//
//  SettingNotificationViewController.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2016/03/05.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import UIKit
import GoogleMobileAds

class SettingNotificationViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate, UITextViewDelegate,GADBannerViewDelegate {

    @IBOutlet weak var SettingTable: UITableView!
    
    let notificationCollection = NotificationManager.sharedInstance
    var notification: Array<Notification> = []

    var table_data = Array<TableData>()
    let ref = Firebase(url: backendUrl)
    var selSubCategoryId: Int!

    struct TableData
    {
        var section:Bool = false
        var title = String()
        var data = String()
        var icon = String()
        var date = String()
        var subCategorId = Int()
        var transition = Bool()
        init(){}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let staHeight = UIApplication.shared.statusBarFrame.height
        // Cellのマージン.
        SettingTable.contentInset = UIEdgeInsetsMake(0, 0, staHeight + addHeight + (self.navigationController?.navigationBar.frame.size.height)!,0)
        SettingTable.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, staHeight + addHeight + (self.navigationController?.navigationBar.frame.size.height)!,0)


        SVProgressHUD.show(withStatus: "Loading...", maskType: SVProgressHUDMaskType.black)
        
        let v:UIView = UIView(frame: CGRect.zero)
        v.backgroundColor = UIColor.clear
        self.SettingTable.tableFooterView = v
        
        self.SettingTable.separatorInset = UIEdgeInsets.zero;
        SettingTable.dataSource = self
        SettingTable.delegate = self
        
        let admobView = GADBannerView(adSize:kGADAdSizeSmartBannerPortrait)
        addHeight = (self.view.frame.width * admobView.frame.height)/admobView.frame.width
        admobView.frame.size = CGSize(width: self.view.frame.width, height: addHeight)
        admobView.frame.origin = CGPoint(x: 0, y: self.view.frame.height-addHeight)
        
        admobView.adUnitID = "ca-app-pub-8256083710740545/3348041715"
        admobView.delegate = self
        admobView.rootViewController = self
        let admobRequest:GADRequest = GADRequest()
        
        if AdMobTest {
            if SimulatorTest {
                admobRequest.testDevices = [kGADSimulatorID]
            }
            else {
                admobRequest.testDevices = [TEST_DEVICE_ID]
            }
            
        }
        admobView.load(admobRequest)
        
        Locale.preferredLanguages // ["ja-JP", "en-GB", "en-JP"]
//        let language = NSLocale.preferredLanguages().first
        let lanArr = Locale.preferredLanguages[0].components(separatedBy: "-")
        let callbackNotification = { () -> Void in
            var new_elements:TableData
            self.notification = self.notificationCollection.notifications
            for notification in self.notification{
                new_elements = TableData()
                
                if(Locale.preferredLanguages[0] == "ja-JP" || lanArr.first == "ja"){
                    new_elements.title = notification.title
                    new_elements.data = notification.message
                }else{
                    new_elements.title = notification.title_en
                    new_elements.data = notification.message_en
                }
                new_elements.section = false
                
                new_elements.transition = notification.transition
                if(notification.transition == true){
                    new_elements.subCategorId = notification.sub_ctegory_id!
                }
                let cal = Calendar(identifier: Calendar.Identifier.gregorian)
                let unitFlags: NSCalendar.Unit = [.year, .month, .day, .hour, .minute, .second]
                let components = (cal as NSCalendar).components(unitFlags, from: notification.date as Date, to:  Date(), options: NSCalendar.Options())
                if(components.day! < 3){
                    new_elements.icon = "new"
                }
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy/MM/dd HH:mm"
                new_elements.date = dateFormatter.string(from: notification.date as Date)
                self.table_data.append(new_elements)

            }
            self.view.addSubview(admobView)
            self.SettingTable.reloadData()
            SVProgressHUD.dismiss()
        }
        
        self.notificationCollection.getList(callbackNotification)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = SettingTable.dequeueReusableCell(withIdentifier: "NotificationTableViewCell", for: indexPath) as! NotificationTableViewCell
        
        do {
            let data = table_data[indexPath.row].data.data(using: String.Encoding.utf8, allowLossyConversion: true)
            if let d = data {
                let str = try NSAttributedString(data: d,
                                                 options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                           NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue],
                                                 documentAttributes: nil)
                cell.TitleTextView.attributedText = str
            }
        } catch {
        }
        
        cell.TitleTextView.isUserInteractionEnabled = true
        cell.TitleTextView.dataDetectorTypes = .link
        cell.TitleTextView.isScrollEnabled = false
        cell.TitleTextView.isEditable = false
        cell.TitleTextView.isSelectable = true
        cell.TitleTextView.delegate = self

        
//        cell.TitleLabel.text = table_data[indexPath.row].data
        cell.TitleLabel.text = ""
        cell.TLabel.text = table_data[indexPath.row].title
        cell.DateLabel.text = table_data[indexPath.row].date
        cell.NewIcon.image = UIImage(named:table_data[indexPath.row].icon)
//        cell.isUserInteractionEnabled = table_data[indexPath.row].transition
        cell.isUserInteractionEnabled = true
        cell.selectionStyle = .none
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection    section: Int) -> Int
    {
        return table_data.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 145
    }
    
    //    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    //    {
    //        return table_data.count
    //    }
    
    
    //    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //        return 70.0
    //    }
    //    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
    //        let orientation: UIInterfaceOrientationMask = [UIInterfaceOrientationMask.Portrait]
    //        return orientation
    //    }
    
    //指定方向に自動的に変更するか？
    //    override func shouldAutorotate() -> Bool{
    //        return true
    //    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let nv = navigationController {
            // 表示
            nv.setNavigationBarHidden(false, animated: true)
            // タイトルを設定する.
            self.navigationItem.title = NSLocalizedString("Notifications",comment: "")
            //            navHeight = (self.navigationController?.navigationBar.frame.size.height)!
        }
    }
    @IBAction func close(_ sender: AnyObject) {
        let newRootVC = UIViewController()
        let navigationController = UINavigationController(rootViewController: newRootVC)
        navigationController.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ table: UITableView, didSelectRowAt indexPath:IndexPath) {
//        SettingTable?.deselectRow(at: indexPath, animated: true)
//        self.selSubCategoryId = table_data[indexPath.row].subCategorId
//        performSegue(withIdentifier: "goViewfromNotification", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "goViewfromNotification") {
            let nextVC: ViewController = segue.destination as! ViewController
            nextVC.subCategoryId = self.selSubCategoryId
        }
    }

    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        let orientation: UIInterfaceOrientationMask = [UIInterfaceOrientationMask.portrait]
        return orientation
    }
    
    //指定方向に自動的に変更するか？
    override var shouldAutorotate : Bool{
        return true
    }
    
    @IBAction func closeView(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: {})
    }
    
    func openAppSettingPage() -> Void {
        let osVersion = UIDevice.current.systemVersion
        if osVersion < "8.0" {
            // not supported
        }else{
            let url = URL(string:UIApplicationOpenSettingsURLString)!
            UIApplication.shared.openURL(url)
        }
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
