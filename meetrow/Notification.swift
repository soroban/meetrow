//
//  Notification.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2016/03/05.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import Foundation

class Notification: NSObject {
    var title: String!
    var title_en: String!
    var message: String!
    var message_en: String!
    var date: Date!
    var transition: Bool!
    var sub_ctegory_id: Int?
    
    init(title: String, title_en: String, message: String, message_en: String, date: Date,transition:Bool, sub_ctegory_id:Int?) {
        self.title=title
        self.title_en=title_en
        self.message=message
        self.message_en=message_en
        self.date=date
        self.transition=transition
        self.sub_ctegory_id=sub_ctegory_id
    }
    
    func saveTweet() {
        //後ほどParseのdbに保存する処理を書く
    }
}
