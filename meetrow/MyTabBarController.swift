//
//  OriginalTabBarController.swift
//  pms
//
//  Created by soroban11 on 2014/12/17.
//  Copyright (c) 2014年 soroban. All rights reserved.
//

import UIKit
import GoogleMobileAds

class MyTabBarController: UITabBarController,UITabBarControllerDelegate, GADBannerViewDelegate {
    var app:AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
//    var bannerView:GADBannerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        
        let colorKey = UIColor(hex: "#222831")
        UITabBar.appearance().tintColor = colorKey
        
        let admobView = GADBannerView(adSize:kGADAdSizeSmartBannerPortrait)
        addHeight = (self.view.frame.width * admobView.frame.height)/admobView.frame.width
        admobView.frame.size = CGSize(width: self.view.frame.width, height: addHeight)
        admobView.frame.origin = CGPoint(x: 0, y: self.view.frame.height-addHeight-self.tabBar.frame.size.height)

        admobView.adUnitID = "ca-app-pub-8256083710740545/6128651710"
        admobView.delegate = self
        admobView.rootViewController = self
        admobView.tag = 123456789
        let admobRequest:GADRequest = GADRequest()
        
        if AdMobTest {
            if SimulatorTest {
                admobRequest.testDevices = [kGADSimulatorID]
            }
            else {
                admobRequest.testDevices = [TEST_DEVICE_ID]
            }
            
        }
        admobView.load(admobRequest)
        admobView.isHidden = true
        self.view.addSubview(admobView)

        //広告設定
//        var tabHeight = self.tabBar.frame.size.height
//        bannerView = GADBannerView()
//        bannerView = GADBannerView(adSize:kGADAdSizeBanner)/Users/oogutihaoki/Desktop/toolbar_icon_back@2x.png
//        //var yPoint =  UIScreen.mainScreen().bounds.size.height - tabHeight - bannerView.frame.height
//        var yPoint =  UIScreen.mainScreen().applicationFrame.size.height - tabHeight-bannerView.frame.height+20
//        bannerView.frame = CGRectMake(0, yPoint, UIScreen.mainScreen().bounds.size.width, bannerView.frame.height)
//        bannerView.adUnitID = ""
//        bannerView.rootViewController = self
//
//        self.view.addSubview(bannerView)
//        bannerView.loadRequest(GADRequest())
//        self.view.bringSubviewToFront(self.tabBar)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        let index = self.viewControllers!.index(of: viewController);
        
        if (index == self.selectedIndex) {
            return false;
        }
//        let topColor = UIColor(red:0.07, green:0.13, blue:0.26, alpha:1)
        let topColor = UIColor(red:1.0, green:1.0, blue:1.0, alpha:1)
        //グラデーションの開始色
        let bottomColor = UIColor(red:1.0, green:1.0, blue:1.0, alpha:0.8)
//        let bottomColor = UIColor(red:0.54, green:0.74, blue:0.74, alpha:1)
        
        //グラデーションの色を配列で管理
        let gradientColors: [CGColor] = [topColor.cgColor, bottomColor.cgColor]
        //グラデーションレイヤーを作成
        let gradientLayer: CAGradientLayer = CAGradientLayer()
        
        //グラデーションの色をレイヤーに割り当てる
        gradientLayer.colors = gradientColors
        //グラデーションレイヤーをスクリーンサイズにする
        gradientLayer.frame = self.view.bounds
        
        //グラデーションレイヤーをビューの一番下に配置
        
        self.view.layer.insertSublayer(gradientLayer, at: 0)
        
        let toView = self.viewControllers![index!].view as UIView

        toView.alpha = 0
        UIView.animate(
                withDuration: 0.27,
                delay:0,
                options:UIViewAnimationOptions.curveEaseOut,
                animations: {() -> Void in
                    toView.alpha = 1
                    //self.arrow.center = CGPoint(x:pt.x, y:pt.y - height/2)
                },
            completion:{ _ in
                gradientLayer.removeFromSuperlayer()
        })
        
        return true
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let nav = viewController as! UINavigationController
        if nav.visibleViewController is TabBarDelegate {
            let vc = nav.visibleViewController as! TabBarDelegate
            vc.didSelectTab(self)
        }
    }
    
}

extension UITabBarController {
    override open var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if self.selectedViewController != nil{
            if self.selectedViewController!.responds(to: #selector(getter: UIViewController.supportedInterfaceOrientations)){
                return self.selectedViewController!.supportedInterfaceOrientations
            }
        }
        return UIInterfaceOrientationMask.all
    }
    open override var shouldAutorotate : Bool {
        if self.selectedViewController != nil{
            if self.selectedViewController!.responds(to: #selector(getter: UIViewController.shouldAutorotate)) {
                return self.selectedViewController!.shouldAutorotate
            }
        }
        return true
    }
}
