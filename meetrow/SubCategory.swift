//
//  SubCategory.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2015/10/15.
//  Copyright © 2015年 大口 尚紀. All rights reserved.
//

import Foundation
import UIKit

class SubCategory: NSObject {
    var sub_ctegory_id: Int!
    var ctegory_id: Int!
    var name: String!
    var name_sub: String!
    var image: String!
    var state: Int!
    var type: Int!
    var licence: String!
    var licencelink: String!
    var day:Array<String>!
    var center_id:Int!
    var ticket: Array<Dictionary<String,Any>>!
    var lrAdd:Dictionary<String,Any>!
    init(sub_ctegory_id: Int, ctegory_id: Int, image: String, name:String, name_sub:String, state:Int, type:Int, licence:String?, licencelink:String?, day:Array<String>?, center_id:Int,ticket:Array<Dictionary<String,Any>>?, lrAdd:Dictionary<String,Any>?) {
        self.sub_ctegory_id=sub_ctegory_id
        self.ctegory_id=ctegory_id
        self.image=image
        self.name=name
        self.name_sub=name_sub
        self.state=state
        self.type=type
        self.licence=licence
        self.licencelink=licencelink
        self.day=day
        self.center_id=center_id
        self.ticket = ticket
        self.lrAdd = lrAdd
    }
}
