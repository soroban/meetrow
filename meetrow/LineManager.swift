//
//  LineManager.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2015/09/01.
//  Copyright (c) 2015年 大口 尚紀. All rights reserved.
//

import UIKit

class LineManager: NSObject {
    
    var lines = Dictionary<String,Array<Line>>()
    static let sharedInstance = LineManager()
    
    var myRootRef: Firebase!
    
    func getLines(_ callback: @escaping () -> Void, subCategoryId:Int) {
        // Firebaseへ接続
        lines = [:]
        self.myRootRef = Firebase(url: backendUrl + "line/line_" + String(subCategoryId))
        self.myRootRef.observeSingleEvent(of: FEventType.value, with: { lines in
            var preColor:String = ""
            var tmpArray: Array<Line> = []

            for line in lines?.children.allObjects as! [FDataSnapshot] {
                let value = line.value as? NSDictionary
                if let im = value?["im"] as? Int,
                    let il = value?["il"] as? Int,
                    let jm = value?["jm"] as? Int,
                    let jl = value?["jl"] as? Int,
                    let direct = value?["direct"] as? Int,
                    let color = value?["color"] as? String
                {
                    if preColor != color{
                        if(tmpArray.count > 1){
                            self.lines[preColor] = tmpArray
                        }
                        
                        preColor = color
                        tmpArray = []
                    }
                    let line = Line(il:il, im:im, jl:jl, jm:jm, direct:direct, color:color)
                    tmpArray.append(line)
                }
            }
            self.lines[preColor] = tmpArray
            callback()
        })
    }
}
