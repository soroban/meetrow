//
//  line.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2015/09/01.
//  Copyright (c) 2015年 大口 尚紀. All rights reserved.
//

import Foundation
import UIKit

class Line: NSObject {
    var il: Int!
    var im: Int!
    var jl: Int!
    var jm: Int!
    var direct: Int!
    var color: String!
    
    init(il: Int, im: Int, jl: Int, jm: Int, direct: Int, color: String) {
        self.il=il
        self.im=im
        self.jl=jl
        self.jm=jm
        self.direct=direct
        self.color = color
    }
    
    func saveTweet() {
        //後ほどParseのdbに保存する処理を書く
    }
}