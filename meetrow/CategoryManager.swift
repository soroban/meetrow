//
//  LineManager.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2015/09/01.
//  Copyright (c) 2015年 大口 尚紀. All rights reserved.
//

import UIKit

class CategoryManager: NSObject {
    
    var categories: Array<Category> = []
    static let sharedInstance = CategoryManager()
    
    var myRootRef: Firebase!
    
    func getCategories(_ callback: @escaping () -> Void, state:Array<Int>){
        // Firebaseへ接続
        self.myRootRef = Firebase(url: backendUrl + "category")
        self.categories = []
        self.myRootRef.queryOrdered(byChild: "order").observeSingleEvent(of: FEventType.value, with: { categories in
            for category in categories?.children.allObjects as! [FDataSnapshot] {
                let value = category.value as? NSDictionary
                let filtered = state.filter { $0 == value?["state"] as? Int }
                if(filtered.count > 0){
                    if let category_id = value?["category_id"] as? Int,
                        let name = value?["name"] as? String,
                        let name_en = value?["name_en"] as? String,
                        let image = value?["image"] as? String,
                        let state = value?["state"] as? Int
                    {
                        let category = Category(category_id:category_id, image:image, name:name, name_en:name_en, state:state)
                        self.categories.append(category)
                    }
                }
            }
            callback()
        })
    }
}
