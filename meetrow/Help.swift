//
//  Help.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2016/03/06.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import Foundation

class Help: NSObject {
    var title: String!
    var title_en: String!
    var message: String!
    var message_en: String!
    var help_id: Int?
    var help_sub_id: Int?
    
    init(title: String, title_en: String, message: String, message_en: String, help_id:Int?, help_sub_id:Int?) {
        self.title=title
        self.title_en=title_en
        self.message=message
        self.message_en=message_en
        self.help_id=help_id
        self.help_sub_id=help_sub_id
    }
    
    func saveTweet() {
        //後ほどParseのdbに保存する処理を書く
    }
}