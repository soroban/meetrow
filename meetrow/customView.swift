//
//  CustomView.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2015/10/01.
//  Copyright © 2015年 大口 尚紀. All rights reserved.
//

import Foundation

class customView: UIView {
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches {
            let t: UITouch = touch as! UITouch
            if t.view!.tag > 1000 && t.view!.tag < 2000 {
                //self.performSegueWithIdentifier("DetailViewSegue", sender: nil)
                ViewController().performSegue(withIdentifier: "DetailViewController",sender: ViewController())
           //     NSLog(String(t.view!.tag))
            }
        }
    }
}

/**
*  デリゲートはクラスのみ
*/
protocol CustomViewDelegate: class {
}
