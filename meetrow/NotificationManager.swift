//
//  NotificationManager.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2016/03/05.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import Foundation

class NotificationManager: NSObject {
    
    var notifications =  Array<Notification>()
    static let sharedInstance = NotificationManager()
    
    var myRootRef: Firebase!
    
    func getList(_ callback: @escaping () -> Void) {
        // Firebaseへ接続
        self.myRootRef = Firebase(url: backendUrl + "notification")
        self.notifications = []
        self.myRootRef.queryOrdered(byChild: "order").queryLimited(toLast: 10).observeSingleEvent(of: FEventType.value, with: { notifications in
            for notification in notifications?.children.allObjects as! [FDataSnapshot] {
                let value = notification.value as? NSDictionary
                if let title = value!["title"] as? String,
                    let title_en = value!["title_en"] as? String,
                    let message = value!["message"] as? String,
                    let message_en = value!["message_en"] as? String,
                    let dateStr = value!["date"] as? String,
                    let transition = value!["transition"] as? Bool
                {
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy/MM/dd HH:mm:ss"
                    let date = dateFormatter.date(from: dateStr)!
                    let sub_ctegory_id = value!["sub_category_id"] as? Int
                    let notification = Notification(title: title, title_en: title_en, message: message, message_en: message_en, date: date, transition:transition, sub_ctegory_id:sub_ctegory_id)
                    self.notifications.append(notification)
                }
            }
            callback()
        })
    }
}
