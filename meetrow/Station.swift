//
//  Station.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2015/09/01.
//  Copyright (c) 2015年 大口 尚紀. All rights reserved.
//

import Foundation
import UIKit

class Station: NSObject {
    var l: Int!
    var m: Int!
    var music_id: Int!
    var jp_name: String!
    var en_name: String!
    var full: Array<String>!
    var jp: Bool!
    var day: Array<String>!
    var no_disp: Bool!
    
    init(l: Int, m: Int, music_id: Int, jp_name: String, en_name: String, full: Array<String>, jp:Bool, day: Array<String>?, no_disp:Bool!) {
        self.l=l
        self.m=m
        self.music_id=music_id
        self.jp_name=jp_name
        self.en_name = en_name
        self.full = full
        self.jp = jp
        self.day = day
        self.no_disp = no_disp
    }
    
    func saveTweet() {
        //後ほどParseのdbに保存する処理を書く
    }
}