//
//  line.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2015/09/01.
//  Copyright (c) 2015年 大口 尚紀. All rights reserved.
//

import Foundation
import UIKit

class Category: NSObject {
    var category_id: Int!
    var name: String!
    var name_en: String!
    var image: String!
    var state: Int!
    
    init(category_id: Int, image: String, name:String, name_en:String,state:Int) {
        self.category_id=category_id
        self.image=image
        self.name=name
        self.name_en=name_en
        self.state=state
    }
}