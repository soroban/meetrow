//
//  SettingPushViewController.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2016/02/26.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import UIKit
import GoogleMobileAds

class SettingPushViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate,UIAlertViewDelegate, GADBannerViewDelegate {

    @IBOutlet weak var pushSwitch: UISwitch!
    @IBOutlet weak var SettingTable: UITableView!
    
    let cateogryCollection = CategoryManager.sharedInstance
    var category: Array<Category> = []

    let subCateogryCollection = SubCategoryManager.sharedInstance
    var subCategory: Array<SubCategory> = []
    
    let pushSettingCollection = PushSettingManager.sharedInstance
    var pushSetting: Array<PushSetting> = []

    var table_data = Array<TableData>()
    let ref = Firebase(url: backendUrl)
    var set = true
    
    struct TableData
    {
        var section:Bool = false
        var data = String()
        var subCategorId = Int()
        var switchOn = Bool()
        init(){}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let staHeight = UIApplication.shared.statusBarFrame.height
        // Cellのマージン.
        SettingTable.contentInset = UIEdgeInsetsMake(0, 0, staHeight + addHeight + (self.navigationController?.navigationBar.frame.size.height)!,0)
        SettingTable.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, staHeight + addHeight + (self.navigationController?.navigationBar.frame.size.height)!,0)
        
        let admobView = GADBannerView(adSize:kGADAdSizeSmartBannerPortrait)
        addHeight = (self.view.frame.width * admobView.frame.height)/admobView.frame.width
        admobView.frame.size = CGSize(width: self.view.frame.width, height: addHeight)
        admobView.frame.origin = CGPoint(x: 0, y: self.view.frame.height-addHeight)
        
        admobView.adUnitID =  "ca-app-pub-8256083710740545/3348041715"
        admobView.delegate = self
        admobView.rootViewController = self
        let admobRequest:GADRequest = GADRequest()
        
        if AdMobTest {
            if SimulatorTest {
                admobRequest.testDevices = [kGADSimulatorID]
            }
            else {
                admobRequest.testDevices = [TEST_DEVICE_ID]
            }
            
        }
        admobView.load(admobRequest)

        
        self.set = true
        let settings = UIApplication.shared.currentUserNotificationSettings
        if settings == nil {
            self.set = false
        }else if(settings!.types.rawValue == 0){
            self.set = false
        }

//        if(self.set == false){
//            let callback = { () -> Void in}
//            pushSettingCollection.new(callback, device_token: deviceTokenString, uuid: UIDevice.currentDevice().identifierForVendor!.UUIDString, set:self.set)
//        }
        
        let osVersion = UIDevice.current.systemVersion
        if osVersion > "8.0" {
            if(set == false){
                let alertController:MyUIAlertController = MyUIAlertController(title: NSLocalizedString("pushtitle",comment: ""), message: NSLocalizedString("pushmessage",comment: ""), preferredStyle: .alert)
                let otherAction = UIAlertAction(title: NSLocalizedString("gotoSettings",comment: ""), style: .default) {
                    action in self.openAppSettingPage()
                }
                let cancelAction = UIAlertAction(title: NSLocalizedString("later",comment: ""), style: .cancel) {
                    action in 
                }
            
                alertController.addAction(otherAction)
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
        
        SVProgressHUD.show(withStatus: "Loading...", maskType: SVProgressHUDMaskType.black)
        
        let v:UIView = UIView(frame: CGRect.zero)
        v.backgroundColor = UIColor.clear
        self.SettingTable.tableFooterView = v
        
        self.SettingTable.separatorInset = UIEdgeInsets.zero;
        SettingTable.dataSource = self
        SettingTable.delegate = self
        
        let callbackSetting = { () -> Void in
            self.pushSetting = self.pushSettingCollection.pushSettings
            
            let callbackCategory = { () -> Void in
                self.category = self.cateogryCollection.categories
                let lanArr = Locale.preferredLanguages[0].components(separatedBy: "-")
                for category in self.category{
                    var new_elements:TableData
                    new_elements = TableData()
                    
                    if((Locale.preferredLanguages[0] == "ja-JP" || lanArr.first == "ja")){
                        new_elements.data = category.name
                    }else{
                        new_elements.data = category.name_en
                    }
                    
                    new_elements.section = true
                    self.table_data.append(new_elements)
                
                    let callbackSubCategory = { () -> Void in
                        self.subCategory = self.subCateogryCollection.subCategories
                        var setting: Dictionary<String, Bool> = [:]
                        
                        for subCategory in self.subCategory{
                            setting[String(subCategory.sub_ctegory_id)] = false
                        }

                        for pushSetting in self.pushSetting{
                            for (key, value) in pushSetting.setting{
                                if(value == true){
                                    let sub_category_id = key.replacingOccurrences(of: "s_", with: "", options: [], range: nil)
                                    if setting[sub_category_id] != nil{
                                        setting[sub_category_id] = true
                                    }
                                }
                            }
                        }

                        let lanArr = Locale.preferredLanguages[0].components(separatedBy: "-")
                        for subCategory in self.subCategory{
                            new_elements = TableData()
                            if((Locale.preferredLanguages[0] == "ja-JP" || lanArr.first == "ja")){
                                new_elements.data = subCategory.name
                            }else{
                                new_elements.data = subCategory.name_sub
                            }
                            new_elements.section = false
                            new_elements.subCategorId = subCategory.sub_ctegory_id
                            new_elements.switchOn = setting[String(subCategory.sub_ctegory_id)]!
                            self.table_data.append(new_elements)
                        }
                        self.view.addSubview(admobView)
                        self.SettingTable.reloadData()
                        SVProgressHUD.dismiss()
                    }
                    self.subCateogryCollection.getSubCategories(callbackSubCategory, categoryId:category.category_id, state:[2])
                }
            }
            self.cateogryCollection.getCategories(callbackCategory, state:[2])
        }
        
        self.pushSettingCollection.getFromTokenAndUuid(callbackSetting, device_token: deviceTokenString, uuid:  UIDevice.current.identifierForVendor!.uuidString)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(table_data[indexPath.row].section == false){
            let cell = SettingTable.dequeueReusableCell(withIdentifier: "PushTableViewCell", for: indexPath) as! PushTableViewCell
            cell.TitleLabel.text = table_data[indexPath.row].data
            cell.TitleLabel.textColor = UIColor(hex: "#222831")
            if  table_data[indexPath.row].switchOn == true{
                cell.PushSwitch.isOn = true
            }
            else{
                cell.PushSwitch.isOn = false
            }
//            cell.PushSwitch.addTarget(self, action: "onClickSwicth:", forControlEvents: UIControlEvents.ValueChanged)
            return cell
        }else{
            let cell = SettingTable.dequeueReusableCell(withIdentifier: "CustomHeaderCell", for: indexPath) as! CustomHeaderCell
            cell.headerLabel.text = table_data[indexPath.row].data
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.headerLabel.textColor = UIColor(hex: "#222831")
            return cell
        }
        
    }
    
    func onClickSwicth(_ sender: UISwitch) {
        self.set = true
        let settings = UIApplication.shared.currentUserNotificationSettings
        if settings == nil {
            self.set = false
        }else if(settings!.types.rawValue == 0){
            self.set = false
        }
        
        if (sender.isOn == true && self.set == false) {
            sender.isOn = false
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection    section: Int) -> Int
    {
        return table_data.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(table_data[indexPath.row].section == false){
            return 55
        }else{
            return 55
        }
    }
    
    //    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    //    {
    //        return table_data.count
    //    }
    
    
    //    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //        return 70.0
    //    }
    //    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
    //        let orientation: UIInterfaceOrientationMask = [UIInterfaceOrientationMask.Portrait]
    //        return orientation
    //    }
    
    //指定方向に自動的に変更するか？
    //    override func shouldAutorotate() -> Bool{
    //        return true
    //    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let nv = navigationController {
            // 表示
            nv.setNavigationBarHidden(false, animated: true)
            // タイトルを設定する.
            self.navigationItem.title = NSLocalizedString("PushNotificationSettings",comment: "")
            //            navHeight = (self.navigationController?.navigationBar.frame.size.height)!
        }
    }
    @IBAction func close(_ sender: AnyObject) {
        let newRootVC = UIViewController()
        let navigationController = UINavigationController(rootViewController: newRootVC)
        navigationController.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ table: UITableView, didSelectRowAt indexPath:IndexPath) {
        
        SettingTable?.deselectRow(at: indexPath, animated: true)
        
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        let orientation: UIInterfaceOrientationMask = [UIInterfaceOrientationMask.portrait]
        return orientation
    }
    
    //指定方向に自動的に変更するか？
    override var shouldAutorotate : Bool{
        return true
    }

    @IBAction func closeView(_ sender: AnyObject) {
        SVProgressHUD.show(withStatus: "Loading...", maskType: SVProgressHUDMaskType.black)
        
        var updateSetting:Dictionary<String, Bool> = [:]
        for i in 1...self.table_data.count-1 {
        //for (var i=0; i < self.table_data.count; i++) {
            if (self.table_data[i].section == false){
                let indexPath = IndexPath(row: i, section: 0)
                let cell = self.SettingTable.cellForRow(at: indexPath) as! PushTableViewCell
                if cell.PushSwitch.isOn == true {
                    updateSetting["s_" + String(self.table_data[i].subCategorId)] = true
                } else {
                    updateSetting["s_" +  String(self.table_data[i].subCategorId)] = false
                }
            }
        }
        let callback = { () -> Void in}
        if(deviceTokenString != ""){
            pushSettingCollection.save(callback, device_token: deviceTokenString, uuid: UIDevice.current.identifierForVendor!.uuidString, updateSetting: updateSetting)
        }
        
        self.category = []
        self.subCategory = []
        self.dismiss(animated: true, completion: { SVProgressHUD.dismiss() })
    }
    
    func openAppSettingPage() -> Void {
        let osVersion = UIDevice.current.systemVersion
        if osVersion < "8.0" {
            // not supported
        }else{
            let url = URL(string:UIApplicationOpenSettingsURLString)!
            UIApplication.shared.openURL(url)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
