//
//  LoginViewController.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2016/02/07.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit
import Fabric
import TwitterKit

class LoginViewController: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate{

    let ref = Firebase(url: backendUrl)
    var delegate: FavoriteViewControllerDelegate!
    var detailViewDelegate: DetailViewControllerDelegate!
    
    @IBOutlet var loginImage: UIImageView!
    @IBOutlet weak var facebookLogin: UILabel!
    @IBOutlet weak var googleLogin: UILabel!
    @IBOutlet weak var twitterLogin: UILabel!
    @IBOutlet weak var guideLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.loginImage.image = UIImage(named: "fav.jpeg")! as UIImage
        self.facebookLogin.isUserInteractionEnabled = true
        self.googleLogin.isUserInteractionEnabled = true
        self.twitterLogin.isUserInteractionEnabled = true
        
        // Setup delegates
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        //GIDSignIn.sharedInstance().signInSilently()
        // Attempt to sign in silently, this will succeed if
        // the user has recently been authenticated
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }

    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        let orientation: UIInterfaceOrientationMask = [UIInterfaceOrientationMask.portrait]
        return orientation
    }
    
    //指定方向に自動的に変更するか？
    override var shouldAutorotate : Bool{
        return true
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        for touch: UITouch in touches {
            let tag = touch.view!.tag
            switch tag {
            case 100001:
                self.loginWithFacebook()
            case 100002:
                self.loginWithGoogle()
            case 100003:
                self.loginWithTwitter()
            default:
                break
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }

    func loginWithGoogle(){
        GIDSignIn.sharedInstance().signIn()
    }
    
    func loginWithFacebook(){
        let facebookLogin = FBSDKLoginManager()
        facebookLogin.loginBehavior = FBSDKLoginBehavior.systemAccount;
        facebookLogin.logIn(withReadPermissions: ["email", "public_profile"], from: self) { (facebookResult, facebookError) -> Void in
            if facebookError != nil {
                print("Facebook login failed. Error \(facebookError)")
            } else if (facebookResult?.isCancelled)! {
                print("Facebook login was cancelled.")
            } else {
                DispatchQueue.main.async {
                    SVProgressHUD.show(withStatus: "Loading...", maskType: SVProgressHUDMaskType.black)
                }
                self.getAuthFirebaseOnFacebook()
            }
        }
    }
    
    func getAuthFirebaseOnFacebook(){
        let accessToken = FBSDKAccessToken.current().tokenString
        self.ref?.auth(withOAuthProvider: "facebook", token: accessToken,
            withCompletionBlock: { error, authData in
                if error != nil {
                    print("Facebook Login failed. \(error)")
                    //self.loginStatus.text = "Not Logged In"
                } else {
                    //print(authData.faceboo);
                    print("Facebook  Logged in!")
                    if(self.delegate != nil){
                        self.delegate.completeLogin()
                        self.dismiss(animated: true, completion: {})
                    }
                    else{
                        self.dismiss(animated: true, completion: {
                            self.detailViewDelegate.loginComp()
                            SVProgressHUD.dismiss()
                        })
                    }
                }
            }
        )
    }
    
    func loginWithTwitter(){
        DispatchQueue.main.async {
            SVProgressHUD.show(withStatus: "Loading...", maskType: SVProgressHUDMaskType.black)
        }

        Twitter.sharedInstance().logIn { session, error in
            if (session != nil) {
                self.getAuthFirebaseOnTwitter()
            } else {
                SVProgressHUD.dismiss()
                print("error: \(error!.localizedDescription)");
            }
        }
    }
    
    func getAuthFirebaseOnTwitter(){
        let store = Twitter.sharedInstance().sessionStore
        let params = [
            "oauth_token": store.session()!.authToken,
            "oauth_token_secret": store.session()!.authTokenSecret,
            "user_id": store.session()!.userID
        ]
        self.ref?.auth(withOAuthProvider: "twitter", parameters: params,
            withCompletionBlock: { error, authData in
                if error != nil {
                    SVProgressHUD.dismiss()
                    print("Twitter Login failed. \(error)")
                    //self.loginStatus.text = "Not Logged In"
                } else {
                    print(authData);
                    print("Twitter Logged in!")
                    if(self.delegate != nil){
                        self.delegate.completeLogin()
                        self.dismiss(animated: true, completion: {})
                    }
                    else{
                        self.dismiss(animated: true, completion: {
                            self.detailViewDelegate.loginComp()
                            SVProgressHUD.dismiss()
                        })
                    }

                    // The logged in user's unique identifier
                }
            }
        )
    }

    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
        withError error: Error!) {
            if (error == nil) {
                self.getAuthFirebaseOnGoogle(didSignInForUser: user)
            }
            else{
                SVProgressHUD.dismiss()
                // Don't assert this error it is commonly returned as nil
                print("\(error.localizedDescription)")
            }
    }
    
    func getAuthFirebaseOnGoogle(didSignInForUser user: GIDGoogleUser!){
        self.ref?.auth(withOAuthProvider: "google", token: user.authentication.accessToken,
            withCompletionBlock: { error, authData in
                if error != nil {
                    SVProgressHUD.dismiss()
                    print("Google Login failed. \(error)")
                } else {
                    print(authData);
                    print("Google Logged in!")
                    if(self.delegate != nil){
                        self.delegate.completeLogin()
                        self.dismiss(animated: true, completion: {})
                    }
                    else{
                        self.dismiss(animated: true, completion: {
                            self.detailViewDelegate.loginComp()
                            SVProgressHUD.dismiss()
                        })
                    }
                }
            }
        )
    }
    
    // Implement the required GIDSignInDelegate methods
    // Unauth when disconnected from Google
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user:GIDGoogleUser!,
        withError error: NSError!) {
            SVProgressHUD.dismiss()
            self.ref?.unauth();
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        // サインウインドウが閉じたあとの処理
        self.dismiss(animated: true, completion: nil)
        //dispatch_async(dispatch_get_main_queue()) {
        SVProgressHUD.show(withStatus: "Loading...", maskType: SVProgressHUDMaskType.black)
        //}
    }
    
    @IBAction func tapClose(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
}
