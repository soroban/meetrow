//
//  FavoriteListViewController.swift
//  Meetrow
//
//  Created by 大口 尚紀 on 2016/05/20.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//


protocol FavoriteListViewControllerDelegate{
    func didFinished(_ next: Int)
}

class FavoriteListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate,UIAlertViewDelegate,FavoriteListViewControllerDelegate,UIViewControllerTransitioningDelegate{
    
    @IBOutlet weak var favoriteTable: UITableView!
    var favoriteCollection = FavoriteManager.sharedInstance
    var favorites: Array<Favorite> = []

    var subCategoryId: Int!
    var table_data = Array<TableData>()
    let ref = Firebase(url: backendUrl)
    var tabHeight:CGFloat!
    var navHeight:CGFloat!
    var subCategoryName:String!
    var selIndex:Int!
    
    struct TableData
    {
        var name = String()
        var en_name = String()
        var jp_name = String()
        var day = Array<String>()
        var time = Array<String>()
        var stage = Array<String>()
        var music_id = Int()
        var jp = Bool()
        init(){}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        navigationItem.rightBarButtonItem = editButtonItem
        SVProgressHUD.show(withStatus: "Loading...", maskType: SVProgressHUDMaskType.black)

        let staHeight = UIApplication.shared.statusBarFrame.height
        self.favoriteTable.contentInset = UIEdgeInsetsMake(0, 0, staHeight + addHeight + self.tabHeight+16,0)
        self.favoriteTable.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, staHeight + addHeight + self.tabHeight+16,0)
        
        self.favoriteTable.dataSource = self
        self.favoriteTable.delegate = self

        let v:UIView = UIView(frame: CGRect.zero)
        v.backgroundColor = UIColor.clear
        self.favoriteTable.tableFooterView = v
        
        let lanArr = Locale.preferredLanguages[0].components(separatedBy: "-")
        
        self.table_data = Array<TableData>()
        let callbackFavorite = { () -> Void in
            let detailCollection = Detail1Manager.sharedInstance
            
            self.favorites = self.favoriteCollection.favorites
            for favorite in self.favorites{
                let callbackDetail = { () -> Void in
                    let details = detailCollection.detailList

                    var key=0
                    for detail in details!{
                        var new_elements:TableData = TableData()
                        if((Locale.preferredLanguages[0] == "ja-JP" || lanArr.first == "ja") && detail.jp == true){
                            new_elements.name = detail.jp_name
                        }else{
                            new_elements.name = detail.en_name
                        }
                        new_elements.en_name = detail.en_name
                        new_elements.jp_name = detail.jp_name
                        new_elements.music_id = detail.id
                        new_elements.jp = detail.jp
                        
                        if(detail.day != nil){
                            new_elements.day = detail.day
                        }
                        
                        if(detail.time != nil){
                            new_elements.time = detail.time
                        }
                        
                        if(detail.stage != nil){
                            new_elements.stage = detail.stage
                        }
                        
                        let cellName = "FavoriteListTableViewCell" + String(key)
                        self.favoriteTable.register(FavoriteListTableViewCell.self, forCellReuseIdentifier: cellName)
                        self.table_data.append(new_elements)
                        key += 1
                    }
                    
                    self.favoriteTable.reloadData()
                    let imageWidth = self.view.frame.width
                    let labelHeight = self.tabHeight+16
                    
                    let subCategoryLabel = SubCaLabel(frame: CGRect(x: 0, y: 0, width: imageWidth, height: labelHeight))
                    
                    //        if (self.dayArr.count > 0){
                    subCategoryLabel.padding = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10)
                    //        }else{
                    //            subCategoryLabel.padding = UIEdgeInsets(top: 5, left: 20, bottom: 5, right: 20)
                    //        }
                    subCategoryLabel.textColor = UIColor(hex: "#222831")
                    //subCategoryLabel.textColor = UIColor.grayColor()
//                    if(imageWidth < 375){
//                        subCategoryLabel.font = UIFont.boldSystemFontOfSize(18)
//                    }else{
                        subCategoryLabel.font = UIFont.boldSystemFont(ofSize: 21)
//                    }
                    subCategoryLabel.numberOfLines = 2
                    subCategoryLabel.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha:0.99)
                    
                    subCategoryLabel.layer.position = CGPoint(x: (self.view.frame.width/2), y:self.view.frame.height-(labelHeight/2))
                    subCategoryLabel.text = self.subCategoryName
                    subCategoryLabel.textAlignment = .center
                    subCategoryLabel.adjustsFontSizeToFitWidth = true
                    subCategoryLabel.minimumScaleFactor = 0.8
                    
                    self.view.addSubview(subCategoryLabel)


                    SVProgressHUD.dismiss()        
                }
                    
                detailCollection.getDetails(callback: callbackDetail, subCategoryId: self.subCategoryId, ids: favorite.fav)
            }
        }
        
        //if(self.ref.authData != nil){
        self.favoriteCollection.getFavorite(callbackFavorite, uid: guuid, SubCategoryId: self.subCategoryId)
        //}
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        self.favoriteTable.isEditing = editing
        if(editing == false){
            SVProgressHUD.show(withStatus: "Loading...", maskType: SVProgressHUDMaskType.black)
            
            var musicIds = [Int]()
            for data in table_data{
                musicIds.append(data.music_id)
            }
            let callbackFavorite = { () -> Void in
                if(self.table_data.count <= 0){
                    self.navigationItem.rightBarButtonItem = nil
                }
                SVProgressHUD.dismiss()
            }
            //if(self.ref.authData != nil){
            
            self.favoriteCollection.saveFavorite(callbackFavorite, uid: guuid, SubCategoryId: self.subCategoryId, musicIds: musicIds)
            //}
            
        }
        self.favoriteTable.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let target = self.table_data[sourceIndexPath.row]
        self.table_data.remove(at: sourceIndexPath.row)
        self.table_data.insert(target, at: destinationIndexPath.row)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete {
            self.table_data.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        }
    }

    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellName = "FavoriteListTableViewCell" + String(indexPath.row)
        let cell = self.favoriteTable.dequeueReusableCell(withIdentifier: cellName, for: indexPath) as! FavoriteListTableViewCell
        
        //if(cell.name.text == nil){
        
            var disp = true
            var first = false
            var last = false
            if(self.table_data.count == 1){
                disp = false
            }
        
            if(indexPath.row == self.table_data.count-1){
                last = true
            }
            if(indexPath.row == 0){
                first = true
            }
        
            if(tableView.isEditing){
                cell.editCell(self.table_data[indexPath.row].music_id, index: indexPath.row, name: self.table_data[indexPath.row].name)
            }else{
                cell.setCell(self.table_data[indexPath.row].music_id, index: indexPath.row, disp: disp, first: first, last: last, name:self.table_data[indexPath.row].name, dayArr:self.table_data[indexPath.row].day, timeArr:self.table_data[indexPath.row].time, stageArr:self.table_data[indexPath.row].stage)
            }
//        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection    section: Int) -> Int
    {
        return table_data.count
    }
    
    func tableView(_ table: UITableView, didSelectRowAt indexPath:IndexPath) {
        table.deselectRow(at: indexPath, animated: true)
        self.selIndex = indexPath.row
        
        let target = self.table_data[indexPath.row]
        
        let nex : DetailViewController = self.storyboard!.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        nex.id = target.music_id
        nex.subCategoryId = self.subCategoryId
        nex.navHeight = self.navHeight
        nex.modalPresentationStyle = .custom
        nex.transitioningDelegate = self
        nex.favDelegate = self
        
        if(self.table_data.count > 1){
            if(indexPath.row == 0){
                let from = self.table_data[indexPath.row];
                let next = self.table_data[indexPath.row+1];
                //次だけ
                let nextRelation = Relation(from_id: from.music_id, from_jp_name: from.jp_name, from_en_name: from.en_name, to_id: next.music_id, to_jp_name: next.jp_name, to_en_name: next.en_name, direct: 0, color: "#222831", jp:next.jp)
                nex.favRelations.append(nextRelation)
            }else if(indexPath.row == self.table_data.count-1){
                let prev = self.table_data[indexPath.row-1];
                let from = self.table_data[indexPath.row];
                
                let prevRelation = Relation(from_id: from.music_id, from_jp_name: from.jp_name, from_en_name: from.en_name, to_id: prev.music_id, to_jp_name: prev.jp_name, to_en_name: prev.en_name, direct: 180, color: "#222831", jp:prev.jp)
                nex.favRelations.append(prevRelation)

            }else{
                let prev = self.table_data[indexPath.row-1];
                let from = self.table_data[indexPath.row];
                let next = self.table_data[indexPath.row+1];
                
                let nextRelation = Relation(from_id: from.music_id, from_jp_name: from.jp_name, from_en_name: from.en_name, to_id: next.music_id, to_jp_name: next.jp_name, to_en_name: next.en_name, direct: 0, color: "#222831", jp:next.jp)
                let prevRelation = Relation(from_id: from.music_id, from_jp_name: from.jp_name, from_en_name: from.en_name, to_id: prev.music_id, to_jp_name: prev.jp_name, to_en_name: prev.en_name, direct: 180, color: "#222831", jp:prev.jp)
                nex.favRelations.append(nextRelation)
                nex.favRelations.append(prevRelation)
            }
        }
        
        self.present(nex, animated: true, completion: {})

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        if(table_data[indexPath.row].section == false){
        //            return
        //        }else{
        //            return 60
        //        }
        return 115
    }
    
    //    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    //    {
    //        return table_data.count
    //    }
    
    
    //    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //        return 70.0
    //    }
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        let orientation: UIInterfaceOrientationMask = [UIInterfaceOrientationMask.portrait]
        return orientation
    }
    
    //指定方向に自動的に変更するか？
    override var shouldAutorotate : Bool{
        return true
    }
    
    @IBAction func closeView(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: {})
    }
    override func viewDidAppear(_ animated: Bool) {
        for view in self.tabBarController!.view.subviews {
            if(view.tag == 123456789){
                view.isHidden = false
            }
        }

        self.favoriteTable.separatorInset = UIEdgeInsets.zero;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        for view in self.tabBarController!.view.subviews {
            if(view.tag == 123456789){
                view.isHidden = true
                view.frame.origin = CGPoint(x: 0, y: self.view.frame.height-view.frame.height-(tabHeight+16)+1)
            }
        }

        if let nv = navigationController {
            // 表示
            nv.setNavigationBarHidden(false, animated: true)
            // タイトルを設定する.
            //self.navigationItem.title = NSLocalizedString("Help",comment: "")
            self.navHeight = (self.navigationController?.navigationBar.frame.size.height)!
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        for view in self.tabBarController!.view.subviews {
            if(view.tag == 123456789){
                view.frame.origin = CGPoint(x: 0, y: self.view.frame.height-view.frame.height-(tabHeight)+1)
            }
        }
    }

    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        
        return CustomPresentationController(presentedViewController: presented, presenting: presenting)
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return CustomAnimatedTransitioning(isPresent: true)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return CustomAnimatedTransitioning(isPresent: false)
    }

    func didFinished(_ next:Int){
        var indexPath = IndexPath(row: self.selIndex+1, section: 0)
        if(self.selIndex > 0){
            if(table_data[self.selIndex-1].music_id == next){
                indexPath = IndexPath(row: self.selIndex-1, section: 0)
            }
        }
        
            UIView.animate(withDuration: 0.5,delay:0.0,options: [],animations:{
                self.favoriteTable.scrollToRow(at: indexPath, at: .middle, animated: true)},
                                       completion: { finished in UIView.animate(withDuration: 0.5, animations:{
                                    self.tableView(self.favoriteTable, didSelectRowAt: indexPath)
                                       })
                                    })
            
            
            //self.favoriteTable.didSelectRowAtIndexPath(indexPath, animated: true, scrollPosition: UITableViewScrollPosition.None);
        }
            
//            dispatch_sync(dispatch_get_main_queue()){
//                var indexPath = NSIndexPath(forRow: self.selIndex, inSection: 0)
//                self.favoriteTable.scrollToRowAtIndexPath(self.indexPath, atScrollPosition: .None, animated: true)
//                
//                self.favoriteTable.layoutIfNeeded()
//                
//            }
            
//            UIView.animateWithDuration(0.25, delay: 0, options: UIViewAnimationOptions.CurveEaseInOut ,
//                                       animations: {
//                                        self.myScrollView.contentOffset = CGPointMake(newContentOffsetX,newContentOffsetY)
//                },
//                                       completion:{ Void in
//                                        //                    // 行番号を取得
//                                        self.detailId = next
//                                        self.detailName = theView.text!
//                                        let nex : DetailViewController = self.storyboard!.instantiateViewControllerWithIdentifier("DetailViewController") as! DetailViewController
//                                        nex.id = self.detailId
//                                        nex.subCategoryId = self.subCategoryId
//                                        nex.navHeight = self.navHeight
//                                        nex.modalPresentationStyle = .Custom
//                                        nex.transitioningDelegate = self
//                                        nex.delegate = self
//                                        nex.tagArr = self.tagArr
//                                        theView.textColor = UIColor.blackColor()
//                                        layear.opacity = 1
//                                        self.tapPt=0
//                                        self.presentViewController(nex, animated: true, completion: {})
//                }
//            )
//        }
//    }
}

