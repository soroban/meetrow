//
//  ObjcUtility.m
//  Meetrow
//
//  Created by 大口 尚紀 on 2016/11/19.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ObjcUtility.h"

@implementation ObjcUtility

+ (CIContext *)cicontextWithOptions:(NSDictionary<NSString *, id> *)options {
    return [CIContext contextWithOptions:options];
}

@end
