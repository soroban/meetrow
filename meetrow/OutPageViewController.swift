//
//  OutPageViewController.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2015/11/15.
//  Copyright © 2015年 大口 尚紀. All rights reserved.
//

import UIKit
import WebKit

class OutPageViewController: UIViewController, UIWebViewDelegate {
    @IBOutlet var myToolBar: UIToolbar!
    var webView: UIWebView!
    var url:URL!
    var navHeight:CGFloat!
    
    @IBOutlet var backItem: UIBarButtonItem!
    @IBOutlet var fowardItem: UIBarButtonItem!
    @IBOutlet var safariItem: UIBarButtonItem!
    @IBOutlet var closeItem: UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navHeight = self.navigationController?.navigationBar.frame.size.height
        
        // set WKWebView
        let webViewHeight = self.view.bounds.height-UIApplication.shared.statusBarFrame.height-self.navHeight-self.myToolBar.frame.size.height
        myToolBar.tintColor = UIColor(hex: "#222831")
        self.webView = UIWebView(frame: CGRect(x: 0, y: UIApplication.shared.statusBarFrame.height+self.navHeight,width: self.view.bounds.width,height: webViewHeight))

        self.webView.delegate = self
//        self.view = self.webView
        let request = URLRequest(url: self.url!)
        self.webView!.loadRequest(request)
        
        // インスタンスをビューに追加する
        self.view.addSubview(self.webView)
    }
    
    // ロード時にインジケータをまわす
    func webViewDidStartLoad(_ webView: UIWebView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    // ロード完了でインジケータ非表示
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.url = URL(string: webView.stringByEvaluatingJavaScript(from: "document.URL")!)
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        if(webView.canGoBack){
            self.backItem.isEnabled = true
        }else{
            self.backItem.isEnabled = false
        }
        if(webView.canGoForward){
            self.fowardItem.isEnabled = true
        }else{
            self.fowardItem.isEnabled = false
        }
        self.safariItem.isEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let nv = navigationController {
            // 表示
            nv.setNavigationBarHidden(false, animated: true)
            // タイトルを設定する.
        }
    }
    
    @IBAction func close(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: {});
    }

    @IBAction func goBack(_ sender: AnyObject) {
        self.webView.goBack()
    }
    
    @IBAction func goFoward(_ sender: AnyObject) {
        self.webView.goForward()
    }
    
    @IBAction func openSafari(_ sender: AnyObject) {
        UIApplication.shared.openURL(self.url)
    }
        /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
