//
//  CustomHeaderCell.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2016/02/05.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import UIKit

class CustomHeaderCell: UITableViewCell {

    @IBOutlet var headerLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
