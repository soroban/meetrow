//
//  StationDetail.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2015/11/08.
//  Copyright © 2015年 大口 尚紀. All rights reserved.
//

import Foundation
import UIKit

class Detail1: NSObject {
    var id: Int!
    var jp_name: String!
    var en_name: String!
    var video_id: Array<String>!
    var video_title: Array<String>!
    var video_thumbnail: Array<String>!
    var jp_wiki_title: String!
    var jp_wiki_descript: String!
    var jp_wiki_update_at: Date!
    var en_wiki_title: String!
    var en_wiki_descript: String!
    var en_wiki_update_at: Date!
    var day: Array<String>!
    var time: Array<String>!
    var stage: Array<String>!
    var jp: Bool!
    
    init(id:Int, jp_name: String, en_name: String, video_id: Array<String>?, video_title: Array<String>?, video_thumbnail: Array<String>?, jp_wiki_title: String?, jp_wiki_descript: String?, jp_wiki_update_at:Date?, en_wiki_title: String?, en_wiki_descript: String?, en_wiki_update_at:Date?, day: Array<String>?, time: Array<String>?, stage: Array<String>?, jp:Bool) {
        self.id = id
        self.jp_name = jp_name
        self.en_name = en_name
        self.video_id = video_id
        self.video_title = video_title
        self.video_thumbnail = video_thumbnail
        self.jp_wiki_title = jp_wiki_title
        self.jp_wiki_descript = jp_wiki_descript
        self.jp_wiki_update_at = jp_wiki_update_at
        self.en_wiki_title = en_wiki_title
        self.en_wiki_descript = en_wiki_descript
        self.en_wiki_update_at = en_wiki_update_at
        self.day = day
        self.time = time
        self.stage = stage
        self.jp = jp
    }
}
