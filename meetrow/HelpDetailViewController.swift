//
//  HelpDetailViewController.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2016/03/11.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import UIKit
import GoogleMobileAds

class HelpDetailViewController: UIViewController, GADBannerViewDelegate {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    var helpMessage:String!
    var helpTitle:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let admobView = GADBannerView(adSize:kGADAdSizeSmartBannerPortrait)
        addHeight = (self.view.frame.width * admobView.frame.height)/admobView.frame.width
        admobView.frame.size = CGSize(width: self.view.frame.width, height: addHeight)
        admobView.frame.origin = CGPoint(x: 0, y: self.view.frame.height-addHeight)
        
        admobView.adUnitID = "ca-app-pub-8256083710740545/3348041715"
        admobView.delegate = self
        admobView.rootViewController = self
        let admobRequest:GADRequest = GADRequest()
        
        if AdMobTest {
            if SimulatorTest {
                admobRequest.testDevices = [kGADSimulatorID]
            }
            else {
                admobRequest.testDevices = [TEST_DEVICE_ID]
            }
            
        }
        admobView.load(admobRequest)
        self.view.addSubview(admobView)
        
        self.titleLabel.text = self.helpTitle
        self.messageLabel.text = self.helpMessage


        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        let orientation: UIInterfaceOrientationMask = [UIInterfaceOrientationMask.portrait]
        return orientation
    }
    
    //指定方向に自動的に変更するか？
    override var shouldAutorotate : Bool{
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
