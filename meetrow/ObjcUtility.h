//
//  ObjcUtility.h
//  Meetrow
//
//  Created by 大口 尚紀 on 2016/11/19.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

#ifndef ObjcUtility_h
#define ObjcUtility_h
#import <UIKit/UIKit.h>

@interface ObjcUtility : NSObject

+ (CIContext *)cicontextWithOptions:(NSDictionary<NSString *, id> *)options;

@end

#endif /* ObjcUtility_h */
