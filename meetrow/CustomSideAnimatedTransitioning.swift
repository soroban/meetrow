//
//  CustomSideAnimatedTransitioning.swift
//  Meetrow
//
//  Created by 大口 尚紀 on 2016/04/10.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import UIKit

class CustomSideAnimatedTransitioning: NSObject, UIViewControllerAnimatedTransitioning {
    
    let isPresent: Bool
    
    init(isPresent: Bool) {
        self.isPresent = isPresent
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        if isPresent {
            animatePresentTransition(transitionContext)
        } else {
            animateDissmissalTransition(transitionContext)
        }
    }
    
    func animatePresentTransition(_ transitionContext: UIViewControllerContextTransitioning) {
        let presentingController: UIViewController! = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)
        let presentedController: UIViewController! = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
        let containerView: UIView! = transitionContext.containerView
        containerView.insertSubview(presentedController.view, belowSubview: presentingController.view)
        //適当にアニメーション
        UIView.animate(withDuration: self.transitionDuration(using: transitionContext), animations: {
            presentedController.view.frame.origin.x -= containerView.bounds.size.width
            }, completion: {
                finished in
                transitionContext.completeTransition(true)
        })
    }
    
    func animateDissmissalTransition(_ transitionContext: UIViewControllerContextTransitioning) {
        let presentedController: UIViewController! = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)
        let containerView: UIView! = transitionContext.containerView
        //適当にアニメーション
        UIView.animate(withDuration: self.transitionDuration(using: transitionContext), animations: {
            presentedController.view.frame.origin.x = containerView.bounds.size.width
            }, completion: {
                finished in
                transitionContext.completeTransition(true)
        })
    }
    
}
