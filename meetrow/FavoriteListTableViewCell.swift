//
//  FavoriteListTableViewCell.swift
//  Meetrow
//
//  Created by 大口 尚紀 on 2016/05/20.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//


class FavoriteListTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var name: UILabel!
    var detailLabel: UILabel!
    var musicId:Int!
    var index:Int!
    
    var disp = true
    var first = false
    var last = false
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.name = UILabel()
        self.name.font = UIFont.boldSystemFont(ofSize: 19)
//        self.name.layer.borderWidth = 1.0
        self.name.numberOfLines = 1
        self.name.lineBreakMode = NSLineBreakMode.byTruncatingTail
        self.addSubview(self.name)
        
        self.detailLabel = UILabel()
        self.detailLabel.numberOfLines = 4
        //self.detailLabel.sizeToFit()
        self.detailLabel.textColor = UIColor(hex: "#222831")
        self.detailLabel.textAlignment = NSTextAlignment.center
//        self.detailLabel.layer.borderWidth = 1.0
        self.detailLabel.adjustsFontSizeToFitWidth = true
        self.detailLabel.minimumScaleFactor = 0.8

        self.addSubview(self.detailLabel)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func setCell(_ musicId : Int, index : Int, disp:Bool, first:Bool, last:Bool, name:String, dayArr:[String]!, timeArr:[String]!, stageArr:[String]!) {
        self.musicId = musicId
        self.index = index
        self.disp = disp
        self.first = first
        self.last = last
        let rounded_size = CGFloat(20)
        
        self.name.text = name
        self.name.frame = CGRect(x: 0, y: 0, width: self.frame.size.width-100-20, height: self.frame.size.height/4)
        self.name.layer.position = CGPoint(x: ((self.frame.size.width-100)/2) + 100 - 10, y: ((self.frame.size.height/4)/2))
    
        // 配置する座標を設定する.
        let shapeLayer = CAShapeLayer()
        // 円のCALayer作成
        shapeLayer.strokeColor = UIColor(hex: "#222831").cgColor
        shapeLayer.fillColor = UIColor.white.cgColor
        shapeLayer.lineWidth = 2  // 輪郭の線の太さは1.0pt
        
        // 図形は円形
        let rectWidth:CGFloat = CGFloat(rounded_size/2)
        let rectHeight:CGFloat = CGFloat(rounded_size/2)
        shapeLayer.path = UIBezierPath(roundedRect: CGRect(x: 42,y: (self.frame.size.height/2)-rectHeight, width: rounded_size, height: rounded_size), cornerRadius: rectWidth).cgPath
        shapeLayer.name = String(musicId)
        // 作成したCALayerを画面に追加
        // ViewにLabelを追加.
        
        if(self.disp){
            let lineShapeLayer = CAShapeLayer()
            lineShapeLayer.strokeColor = UIColor(hex: "#222831").cgColor
            lineShapeLayer.fillColor = UIColor.clear.cgColor
            lineShapeLayer.lineWidth = 8
                        
            let cgPath:CGMutablePath = CGMutablePath()
            if(self.first){
                cgPath.move(to: CGPoint(x: 42+rectWidth, y:(self.frame.size.height/2)-rectHeight))
//                CGPathMoveToPoint(cgPath, nil, 42+rectWidth, (self.frame.size.height/2)-rectHeight)
            }else{
                //CGPathMoveToPoint(cgPath, nil, 42+rectWidth, 0)
                cgPath.move(to: CGPoint(x: 42+rectWidth,y: 0))
            }
            
            if(self.last){
                cgPath.addLine(to: CGPoint(x: 42+rectWidth, y:(self.frame.size.height/2)-rectHeight))
//                CGPathAddLineToPoint(cgPath, nil,42+rectWidth, (self.frame.size.height/2)-rectHeight)
            }else{
                cgPath.addLine(to: CGPoint(x:42+rectWidth, y:self.frame.size.height))
//                CGPathAddLineToPoint(cgPath, nil,42+rectWidth, self.frame.size.height)
            }
            
            let path = UIBezierPath(cgPath: cgPath)
            lineShapeLayer.path = path.cgPath
            lineShapeLayer.name = "99999"
            // viewのlayerに描画したものをセットする
            self.layer.addSublayer(lineShapeLayer)
        }
        self.layer.addSublayer(shapeLayer)
        
        
        var detailIndex=0
        var detailText = " "
        var lineWidth = CGFloat(0)
        let attributedText = NSMutableAttributedString()

        var detailCheck = false
        
        if(dayArr != nil || timeArr != nil || stageArr != nil){
            detailCheck = true
        }
        
        if(detailCheck == true){
            var stageCheck = false
            var areaCheck = false
            var timeCheck = false
            var newLine = false
            
            let weekdays = ["日","月","火","水","木","金","土"]
            
            var dayCount = 0;
            for artsitDay in dayArr{
                let areaArr = artsitDay.components(separatedBy: "-")
                var day = artsitDay
                var area = ""
                if(areaArr.count > 1){
                    area = areaArr[0]
                    day = areaArr[1]
                    areaCheck = true
                }
                
                var time = ""
                if(timeArr != nil) {
                    if(timeArr.count > detailIndex){
                        time = " " + timeArr[detailIndex]
                        timeCheck = true
                    }
                }
                
                var stage = ""
                if(stageArr != nil) {
                    if(stageArr.count > detailIndex){
                        stage = " " + stageArr[detailIndex]
                        stageCheck = true
                    }
                }
                
                let date_formatter: DateFormatter = DateFormatter()
                date_formatter.dateFormat = "yyyy/MM/dd"
                let date = date_formatter.date(from: day)
                let cal: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
                let comp: DateComponents = (cal as NSCalendar).components(
                    [NSCalendar.Unit.weekday],
                    from: date!
                )
                let weekday: Int = comp.weekday!
                let weekdaySymbolIndex: Int = weekday - 1
                
                let out_formatter: DateFormatter = DateFormatter()
                out_formatter.locale = Locale(identifier: "en_US")
                let lanArr = Locale.preferredLanguages[0].components(separatedBy: "-")
                if(Locale.preferredLanguages[0] == "ja-JP" || lanArr.first == "ja"){
                    out_formatter.dateFormat = "MM.dd [\(weekdays[weekdaySymbolIndex])]"
                }else{
                    out_formatter.dateFormat = "E, MMM d"
                }
                
                if(detailIndex > 0){
                    attributedText.append(NSAttributedString(string: "\n"))
                    lineWidth = 5
                }
                
                
                let attachment = NSTextAttachment()
                attachment.image = UIImage(named: "clock.png")
                
                if(stageCheck == true && areaCheck == true){
                    newLine = true
                }else if(stageCheck == true && timeCheck == true && dayArr.count > 1){
                    newLine = true
                }

                
                if(newLine){
                    attachment.bounds = CGRect(x: 0, y: -4, width: self.frame.size.height/7, height: self.frame.size.height/7)
                }else{
                    attachment.bounds = CGRect(x: 0, y: -2, width: self.frame.size.height/7, height: self.frame.size.height/7)
                }

                attributedText.append(NSAttributedString(attachment: attachment))
                
                if(area != ""){
                    detailText = " " + out_formatter.string(from: date!) + " - " + area
                    if(stage != ""){
                        detailText = detailText + time + "\n" + stage
                    }
                    
                    attributedText.append(NSAttributedString(string: detailText))
                }else{
                    if(date != nil){
                        detailText = " " + out_formatter.string(from: date!)
                        if(stage != ""){
                            detailText = detailText + time + "\n" + stage
                        }
                        attributedText.append(NSAttributedString(string: detailText))
                    }
                }
                detailIndex += 1
                dayCount += 1;
                if(dayCount > 1){
                    if(dayArr.count > 2){
                        let endPoint = attributedText.length
                        attributedText.deleteCharacters(in: NSRange(location: endPoint-2,length: 2))
                        attributedText.append(NSAttributedString(string: "..."))
                    }
                    break
                }

            }
            
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = lineWidth
            paragraphStyle.alignment = NSTextAlignment.left
            paragraphStyle.lineBreakMode = NSLineBreakMode.byTruncatingTail
            attributedText.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, attributedText.length))

            self.detailLabel.frame = CGRect(x: 0, y: 0, width: self.frame.size.width-100-50, height: (self.frame.size.height*3)/4)
            self.detailLabel.layer.position = CGPoint(x: ((self.frame.size.width-100)/2) + 100 - 25, y: (self.frame.size.height/4)+(((self.frame.size.height*3)/4)/2)-5)
//            self.detailLabel.layer. borderWidth
//            self.detailLabel.layer.borderWidth = 1.0
            self.name.adjustsFontSizeToFitWidth = true
            self.name.minimumScaleFactor = 0.6
 
            self.detailLabel.font = UIFont.systemFont(ofSize: 15)
            let stText:String = attributedText.string
            if(lineNumber(self.detailLabel, text:stText) <= 2){
                self.name.layer.position = CGPoint(x: ((self.frame.size.width-100)/2) + 100 - 10, y: ((self.frame.size.height/4)/2) + 10)
            }else{
                self.detailLabel.font = UIFont.systemFont(ofSize: 11)
            }

            
//            if(newLine == false){
//                self.detailLabel.font = UIFont.systemFontOfSize(15)
//                self.name.layer.position = CGPoint(x: ((self.frame.size.width-100)/2) + 100 - 10, y: ((self.frame.size.height/4)/2) + 10)
//            }else{
//                self.detailLabel.font = UIFont.systemFontOfSize(11)
//            }
            
//            self.detailLabel.lineBreakMode = NSLineBreakMode.ByTruncatingTail
            
            self.detailLabel.attributedText = attributedText
            

            //self.detailLabel.sizeToFit()
//            print(attributedText)
            
        }
        
        // Configure the view for the selected state

    }
    
    func editCell(_ musicId : Int, index : Int, name:String) {
        self.name.text = name
        self.musicId = musicId
        let rounded_size = CGFloat(20)
        
        self.name.text = name
        
        // 配置する座標を設定する.
        let shapeLayer = CAShapeLayer()
        // 円のCALayer作成
        shapeLayer.strokeColor = UIColor(hex: "#222831").cgColor
        shapeLayer.fillColor = UIColor.white.cgColor
        shapeLayer.lineWidth = 2  // 輪郭の線の太さは1.0pt
        
        // 図形は円形
        let rectWidth:CGFloat = CGFloat(rounded_size/2)
        let rectHeight:CGFloat = CGFloat(rounded_size/2)
        shapeLayer.path = UIBezierPath(roundedRect: CGRect(x: 42,y: (self.frame.size.height/2)-rectHeight, width: rounded_size, height: rounded_size), cornerRadius: rectWidth).cgPath
        shapeLayer.name = String(musicId)
        // 作成したCALayerを画面に追加
        // ViewにLabelを追加.
        
        for layer in self.layer.sublayers! {
            if(layer.name == "99999"){
                layer.removeFromSuperlayer()
            }
        }
        
        self.layer.addSublayer(shapeLayer)
        
        // Configure the view for the selected state
        
    }
    
    func lineNumber(_ label: UILabel, text: String) -> Int {
        let oneLineRect  =  "a".boundingRect(with: label.bounds.size, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: label.font], context: nil)
        let boundingRect = text.boundingRect(with: label.bounds.size, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: label.font], context: nil)
        
        return Int(boundingRect.height / oneLineRect.height)
    }

}
