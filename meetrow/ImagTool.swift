//
//  MyImage.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2015/10/13.
//  Copyright © 2015年 大口 尚紀. All rights reserved.
//

import Foundation

class ImageTool {
    
    static func cropThumbnailImage(_ image :UIImage, w:Int, h:Int) ->UIImage
    {
        let origRef    = image.cgImage;
        let origWidth  = Int((origRef?.width)!)
        let origHeight = Int((origRef?.height)!)
        var resizeWidth:Int = 0, resizeHeight:Int = 0
        
        //縦長
        if (origWidth < origHeight) {
            resizeWidth = w
            resizeHeight = origHeight * resizeWidth / origWidth
            if(resizeHeight < h){
                resizeHeight = h
                resizeWidth = origWidth * resizeHeight / origHeight
            }
        //横長
        } else {
            resizeHeight = h
            resizeWidth = origWidth * resizeHeight / origHeight
            if(resizeWidth < w){
                resizeWidth = w
                resizeHeight = origHeight * resizeWidth / origWidth
            }
        }
        let resizeSize = CGSize(width: CGFloat(resizeWidth), height: CGFloat(resizeHeight))
        UIGraphicsBeginImageContext(resizeSize)
        
        image.draw(in: CGRect(x: 0, y: 0, width: CGFloat(resizeWidth), height: CGFloat(resizeHeight)))
        
        let resizeImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    
        // 切り抜き処理
        let cropRect  = CGRect(
        x: CGFloat((resizeWidth - w) / 2)+2,
        y: CGFloat((resizeHeight - h) / 2)+2,
        width: CGFloat(w), height: CGFloat(h))
        let cropRef   = resizeImage?.cgImage?.cropping(to: cropRect)
        let cropImage = UIImage(cgImage: cropRef!)
    
        return cropImage
    }
}
