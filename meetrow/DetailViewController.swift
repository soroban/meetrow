//
//  DetailViewController.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2015/09/30.
//  Copyright © 2015年 大口 尚紀. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import SafariServices
import MediaPlayer

protocol DetailViewControllerDelegate{
    func loginComp()
}

//import YouTubePlayer

class DetailViewController: UIViewController, UIWebViewDelegate, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, SFSafariViewControllerDelegate, DetailViewControllerDelegate, CAAnimationDelegate {
    let ref = Firebase(url: backendUrl)
    var app:AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
    var delegate: ViewControllerDelegate!
    var favDelegate: FavoriteListViewControllerDelegate!
    var favoriteCollection = FavoriteManager.sharedInstance
    var favorite: Array<Favorite> = []
    var favRelations: Array<Relation> = []
    var id:Int!
    var subCategoryId:Int!
    var playerViewController: AVPlayerViewController!
    var myWebView: UIWebView!
    var myTableView : UITableView!
    var secTableView : UITableView!
    var wikiTableView : UITableView!
    var detailTableView : UITableView!
    var navHeight:CGFloat!
    var staHeight:CGFloat!
    var cancelBUtton:UIButton!
    var titleLabel: UILabel!
    var playWidth: CGFloat!
    var playHeight: CGFloat!
    var tapTime: Date!
    var tapTimer : Timer!
    var countTimer : Timer!
    var startLabel: UILabel!
    var checkPlayTimer: Timer!
    var endLabel: UILabel!
    var disp: Bool!
    var mySlider:UISlider!
    var musicTitelLabel:UILabel!
    var durtion:Int!
    var touchFlg:Bool!
    var loadingTimer : Timer!
    var loadingLabel : UILabel!
    var preState: Int!
    var buttonSize: CGFloat!
    var playButton: UIButton!
    var smallPlayButton: UIButton!
    var pauseButton: UIButton!
    var smallPauseButton: UIButton!
    var effectView: UIVisualEffectView!
    var resizeButton: UIButton!
    var compressButton: UIButton!
    var myLayer:CAShapeLayer!
    var centerLayer:CAShapeLayer!
    var centerCLayer:CAShapeLayer!
    
    // AVPlayer.
    var videoPlayer : AVPlayer!
    // シークバー.
    var seekBar : UISlider!
    var fullscreen = false
    var musicList:[Dictionary<String, String>] = []
    var secDataList:[Dictionary<String, String>] = []
    var secHeight:CGFloat!
    var thiHeight:CGFloat = 0
    var detailHeight:CGFloat = 0
    var screenHeight:CGFloat = 0
    var screenWidth:CGFloat = 0
    var moreLabel:UILabel!
    var videoIdArr:[String]!
    var videoTitleArr:[String]!
    var videoThumbnailArr:[String]!
    var name:String!
    var url:URL!
    var wikiUrl:URL!
    var indexNo = 1
    var forwardButton: UIButton!
    var backButton: UIButton!
    var likeButton:UIButton = UIButton(type: UIButtonType.custom)
    var plusNum = 5
    var language:String!
    var wikiTitleLabel:UILabel!
    var wikiDescLabel:UILabel!
    var quoteLabel:UILabel!
    var wikiLabel:UILabel!
    var detailLabel:UILabel!
    var scheduleLabel:UILabel!
    var fromGowiki:Bool!
    var relation: Array<Relation> = []
    var triger:Bool!
    var userState:Int!
    var commandCenter:MPRemoteCommandCenter!
    var playPreState:Int!
    var playCheckCount:Int!
    var tagArr: Dictionary<Int, Bool> = [:]
    
    var dayArr:[String]!
    var timeArr:[String]!
    var stageArr:[String]!
    var detailCheck = false
    var clockImageView: UIImageView!
    
    let API_KEY:String = "AIzaSyC9yUyKeFCmPN3qHCm6sEzzdpiIucUPSK0"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let defaults = UserDefaults.standard
        if (defaults.bool(forKey: "background") == true){
            self.addRemoteControlEvent()
            UIApplication.shared.beginReceivingRemoteControlEvents()
        }else{
            UIApplication.shared.endReceivingRemoteControlEvents()
        }
    
        NotificationCenter.default.addObserver(self, selector: #selector(DetailViewController.enterForeground(_:)), name:NSNotification.Name(rawValue: "applicationWillEnterForeground"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(DetailViewController.enterBackground(_:)), name:NSNotification.Name(rawValue: "applicationDidEnterBackground"), object: nil)
        
        DispatchQueue.main.async {
            SVProgressHUD.show(withStatus: "Loading...", maskType: SVProgressHUDMaskType.black)
        }
        self.triger = true
        self.userState = 1
        self.preState = 0
        
        let start = Date()
        let languages = Locale.preferredLanguages
        self.language = languages.first
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        self.screenHeight = self.view.frame.size.height
        self.screenWidth = self.view.frame.size.width
        self.view.backgroundColor = UIColor.clear
        var detailCollection = Detail1Manager.sharedInstance
        var relationCollection = RelationManager.sharedInstance
        
        if(id == 1){
            detailCollection = Detail1Manager.sharedInstance
            relationCollection = RelationManager.sharedInstance
        }
        
        let callbackDetail = { () -> Void in
            let detail = detailCollection.detail
            
            self.name = detail?.en_name
            let lanArr = Locale.preferredLanguages[0].components(separatedBy: "-")
            if((Locale.preferredLanguages[0] == "ja-JP" || lanArr.first == "ja") && detail?.jp == true){
                self.name = detail?.jp_name
            }else{
                self.name = detail?.en_name
            }
            
            self.videoIdArr = detail?.video_id
            self.videoTitleArr = detail?.video_title
            self.videoThumbnailArr = detail?.video_thumbnail
            self.disp = false
            self.touchFlg = false
            
            let regex = try? NSRegularExpression(pattern:"^ja",options: NSRegularExpression.Options.caseInsensitive)
            let num = regex!.rangeOfFirstMatch(in: self.language, options: [],range: NSRange(location: 0, length: self.language.utf16.count)).length
            
            if(num > 0){
                if(detail?.jp_wiki_title != nil && detail?.jp_wiki_title.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty != true){
                    var encodedString = detail?.jp_wiki_title.escapeStr()
                    encodedString = encodedString?.replacingOccurrences(of: "%20", with: "+", options: [], range: nil)
                    self.wikiUrl = URL(string:"https://ja.m.wikipedia.org/wiki/" + encodedString!)
                }
            }else{
                if(detail?.en_wiki_title != nil && detail?.en_wiki_title.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty != true){
                    var encodedString = detail?.en_wiki_title.escapeStr()
                    encodedString = encodedString?.replacingOccurrences(of: "%20", with: "+", options: [], range: nil)
                    self.wikiUrl = URL(string:"https://en.m.wikipedia.org/wiki/" + encodedString!)
                }
            }
            
            self.tapTime = Date()
            self.staHeight = UIApplication.shared.statusBarFrame.height
            
            self.playWidth = UIScreen.main.nativeBounds.size.width*2
            self.playHeight = (UIScreen.main.nativeBounds.size.width)*(9/16)
            
            
            self.cancelBUtton = UIButton()
            // サイズを設定する.
            self.cancelBUtton.frame = CGRect(x: 0,y: self.staHeight-3,width: self.navHeight,height: self.navHeight)
            
            // 背景色を設定する.
            self.cancelBUtton.backgroundColor = UIColor.white
            // タイトルを設定する(通常時).
            self.cancelBUtton.setTitle("×", for: UIControlState())
            self.cancelBUtton.titleLabel?.font = UIFont.systemFont(ofSize: 47)
            self.cancelBUtton.setTitleColor(UIColor.black, for: UIControlState())
            
            self.cancelBUtton.addTarget(self, action: #selector(DetailViewController._closeView(_:)), for: .touchUpInside)
            self.view.addSubview(self.cancelBUtton)
            
            if(self.favDelegate == nil){
//                if(self.ref.authData != nil){
                    let callbackFavorite = { () -> Void in
                        self.favorite = self.favoriteCollection.favorites
                        if(self.favorite.count > 0){
                            for favorite in self.favorite{
                                if favorite.fav.index(of: self.id) != nil {
                                    self.filledLikeButton()
                                }else{
                                    self.normalLikeButton()
                                }
                            }
                        }else{
                            self.normalLikeButton()
                        }
                    }
                    
                    let AES = CryptoJS.AES()
                    self.favoriteCollection.getFavorite(callbackFavorite, uid: guuid, SubCategoryId: self.subCategoryId)
//                }else{
//                    self.normalLikeButton()
//                }
            }
        
            self.myWebView = UIWebView()
            // view.addConstraint(heightConstraint) // also work
            // Delegateを設定する.
            self.myWebView.delegate = self
            self.myWebView.scalesPageToFit = true
            self.myWebView.scrollView.isScrollEnabled = false
            self.myWebView.isOpaque = false
            self.myWebView.isUserInteractionEnabled = true
            self.myWebView.backgroundColor = UIColor.clear
            self.myWebView.mediaPlaybackRequiresUserAction = false
            self.myWebView.allowsInlineMediaPlayback = true
            self.myWebView.mediaPlaybackAllowsAirPlay = true
            // WebViewのサイズを設定する.
            self.myWebView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.width*9/16)
            
            // fullscreen表示ではなく、inline表示にする.
            self.myWebView.allowsInlineMediaPlayback = true
            // 自動的に再生を開始.deke
            self.myWebView.mediaPlaybackRequiresUserAction = false
            self.myWebView.tag = 999999
            
            self.tapTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(DetailViewController.tapCount(_:)), userInfo: nil, repeats: true)
            self.countTimer =  Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(DetailViewController.countSlider(_:)), userInfo: nil, repeats: true)
            self.loadingTimer =  Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(DetailViewController.getLoadingTimer(_:)), userInfo: nil, repeats: true)
            
            self.moreLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 140, height: 40))
            self.moreLabel.tag = 100013
            self.moreLabel.numberOfLines = 0
            
            self.moreLabel.text = NSLocalizedString("ReadMore",comment: "")
            self.moreLabel.font = UIFont.boldSystemFont(ofSize: 18.0)
            // 色を指定しておく
            let keyColor = UIColor(hex: "#222831")
            // ボーダーの色
            self.moreLabel.layer.borderColor = keyColor.cgColor
            // ボーダーの太さ
            self.moreLabel.layer.borderWidth = 2.0
            // 丸角
            self.moreLabel.layer.cornerRadius = 2
            self.moreLabel.textAlignment =  NSTextAlignment.center
            self.moreLabel.isUserInteractionEnabled = true
            let moreTap = UITapGestureRecognizer(target:self, action: #selector(DetailViewController.moreLoad(_:)))
            moreTap.numberOfTapsRequired = 1
            moreTap.delegate = self
            self.moreLabel.addGestureRecognizer(moreTap)

            let tap = UITapGestureRecognizer(target:self, action: #selector(DetailViewController.viewTap(_:)))
            tap.numberOfTapsRequired = 1
            tap.delegate = self
            self.myWebView.addGestureRecognizer(tap)
            //self.moreLabel.addGestureRecognizer(tap)
            
            self.buttonSize = self.myWebView.frame.width/6
            //ボタン追加
            var image = UIImage(named: "play.png")! as UIImage
            self.playButton = UIButton(type: .custom)
            self.playButton.tag = 100001
            self.playButton.frame = CGRect(x: 0, y: 0, width: self.buttonSize, height: self.buttonSize)
            self.playButton.layer.position = CGPoint(x: self.myWebView.frame.width/2, y:self.myWebView.frame.height/2)
            self.playButton.setImage(image, for: UIControlState())
            self.playButton.addTarget(self, action: #selector(DetailViewController.play(_:)), for:.touchUpInside)
            self.playButton.adjustsImageWhenHighlighted = false
            self.playButton.isHidden = true
            self.playButton.layer.cornerRadius = self.buttonSize/2
            self.playButton.backgroundColor =  UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha:0.5)
            
            //ボタン追加
            image = UIImage(named: "play.png")! as UIImage
            self.smallPlayButton = UIButton(type: .custom)
            self.smallPlayButton.tag = 100014
            self.smallPlayButton.frame = CGRect(x: 0, y: 0, width: self.buttonSize/1.5, height: self.buttonSize/1.5)
            self.smallPlayButton.setImage(image, for: UIControlState())
            self.smallPlayButton.addTarget(self, action: #selector(DetailViewController.play(_:)), for:.touchUpInside)
            self.smallPlayButton.adjustsImageWhenHighlighted = false
            self.smallPlayButton.layer.cornerRadius = self.buttonSize/3
            self.smallPlayButton.backgroundColor =  UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha:0.5)
            self.smallPlayButton.isHidden = true
            
            
            image = UIImage(named: "pause.png")! as UIImage
            self.pauseButton = UIButton(type: .custom)
            self.pauseButton.tag = 100002
            self.pauseButton.frame = CGRect(x: 0, y: 0, width: self.buttonSize, height: self.buttonSize)
            self.pauseButton.layer.position = CGPoint(x: self.myWebView.frame.width/2, y:self.myWebView.frame.height/2)
            self.pauseButton.setImage(image, for: UIControlState())
            self.pauseButton.addTarget(self, action: #selector(DetailViewController.pause(_:)), for:.touchUpInside)
            self.pauseButton.adjustsImageWhenHighlighted = false
            //self.pauseButton.hidden = true
            self.pauseButton.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha:0.5)
            self.pauseButton.layer.cornerRadius = self.buttonSize/2

            //ボタン追加
            image = UIImage(named: "pause.png")! as UIImage
            self.smallPauseButton = UIButton(type: .custom)
            self.smallPauseButton.tag = 100015
            self.smallPauseButton.frame = CGRect(x: 0, y: 0, width: self.buttonSize/1.5, height: self.buttonSize/1.5)
            self.smallPauseButton.setImage(image, for: UIControlState())
            self.smallPauseButton.addTarget(self, action: #selector(DetailViewController.pause(_:)), for:.touchUpInside)
            self.smallPauseButton.adjustsImageWhenHighlighted = false
            self.smallPauseButton.layer.cornerRadius = self.buttonSize/3
            self.smallPauseButton.backgroundColor =  UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha:0.5)
            //self.smallPauseButton.hidden = true

            image = UIImage(named: "forward.png")! as UIImage
            self.forwardButton = UIButton(type: .custom)
            self.forwardButton.tag = 100016
            self.forwardButton.frame = CGRect(x: 0, y: 0, width: self.buttonSize/1.5, height: self.buttonSize)
            self.forwardButton.layer.position = CGPoint(x: self.myWebView.frame.width-(self.buttonSize/1.5/2), y:self.myWebView.frame.height/2)
            self.forwardButton.setImage(image, for: UIControlState())
            self.forwardButton.addTarget(self, action: #selector(DetailViewController.forward(_:)), for:.touchUpInside)
            self.forwardButton.adjustsImageWhenHighlighted = false
            self.forwardButton.isHidden = true
            self.forwardButton.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha:0.5)
            self.forwardButton.layer.cornerRadius = 5
            
            image = UIImage(named: "back.png")! as UIImage
            self.backButton = UIButton(type: .custom)
            self.backButton.tag = 100017
            self.backButton.frame = CGRect(x: 0, y: 0, width: self.buttonSize/1.5, height: self.buttonSize)
            self.backButton.layer.position = CGPoint(x: self.buttonSize/1.5/2, y:self.myWebView.frame.height/2)
            self.backButton.setImage(image, for: UIControlState())
            self.backButton.addTarget(self, action: #selector(DetailViewController.back(_:)), for:.touchUpInside)
            self.backButton.adjustsImageWhenHighlighted = false
            self.backButton.isHidden = true
            self.backButton.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha:0.5)
            self.backButton.layer.cornerRadius = 5
            
            let effectWidth = UIScreen.main.bounds.size.width
            let effectHeight = UIScreen.main.bounds.size.width*9/(16*4.2)
            
            let effect = UIBlurEffect(style: UIBlurEffectStyle.extraLight)
            self.effectView = UIVisualEffectView(effect: effect)
            self.effectView.tag = 100003
            self.effectView.frame = CGRect(x: 0, y: 0, width: effectWidth, height: effectHeight)
            self.effectView.layer.position = CGPoint(x: effectWidth/2, y:self.myWebView.frame.height-effectHeight/2)
            self.effectView.isHidden = true
            
            self.startLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 60, height: 20))
            self.startLabel.tag = 100006
            self.startLabel.text = "0:00"
            self.startLabel.textColor = UIColor(hex: "#222831")
            self.startLabel.textAlignment = NSTextAlignment.right
            self.startLabel.font = UIFont.systemFont(ofSize: 12.0)
            self.startLabel.layer.position = CGPoint(x: 20, y:effectHeight/2)
            
            self.effectView.addSubview(self.startLabel)
            
            image = UIImage(named: "resize.png")! as UIImage
            let rButtonSize = effectHeight*3.5/5
            self.resizeButton = UIButton()
            self.resizeButton.tag = 100004
            self.resizeButton.frame = CGRect(x: 0, y: 0, width: rButtonSize, height: rButtonSize)
            self.resizeButton.layer.position = CGPoint(x: effectWidth-(self.resizeButton.frame.size.width*2.5/4), y:effectHeight/2)
            self.resizeButton.setImage(image, for: UIControlState())
            self.resizeButton.addTarget(self, action: #selector(DetailViewController.goFullScreen(_:)), for:.touchUpInside)
            self.resizeButton.adjustsImageWhenHighlighted = false
            if(self.fullscreen == true){
                self.resizeButton.isHidden = true
            }else{
                self.resizeButton.isHidden = false
            }
            
            image = UIImage(named: "compress.png")! as UIImage
            let cButtonSize = effectHeight*3.5/5
            self.compressButton = UIButton()
            self.compressButton.tag = 100009
            self.compressButton.frame = CGRect(x: 0, y: 0, width: cButtonSize, height: cButtonSize)
            self.compressButton.layer.position = CGPoint(x: effectWidth-(self.resizeButton.frame.size.width*2.5/4), y:effectHeight/2)
            self.compressButton.setImage(image, for: UIControlState())
            self.compressButton.addTarget(self, action: #selector(DetailViewController.outFullScreen(_:)), for:.touchUpInside)
            self.compressButton.adjustsImageWhenHighlighted = false
            if(self.fullscreen == true){
                self.compressButton.isHidden = false
            }else{
                self.compressButton.isHidden = true
            }
            self.effectView.addSubview(self.resizeButton)
            self.effectView.addSubview(self.compressButton)
            
            let sliderWidth = self.myWebView.frame.width-(self.compressButton.frame.size.width+self.startLabel.frame.width+self.startLabel.frame.width+20)
            // Sliderを作成する.
            self.mySlider = UISlider(frame: CGRect(x: 70, y: (effectHeight/2)-14, width: sliderWidth, height: 30))
            self.mySlider.tag = 100005
            self.mySlider.backgroundColor = UIColor.white
            self.mySlider.layer.cornerRadius = 10.0
            self.mySlider.layer.shadowOpacity = 0.5
            self.mySlider.layer.masksToBounds = false
            // 最小値と最大値を設定する.
            self.mySlider.minimumValue = 0
            self.mySlider.maximumValue = 10000
            // Sliderの位置を設定する.
            self.mySlider.value = 0
            // Sliderの現在位置より右のTintカラーを変える.
            self.mySlider.maximumTrackTintColor = UIColor.gray
            self.mySlider.backgroundColor = UIColor.clear
            // Sliderの現在位置より左のTintカラーを変える.
            self.mySlider.minimumTrackTintColor = UIColor.white
            
            self.mySlider.addTarget(self, action: #selector(DetailViewController.stopSlider(_:)), for: UIControlEvents.valueChanged)
            self.mySlider.addTarget(self, action: #selector(DetailViewController.stopSlider(_:)), for: UIControlEvents.touchDown)
            self.mySlider.addTarget(self, action: #selector(DetailViewController.setLoading(_:)), for: UIControlEvents.touchUpInside)
            self.mySlider.addTarget(self, action: #selector(DetailViewController.endSlider(_:)), for: UIControlEvents.touchUpInside)
            self.effectView.addSubview(self.mySlider)
            
            self.endLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 60, height: 20))
            self.endLabel.tag = 100007
            self.endLabel.text = "0:00"
            self.endLabel.textColor = UIColor(hex: "#222831")
            self.endLabel.textAlignment = NSTextAlignment.right
            self.endLabel.font = UIFont.systemFont(ofSize: 12.0)
            self.endLabel.layer.position = CGPoint(x: self.mySlider.layer.position.x + (self.mySlider.frame.size.width/2) + 20, y:effectHeight/2)
            self.effectView.addSubview(self.endLabel)
            
            self.musicTitelLabel = UILabel(frame: CGRect(x: 0, y: 3, width: effectWidth, height: 22))
            self.musicTitelLabel.tag = 100021
            self.musicTitelLabel.layer.shadowOpacity = 0.5
            self.musicTitelLabel.textColor = UIColor.white
            self.musicTitelLabel.font = UIFont.systemFont(ofSize: 15)
            // 最小値と最大値を設定する.
            
            self.loadingLabel = UILabel(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.width*9/16))
            self.loadingLabel.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha:0.2)
            self.loadingLabel.tag = 100008
            self.loadingLabel.isUserInteractionEnabled = true
            
            self.titleLabel = UILabel()
            self.titleLabel.frame = CGRect(x: 20, y: -5, width: UIScreen.main.bounds.size.width*CGFloat(8)/CGFloat(10),height: self.navHeight)
            self.titleLabel.textColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha:0.95)
            self.titleLabel.font = UIFont.boldSystemFont(ofSize: 20.0)
            
            self.titleLabel.text = self.name
            self.titleLabel.tag = 100023

            do {
                self.musicList.append(["title":"","thumbnail":"","videoId":""])
                self.secDataList.append(["title":"" as String,"thumbnail":"" as String,"videoId":"" as String])
                var index=0
                if(self.videoIdArr == nil){
                    self.closeN()
                }
                else{
                for lVideoId in self.videoIdArr {
                    
                    let thumbnail:String = self.videoThumbnailArr[index].removingPercentEncoding!
                    let title:String = self.videoTitleArr[index].removingPercentEncoding!
                    
                    if(title != nil){
                        self.musicList.append(["title":title, "thumbnail":thumbnail, "videoId":lVideoId])
                        if(self.secDataList.count < 4){
                            self.secDataList.append(["title":title, "thumbnail":thumbnail, "videoId":lVideoId])
                        }
                    }
                    index += 1
                }
                
                let videoID:String = self.musicList[1]["videoId"]!
                self.musicTitelLabel.text = "              " + (self.musicList[1]["title"])!
                if(self.musicList.count < 4){
                   self.moreLabel.text = NSLocalizedString("Youtube",comment: "")
                }
                        
                // ファイルパスを生成.
                let path: NSString = Bundle.main.path(forResource: "player", ofType:"html")! as NSString
                
                var myHTMLString = try String(contentsOfFile:path as String) as NSString
                // requestを生成.
                    
                myHTMLString = myHTMLString.replacingOccurrences(of: "(varVideoId)", with: videoID) as NSString
                myHTMLString = myHTMLString.replacingOccurrences(of: "(varHeight)", with: "\(self.playHeight)") as NSString
                myHTMLString = myHTMLString.replacingOccurrences(of: "(varWidth)", with: "\(self.playWidth)") as NSString
                myHTMLString = myHTMLString.replacingOccurrences(of: "(varMaxHeight)", with: "\(self.view.bounds.width)") as NSString
                myHTMLString = myHTMLString.replacingOccurrences(of: "(varMaxWidth)", with: "\(self.view.bounds.height)") as NSString

                var listStr:String = ""
                
                for i in 2 ..< self.musicList.count {
                    let videoId = self.musicList[i]["videoId"]
                    if(i == self.musicList.count-1){
                        listStr = listStr + videoId!
                    }else{
                        listStr = listStr + videoId! + ","
                    }
                }
                
                listStr = "'" + listStr + "'"
                myHTMLString = myHTMLString.replacingOccurrences(of: "(varVideoList)", with: "\(listStr)") as NSString
                
                DispatchQueue.main.async {
                    self.myWebView.loadHTMLString(myHTMLString as String, baseURL: URL(string: "https://www.youtube.com"))
                }

                self.secHeight = (self.view.bounds.height/CGFloat(13)*CGFloat(2))
                
                //wikipediaの表示
                
                self.wikiTitleLabel = UILabel(frame: CGRect(x: 10, y: 20, width: UIScreen.main.bounds.size.width-20, height:100))
                
//                var htmlDescText = ""
                var htmlTitleText = ""
                var quoteText = ""

                let dateFormatter = DateFormatter()
                dateFormatter.locale = Locale(identifier: self.language) // ロケールの設定
                
                dateFormatter.timeStyle = .short
                dateFormatter.dateStyle = .medium

                var wikiDisp = false
                if(num > 0){
                    if(detail?.jp_wiki_title != nil && detail?.jp_wiki_title.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty != true){
                        htmlTitleText = (detail?.jp_wiki_title)!
//                        htmlDescText =  detail.jp_wiki_descript + "..."
//                        quoteText = "『フリー百科事典　ウィキペディア日本語版』 <br>"+dateFormatter.stringFromDate(detail.jp_wiki_update_at)+"<br> URL: http://ja.wikipedia.org"
                        quoteText = "『フリー百科事典　ウィキペディア日本語版』 <br>"+"URL: http://ja.wikipedia.org"
                        wikiDisp = true
                    }
                }else{
                    if(detail?.en_wiki_title != nil && detail?.en_wiki_title.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty != true){
                        htmlTitleText = (detail?.en_wiki_title)!
//                        htmlDescText = detail.en_wiki_descript + "..."
//                        quoteText = "("+dateFormatter.stringFromDate(detail.en_wiki_update_at)+"). In Wikipedia: The Free Encyclopedia. <br>URL: https://en.wikipedia.org/wiki/"+detail.en_wiki_title
                        quoteText = "In Wikipedia: The Free Encyclopedia. <br>URL: https://en.wikipedia.org/"
                        wikiDisp = true
                    }
                }
                
                if(wikiDisp == true){
                    self.wikiLabel = UILabel(frame: CGRect(x: UIScreen.main.bounds.size.width/2-70, y: 30, width: 140, height: 40))
                    self.wikiLabel.tag = 100019
                    self.wikiLabel.numberOfLines = 0
                    self.wikiLabel.text = "Wikipedia"
                    self.wikiLabel.font = UIFont.boldSystemFont(ofSize: 18.0)
                    // 色を指定しておく
                    let keyColor = UIColor(hex: "#222831")
                    // ボーダーの色
                    self.wikiLabel.layer.borderColor = keyColor.cgColor
                    // ボーダーの太さ
                    self.wikiLabel.layer.borderWidth = 2.0
                    // 丸角
                    self.wikiLabel.layer.cornerRadius = 2
                    self.wikiLabel.textAlignment =  NSTextAlignment.center
                    self.wikiLabel.isUserInteractionEnabled = true
                    let wikiTap = UITapGestureRecognizer(target:self, action: #selector(DetailViewController.goWikipedia(_:)))
                    wikiTap.numberOfTapsRequired = 1
                    wikiTap.delegate = self
                    self.wikiLabel.addGestureRecognizer(wikiTap)

                    self.quoteLabel = UILabel(frame: CGRect(x: 10, y: self.wikiLabel.frame.height+30+20, width: UIScreen.main.bounds.size.width-20, height: 100))
                    self.quoteLabel.attributedText = try? NSAttributedString(data: quoteText.data(using: String.Encoding.unicode, allowLossyConversion: true)!,options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType],documentAttributes: nil)
                    self.quoteLabel.font = UIFont.systemFont(ofSize: 17)
                    self.quoteLabel.numberOfLines = 0
                    self.quoteLabel.sizeToFit()
                
                    self.thiHeight = self.wikiLabel.frame.height+self.quoteLabel.frame.height+70
                }
                self.secTableView = UITableView(frame: CGRect(x: 0, y: 10, width: self.view.bounds.width,height: self.secHeight*CGFloat(self.musicList.count)+2))
                self.secTableView.tag = 100012
                self.secTableView.backgroundColor = UIColor.clear

                for i in 0 ..< self.musicList.count {
                    self.secTableView.register(CustomCell.self, forCellReuseIdentifier: "CustomCell" + String(i))
                }

                // Cellに使われるクラスを登録.CGFloat(13)
                self.secTableView.backgroundColor = UIColor.clear
                self.secTableView.delegate = self
                self.secTableView.dataSource = self
                self.secTableView.setContentOffset(CGPoint(x: 0, y: self.secTableView.contentSize.height - self.secTableView.frame.size.height+20), animated: false)
                self.secTableView.allowsSelection = true
                self.secTableView.isScrollEnabled = false
                let v:UIView = UIView(frame: CGRect.zero)
                v.backgroundColor = UIColor.clear
                self.secTableView.tableFooterView = v
                
                
                //wikiepediaの表示
                self.wikiTableView = UITableView(frame: CGRect(x: 0, y: 10, width: self.view.bounds.width,height: self.thiHeight+2))
                self.wikiTableView.tag = 100018
                self.wikiTableView.backgroundColor = UIColor.clear
                self.wikiTableView.register(CustomCell.self, forCellReuseIdentifier: "thiCustomCell")
                
                // Cellに使われるクラスを登録.CGFloat(13)
                self.wikiTableView.backgroundColor = UIColor.clear
                self.wikiTableView.delegate = self
                self.wikiTableView.dataSource = self
                self.wikiTableView.setContentOffset(CGPoint(x: 0, y: self.wikiTableView.contentSize.height - self.wikiTableView.frame.size.height+20), animated: false)
                self.wikiTableView.allowsSelection = false
                self.wikiTableView.isScrollEnabled = false
                
                self.dayArr = detail?.day
                self.timeArr = detail?.time
                self.stageArr = detail?.stage
                
                if(self.dayArr != nil || self.timeArr != nil || self.stageArr != nil){
                    self.detailCheck = true
                }
                
                if(self.detailCheck == true){
                    let weekdays = ["日","月","火","水","木","金","土"]
                    var detailLabHeight = CGFloat(20) + CGFloat(25 * self.dayArr.count)

                    var detailText = ""
                    var lineWidth = CGFloat(0)
                    var detailIndex=0
                    
                    let attributedText = NSMutableAttributedString()
                    
                    let attachment = NSTextAttachment()
                    attachment.image = UIImage(named: "clock.png")
                    attachment.bounds = CGRect(x: 0, y: -3, width: 17, height: 17)
                    var stageCheck = false
                    var areaCheck = false
                    var newLine = false
                    for artsitDay in self.dayArr{
                        let areaArr = artsitDay.components(separatedBy: "-")
                        var day = artsitDay
                        var area = ""
                        if(areaArr.count > 1){
                            area = areaArr[0]
                            day = areaArr[1]
                            areaCheck = true
                        }
                        
                        var time = ""
                        
                        if(self.timeArr != nil) {
                            if(self.timeArr.count > detailIndex){
                                time = " " + self.timeArr[detailIndex]
                            }
                        }

                        var stage = ""
                        if(self.stageArr != nil) {
                            if(self.stageArr.count > detailIndex){
                                stageCheck = true
                                stage = " " + self.stageArr[detailIndex]
                            }
                        }

                        
                        let date_formatter: DateFormatter = DateFormatter()
                        date_formatter.dateFormat = "yyyy/MM/dd"
                        let date = date_formatter.date(from: day)
                        let cal: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
                        let comp: DateComponents = (cal as NSCalendar).components(
                            [NSCalendar.Unit.weekday],
                            from: date!
                        )
                        let weekday: Int = comp.weekday!
                        let weekdaySymbolIndex: Int = weekday - 1
                        
                        let out_formatter: DateFormatter = DateFormatter()
                        out_formatter.locale = Locale(identifier: "en_US")
                        let lanArr = Locale.preferredLanguages[0].components(separatedBy: "-")
                        if(Locale.preferredLanguages[0] == "ja-JP" || lanArr.first == "ja"){
                            out_formatter.dateFormat = "MM.dd [\(weekdays[weekdaySymbolIndex])]"
                        }else{
                            out_formatter.dateFormat = "E, MMM dd"
                        }
                        
                        if(detailIndex > 0){
                           attributedText.append(NSAttributedString(string: "\n"))
                           lineWidth = 5
                        }
                        attributedText.append(NSAttributedString(attachment: attachment))
                    
                        if(area != ""){
                            detailText = " " + out_formatter.string(from: date!) + " - " + area
                            if(stage != ""){
                                detailText = detailText  + time + stage
                            }
                            attributedText.append(NSAttributedString(string: detailText))
                        }else{
                            if(date != nil){
                                detailText = " " + out_formatter.string(from: date!)
                                if(stage != ""){
                                    detailText = detailText + time + stage
                                }
                                attributedText.append(NSAttributedString(string: detailText))
                            }
                        }
                        detailIndex += 1
                    }
                    
                    self.scheduleLabel = UILabel(frame: CGRect(x: 5, y: 10, width: self.myWebView.frame.width-20, height: 30))
                    
                    if(stageCheck == true && areaCheck){
                        newLine = true
                    }
                    if(newLine == true && self.view.frame.width < 375 ){
                        self.scheduleLabel.layer.position = CGPoint(x: self.scheduleLabel.center.x, y:self.scheduleLabel.center.y-5)
                        detailLabHeight = detailLabHeight * 1.5
                    }
                    
                    self.detailHeight = detailLabHeight + 42
                    
                    let paragraphStyle = NSMutableParagraphStyle()
                    paragraphStyle.lineSpacing = lineWidth
                    attributedText.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSMakeRange(0, attributedText.length))
                    //self.detailLabel = UILabel(frame: CGRect(x: 5, y: 30, width: self.myWebView.frame.width-20, height: detailLabHeight+2))
                    self.detailLabel = UILabel(frame: CGRect(x: 5, y: 35, width: self.myWebView.frame.width-20, height: detailLabHeight-10))
//                    self.detailLabel.sizeToFit()
                    self.detailLabel.numberOfLines = 0
                    self.detailLabel.attributedText = attributedText
                    self.detailLabel.textColor = UIColor(hex: "#222831")
                    self.detailLabel.font = UIFont.boldSystemFont(ofSize: 15)
//                    self.detailLabel.layer.borderWidth = 1.0
                    self.detailLabel.textAlignment = NSTextAlignment.left
                    
                    self.scheduleLabel.numberOfLines = 0
                    self.scheduleLabel.text = "Schedule"
                    self.scheduleLabel.font = UIFont.boldSystemFont(ofSize: 18)
                    self.scheduleLabel.textColor = UIColor(hex: "#222831")
//                    self.scheduleLabel.layer.borderWidth = 1.0
                    
                    //詳細情報
                    self.detailTableView = UITableView(frame: CGRect(x: 5, y: 10, width: self.view.bounds.width,height: self.detailHeight+2))
                    self.detailTableView.tag = 100022
                    self.detailTableView.register(CustomCell.self, forCellReuseIdentifier: "detailCustomCell")
                    // Cellに使われるクラスを登録.CGFloat(13)
                    self.detailTableView.backgroundColor = UIColor.clear
                    self.detailTableView.delegate = self
                    self.detailTableView.dataSource = self
                    self.detailTableView.setContentOffset(CGPoint(x: 0, y: self.detailTableView.contentSize.height - self.detailTableView.frame.size.height+20), animated: false)
                    self.detailTableView.allowsSelection = false
                    self.detailTableView.isScrollEnabled = false
                }

                self.myTableView = UITableView(frame: CGRect(x: 0, y: UIApplication.shared.statusBarFrame.height+self.navHeight,width: self.view.bounds.width,height: self.view.bounds.height-self.navHeight))
                self.myTableView.tag = 100011
                self.myTableView.backgroundColor = UIColor.clear
                self.myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")                
                self.myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell0")
                self.myTableView.register(TouchCustomCell.self, forCellReuseIdentifier: "MyCell1")
                self.myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell2")
                self.myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell3")
                
                if(self.detailCheck == true){
                    self.myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell4")
                    self.myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell5")
                }else {
                    self.myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell4")
                }
                
                // Cellに使われるクラスを登録.
                self.myTableView.backgroundColor = UIColor.clear
                self.myTableView.delegate = self
                self.myTableView.dataSource = self
                self.myTableView.separatorColor = UIColor.clear
                self.myTableView.allowsSelection = false
                self.myTableView.setContentOffset(CGPoint(x: 0, y: self.myTableView.contentSize.height - self.myTableView.frame.size.height+20+self.navHeight), animated: false)
                self.view.addSubview(self.myTableView)
                
                SVProgressHUD.dismiss()
                NotificationCenter.default.addObserver(self, selector: #selector(DetailViewController.onOrientationChange(_:)), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
                }
            } catch let error as NSError {
                print(error)
            }

        }
        
        if(self.favDelegate == nil){
            let callbackRelation = { () -> Void in
            let locRelation = relationCollection.relations
            var falseRelation:Array<Relation> = []
            for relation in locRelation{
                let index = 1000000+relation.to_id
                if(self.tagArr[index] == false){
                    falseRelation.append(relation);
                }
                else{
                    self.relation.append(relation);
                }
            }
            
            for temp in falseRelation{
                let callbackRelationColor =  { () -> Void in
                    let colorRelation = relationCollection.relations
                    var post=false
                    var preArr:Array<Relation> = []
                    var postArr:Array<Relation> = []
                    var index = 0
                    for clRelation in colorRelation{
                        if(clRelation.from_id == self.id){
                            post = true
                        }else if(post == false){
                            //pre
                            if(index%2 == 1){
                                preArr.insert(clRelation, at: 0)
                            }
                        }else{
                            //post
                            if(index%2 == 0){
                                postArr.append(clRelation)
                            }
                        }
                        index += 1
                    }
                    
                    if(preArr.count > 0){
                        if(preArr[0].from_id == temp.to_id){
                            for pre in preArr {
                                let preIndex = 1000000+pre.to_id
                                if(self.tagArr[preIndex] == true){
                                    self.relation.append(pre);
                                    break
                                }
                            }
                        }
                    }
                    
                    if(postArr.count > 0){
                        if(postArr[0].from_id == temp.to_id){
                            for post in postArr {
                                let postIndex = 1000000+post.to_id
                                if(self.tagArr[postIndex] == true){
                                    self.relation.append(post)
                                    break
                                }
                            }
                        }
                    }
                }
                relationCollection.getRelationColor(callbackRelationColor, subCategoryId: self.subCategoryId, color: temp.color)
            }
            
            detailCollection.getDetailFromId(callback: callbackDetail, subCategoryId: self.subCategoryId, id: self.id)
            }
            relationCollection.getRelationFromId(callbackRelation, subCategoryId: self.subCategoryId, id: self.id)
        }else{
            for tempRel in favRelations{
                self.relation.append(tempRel)
            }
            
            detailCollection.getDetailFromId(callback: callbackDetail, subCategoryId: self.subCategoryId, id: self.id)
        }
    }
    
    func loginComp(){
        if(self.ref?.authData != nil){
            let callbackFavorite = { () -> Void in
                self.favorite = self.favoriteCollection.favorites
                if(self.favorite.count > 0){
                    for favorite in self.favorite{
                        if favorite.fav.index(of: self.id) != nil {
                            self.filledLikeButton()
                        }else{
                            self.normalLikeButton()
                        }
                    }
                }else{
                    self.normalLikeButton()
                }
            }
            self.favoriteCollection.getFavorite(callbackFavorite, uid: (self.ref?.authData.uid)!, SubCategoryId: self.subCategoryId)
        }
    }
    
    func filledLikeButton(){
        likeButton.setImage(UIImage(named: "like_filled.png"), for: UIControlState())
        likeButton.frame = CGRect(x: UIScreen.main.applicationFrame.size.width-self.navHeight-10, y: self.staHeight, width: self.navHeight, height: self.navHeight)
        likeButton.backgroundColor = UIColor.white
        likeButton.addTarget(self, action: #selector(DetailViewController.tapSubLikeButton), for: UIControlEvents.touchUpInside)
        self.view.addSubview(likeButton)
    }
    
    func normalLikeButton(){
        likeButton.setImage(UIImage(named: "like.png"), for: UIControlState())
        likeButton.frame = CGRect(x: UIScreen.main.applicationFrame.size.width-self.navHeight-10,y: self.staHeight, width: self.navHeight, height: self.navHeight)
        likeButton.backgroundColor = UIColor.white
        likeButton.addTarget(self, action: #selector(DetailViewController.tapAddLikeButton), for: UIControlEvents.touchUpInside)
        self.view.addSubview(likeButton)
    }
    
    func tapAddLikeButton() {
//        if(self.ref.authData != nil){
        
            let callbackButton = { () -> Void in
            }
            self.filledLikeButton()
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
                self.favoriteCollection.addFavorite(callbackButton, uid: guuid, SubCategoryId: self.subCategoryId, musicId: self.id)
            }
//        }else{
//            performSegueWithIdentifier("LoginFromFaboriteSegue", sender: nil)
//        }
    }
    
    func tapSubLikeButton() {
        let callbackButton = { () -> Void in
        }
    
//        if(self.ref.authData != nil){
        
            self.normalLikeButton()
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
                self.favoriteCollection.subFavorite(callbackButton, uid: guuid, SubCategoryId: self.subCategoryId, musicId: self.id)
            }
//        }else{
//            performSegueWithIdentifier("LoginFromFaboriteSegue", sender: nil)
//        }
    }

    
    let session: URLSession = URLSession.shared
    
    // GET METHOD
//    func get(_ url: URL, completionHandler: (Data?, URLResponse?, NSError?) -> Void) {
//        let request: NSMutableURLRequest = NSMutableURLRequest(url: url)
//        
//        request.httpMethod = "GET"
//        request.addValue("application/json", forHTTPHeaderField: "Accept")
//        session.dataTask(with: request, completionHandler: { (responseData, response, error) -> Void in})
//    }
    
    @IBAction func _closeView(_ sender: AnyObject) {
        self.tapTimer.invalidate()
        self.countTimer.invalidate()
        self.loadingTimer.invalidate()
        
        let defaults = UserDefaults.standard
        if (defaults.bool(forKey: "background") == true){
            self.removeRemoteControlEvent()
        }
        UIApplication.shared.endReceivingRemoteControlEvents()
        
        self.dismiss(animated: true , completion: {
            self.myWebView.removeFromSuperview()
            self.myWebView = nil
            
            self.myTableView.removeFromSuperview()
            self.myTableView = nil
            
            self.secTableView.removeFromSuperview()
            self.secTableView = nil
            
            self.wikiTableView.removeFromSuperview()
            self.wikiTableView = nil
            
            if(self.detailCheck == true){
                self.detailTableView.removeFromSuperview()
                self.detailTableView = nil
            }
        })
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.removeObserver(self, name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        notificationCenter.removeObserver(self, name:NSNotification.Name(rawValue: "applicationWillEnterForeground"), object: nil)
        notificationCenter.removeObserver(self, name:NSNotification.Name(rawValue: "applicationDidEnterBackground"), object: nil)
    }
    
    func closeN(){
        self.tapTimer.invalidate()
        self.countTimer.invalidate()
        self.loadingTimer.invalidate()
        
        let defaults = UserDefaults.standard
        if (defaults.bool(forKey: "background") == true){
            self.removeRemoteControlEvent()
        }
        UIApplication.shared.endReceivingRemoteControlEvents()
        
        self.dismiss(animated: true , completion: {
            self.myWebView.removeFromSuperview()
            self.myWebView = nil
            
        })
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.removeObserver(self, name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        notificationCenter.removeObserver(self, name:NSNotification.Name(rawValue: "applicationWillEnterForeground"), object: nil)
        notificationCenter.removeObserver(self, name:NSNotification.Name(rawValue: "applicationDidEnterBackground"), object: nil)
        SVProgressHUD.dismiss()
    }

    func closeView(_ next_id:Int){
        self.tapTimer.invalidate()
        self.countTimer.invalidate()
        self.loadingTimer.invalidate()
        
        let defaults = UserDefaults.standard
        if (defaults.bool(forKey: "background") == true){
            self.removeRemoteControlEvent()
        }
        UIApplication.shared.endReceivingRemoteControlEvents()
        
        self.dismiss(animated: true , completion: {
            self.myWebView.removeFromSuperview()
            self.myWebView = nil
            
            self.myTableView.removeFromSuperview()
            self.myTableView = nil
            
            self.secTableView.removeFromSuperview()
            self.secTableView = nil
            
            self.wikiTableView.removeFromSuperview()
            self.wikiTableView = nil
            
            if(self.detailCheck){
                self.detailTableView.removeFromSuperview()
                self.detailTableView = nil
            }
            
            let notificationCenter = NotificationCenter.default
            notificationCenter.removeObserver(self, name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
            notificationCenter.removeObserver(self, name:NSNotification.Name(rawValue: "applicationWillEnterForeground"), object: nil)
            notificationCenter.removeObserver(self, name:NSNotification.Name(rawValue: "applicationDidEnterBackground"), object: nil)
            if(self.favDelegate == nil){
                self.delegate.didFinished(next_id)
            }else{
                self.favDelegate.didFinished(next_id)
            }
        })
    }

    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if(tableView.tag == 100012){
            self.myWebView.stringByEvaluatingJavaScript(from: "playVideoAt(\(indexPath.row-1));")
            self.indexNo = indexPath.row
            self.userState = 1
            self.updateSmallButton()
        }
        
        return nil;
    }
    
    /*
    Cellが選択された際に呼び出されるデリゲートメソッド.
    */
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    /*
    Cellの総数を返すデータソースメソッド.
    (実装必須)
    */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.fullscreen == true){
            return 1
        }else{
            if(tableView.tag == 100011){
                if(self.detailCheck){
                    return 6
                }else{
                    return 5
                }
            }else if(tableView.tag == 100012){
                return self.secDataList.count
            }else if(tableView.tag == 100018){
                return 3
            }else if(tableView.tag == 100022){
                return 2
            }
        }
        return 0
    }
    
    /*
    Cellに値を設定するデータソースメソッド.
    (実装必須)
    */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(self.fullscreen == true){
            let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell0", for: indexPath)
            cell.contentView.addSubview(self.myWebView)
            cell.contentView.addSubview(self.playButton)
            cell.contentView.addSubview(self.pauseButton)
            cell.contentView.addSubview(self.effectView)
            cell.contentView.addSubview(self.backButton)
            cell.contentView.addSubview(self.forwardButton)
            cell.contentView.addSubview(self.loadingLabel)
            cell.contentView.addSubview(self.musicTitelLabel)
            cell.contentView.addSubview(self.titleLabel)
            cell.contentView.bringSubview(toFront: self.loadingLabel)
            
            cell.tag = 100010
            cell.sizeToFit()
            
            return cell
        }
        else{
            if(tableView.tag == 100011){
                /*if(indexPath.row == 0){
                    let cell = tableView.dequeueReusableCellWithIdentifier("MyCell1", forIndexPath: indexPath)
                    
                    for view in cell.subviews{
                        if let button = view as? UIButton {
                            button.removeFromSuperview()
                        }
                    }

                    // 再利用するCellを取得する.
                    cell.backgroundColor = UIColor.clearColor()

                    cancelBUtton = UIButton()
                    // サイズを設定する.
                    cancelBUtton.frame = CGRectMake(0,0,navHeight,navHeight)
            
                    // 背景色を設定する.
                    cancelBUtton.backgroundColor = UIColor.clearColor()
                    // タイトルを設定する(通常時).
                    cancelBUtton.setTitle("×", forState: UIControlState.Normal)
                    cancelBUtton.titleLabel?.font = UIFont.systemFontOfSize(47)
                    cancelBUtton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
            
                    cancelBUtton.addTarget(self, action: "_closeView:", forControlEvents: .TouchUpInside)
                    cell.backgroundColor = UIColor.whiteColor()
                    // ボタンをViewに追加する.
                    cell.contentView.addSubview(cancelBUtton)
                    return cell
                }*/
                if(indexPath.row == 0){
                    let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell0", for: indexPath)
                    // 再利用するCellを取得する.
                    cell.backgroundColor = UIColor.clear

                    cell.contentView.addSubview(self.myWebView)
                    cell.contentView.addSubview(self.playButton)
                    cell.contentView.addSubview(self.pauseButton)
                    cell.contentView.addSubview(self.effectView);
                    cell.contentView.addSubview(self.forwardButton)
                    cell.contentView.addSubview(self.backButton);
                    cell.contentView.addSubview(self.loadingLabel)
                    cell.contentView.addSubview(self.titleLabel)
                    cell.contentView.addSubview(self.musicTitelLabel)

                    cell.contentView.bringSubview(toFront: self.loadingLabel)
            
                    cell.tag = 100010
                    cell.sizeToFit()
                    return cell
                }
                else if(indexPath.row == 1){
                    let touchCustomCell:TouchCustomCell = tableView.dequeueReusableCell(withIdentifier: "MyCell1", for: indexPath) as! TouchCustomCell
                    
                    if((touchCustomCell.contentView.layer.sublayers?.count == nil)){
                        // 再利用するCellを取得する.
                        touchCustomCell.backgroundColor = UIColor.clear

                        let pi = CGFloat(M_PI)
                        let start:CGFloat = 0.0 // 開始の角度
                        let end :CGFloat = pi * 2.0 // 終了の角度
                        let startPath: UIBezierPath = UIBezierPath();
                        startPath.move(to: CGPoint(x: touchCustomCell.contentView.frame.width/2, y: touchCustomCell.contentView.frame.height/2+5))
                        startPath.addArc(withCenter: CGPoint(x: touchCustomCell.contentView.frame.width/2, y: touchCustomCell.contentView.frame.height/2+5), radius: 0, startAngle: start, endAngle: end, clockwise: true) // 円弧
                
                        let endPath: UIBezierPath = UIBezierPath();
                        endPath.move(to: CGPoint(x: touchCustomCell.contentView.frame.width/2, y: touchCustomCell.contentView.frame.height/2+5))
                        endPath.addArc(withCenter: CGPoint(x: touchCustomCell.contentView.frame.width/2, y: touchCustomCell.contentView.frame.height/2+5), radius: (touchCustomCell.contentView.frame.height/2), startAngle: start, endAngle: end, clockwise: true) // 円弧

                        let ccpath: UIBezierPath = UIBezierPath();
                        ccpath.move(to: CGPoint(x: touchCustomCell.contentView.frame.width/2, y: touchCustomCell.contentView.frame.height/2+5))
                        ccpath.addArc(withCenter: CGPoint(x: touchCustomCell.contentView.frame.width/2, y: touchCustomCell.contentView.frame.height/2+5), radius: (touchCustomCell.contentView.frame.height/2)*(3/10), startAngle: start, endAngle: end, clockwise: true) // 円弧
                
                        let cpath: UIBezierPath = UIBezierPath();
                        cpath.move(to: CGPoint(x: touchCustomCell.contentView.frame.width/2, y: touchCustomCell.contentView.frame.height/2+5))
                        cpath.addArc(withCenter: CGPoint(x: touchCustomCell.contentView.frame.width/2, y: touchCustomCell.contentView.frame.height/2+5), radius: (touchCustomCell.contentView.frame.height/2)*(2/10), startAngle: start, endAngle: end, clockwise: true) // 円弧
                
                        if((self.myLayer) != nil){
                            self.myLayer.removeFromSuperlayer()
                        }
                        if((self.centerLayer) != nil){
                            self.centerLayer.removeFromSuperlayer()
                        }
                        if((self.centerCLayer) != nil){
                            self.centerCLayer.removeFromSuperlayer()
                        }
                
                        self.myLayer = CAShapeLayer()
                        self.myLayer.fillColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.6).cgColor
                        touchCustomCell.contentView.layer.addSublayer(self.myLayer)
                
                        self.centerCLayer = CAShapeLayer()
                        self.centerCLayer.fillColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.8).cgColor
                        self.centerCLayer.path = ccpath.cgPath
                        touchCustomCell.contentView.layer.addSublayer(self.centerCLayer)
                
                        self.centerLayer = CAShapeLayer()
                        self.centerLayer.fillColor = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.6).cgColor
                        self.centerLayer.path = cpath.cgPath
                        touchCustomCell.contentView.layer.addSublayer(self.centerLayer)
                        
                        var names:[[String]]=[[],[],[],[],[],[],[],[]]
                        var ids:[[Int]]=[[],[],[],[],[],[],[],[]]
                        var colors:[[String]]=[[],[],[],[],[],[],[],[]]
                        
                        for relation in self.relation{
                            var index:Int = 0;
                            if(relation.direct == 90){
                                index = 0
                            }
                            if(relation.direct == 135){
                                index = 1
                            }
                            if(relation.direct == 180){
                                index = 2
                            }
                            if(relation.direct == -135){
                                index = 3
                            }
                            if(relation.direct == -90){
                                index = 4
                            }
                            if(relation.direct == -45){
                                index = 5
                            }
                            if(relation.direct == 0){
                                index = 6
                            }
                            if(relation.direct == 45){
                                index = 7
                            }
                            
                            var check = true
                            let lanArr = Locale.preferredLanguages[0].components(separatedBy: "-")
                            for i in 0...7 {
                                if((Locale.preferredLanguages[0] == "ja-JP" || lanArr.first == "ja") && relation.jp == true){
                                    if(names[i].index(of: relation.to_jp_name) != nil){
                                        check = false
                                    }
                                }else{
                                    if(names[i].index(of: relation.to_en_name) != nil){
                                        check = false
                                    }
                                }
                            }
                        
                            if(check == true){
                                if((Locale.preferredLanguages[0] == "ja-JP" || lanArr.first == "ja") && relation.jp == true){
                                    names[index].append(relation.to_jp_name)
                                }else{
                                    names[index].append(relation.to_en_name)
                                }

                                colors[index].append(relation.color)
                                ids[index].append(relation.to_id)
                            }
                        }
                        
                        for i in 0...7 {
                            let len = (touchCustomCell.contentView.frame.height/2)*(7/10)
                            let diam = len/CGFloat(names[i].count)
                            
                            var centerX = touchCustomCell.contentView.frame.width/2
                            var centerY = touchCustomCell.contentView.frame.height/2
                            
                            var initX = CGFloat(touchCustomCell.contentView.frame.width/2)
                            var initY = CGFloat(touchCustomCell.contentView.frame.height/2)+5
                            
                            if(i == 0){
                                initX = CGFloat(touchCustomCell.contentView.frame.width/2) + CGFloat(touchCustomCell.contentView.frame.height/2)
                                initY = CGFloat(touchCustomCell.contentView.frame.height/2) + 5
                            }
                            if(i == 1){
                                initX = CGFloat(touchCustomCell.contentView.frame.width/2) + (CGFloat(touchCustomCell.contentView.frame.height/2)/CGFloat(M_SQRT2))
                                initY = CGFloat(touchCustomCell.contentView.frame.height/2) + 5 - (CGFloat(touchCustomCell.contentView.frame.height/2)/CGFloat(M_SQRT2))
                            }
                            if(i == 2){
                                initX = CGFloat(touchCustomCell.contentView.frame.width/2)
                                initY = CGFloat(touchCustomCell.contentView.frame.height/2) + 5 - CGFloat(touchCustomCell.contentView.frame.height/2)
                            }
                            if(i == 3){
                                initX = CGFloat(touchCustomCell.contentView.frame.width/2) - (CGFloat(touchCustomCell.contentView.frame.height/2)/CGFloat(M_SQRT2))
                                initY = CGFloat(touchCustomCell.contentView.frame.height/2) + 5 - (CGFloat(touchCustomCell.contentView.frame.height/2)/CGFloat(M_SQRT2))
                            }
                            if(i == 4){
                                initX = CGFloat(touchCustomCell.contentView.frame.width/2) - CGFloat(touchCustomCell.contentView.frame.height/2)
                                initY = CGFloat(touchCustomCell.contentView.frame.height/2) + 5
                            }
                            if(i == 5){
                                initX = CGFloat(touchCustomCell.contentView.frame.width/2) - (CGFloat(touchCustomCell.contentView.frame.height/2)/CGFloat(M_SQRT2))
                                initY = CGFloat(touchCustomCell.contentView.frame.height/2) + 5 + (CGFloat(touchCustomCell.contentView.frame.height/2)/CGFloat(M_SQRT2))
                            }
                            if(i == 6){
                                initX = CGFloat(touchCustomCell.contentView.frame.width/2)
                                initY = CGFloat(touchCustomCell.contentView.frame.height/2) + 5 + CGFloat(touchCustomCell.contentView.frame.height/2)
                            }
                            if(i == 7){
                                initX = CGFloat(touchCustomCell.contentView.frame.width/2) + (CGFloat(touchCustomCell.contentView.frame.height/2)/CGFloat(M_SQRT2))
                                initY = CGFloat(touchCustomCell.contentView.frame.height/2) + 5 + (CGFloat(touchCustomCell.contentView.frame.height/2)/CGFloat(M_SQRT2))
                            }
                            
                            if(names[i].count > 0){
                            for key in 0...names[i].count-1 {
                                var radius:CGFloat = (touchCustomCell.contentView.frame.height/2)*(2.65/10);
                                var wradius:CGFloat = radius * 0.9
                                var labelS:CGFloat = wradius*2*0.9

                                let stapath: UIBezierPath = UIBezierPath();
                                let stacpath: UIBezierPath = UIBezierPath();
                                var labelF:CGFloat = 10
                                
                                if(i == 0){
                                    if(names[i].count == 1){
                                        centerX = initX - radius - 5
                                        centerY = initY
                                    }
                                    else{
                                        radius = CGFloat(diam/2)
                                        wradius = 0.9 * radius
                                        labelS = wradius*2*0.9
                                        centerX = initX - (diam*CGFloat(key)) - radius
                                        centerY = initY
                                        labelF = 7
                                    }
                                }
                                if(i == 1){
                                    if(names[i].count == 1){
                                        centerX = initX - (radius+5)/CGFloat(M_SQRT2)
                                        centerY = initY + (radius+5)/CGFloat(M_SQRT2)
                                    }
                                    else{
                                        radius = CGFloat(diam/2)
                                        wradius = 0.9 * radius
                                        labelS = wradius*2*0.9
                                        centerX = initX - (diam*CGFloat(key))/CGFloat(M_SQRT2) - radius/CGFloat(M_SQRT2)
                                        centerY = initY + (diam*CGFloat(key))/CGFloat(M_SQRT2) + radius/CGFloat(M_SQRT2)
                                        labelF = 7
                                    }
                                }
                                if(i == 2){
                                    if(names[i].count == 1){
                                        centerX = initX
                                        centerY = initY + radius + 5
                                    }
                                    else{
                                        radius = CGFloat(diam/2)
                                        wradius = 0.9 * radius
                                        labelS = wradius*2*0.9
                                        centerX = initX
                                        centerY = initY + (diam*CGFloat(key)) + radius
                                        labelF = 7
                                    }
                                }
                                if(i == 3){
                                    if(names[i].count == 1){
                                        centerX = initX + (radius+5)/CGFloat(M_SQRT2)
                                        centerY = initY + (radius+5)/CGFloat(M_SQRT2)
                                    }
                                    else{
                                        radius = CGFloat(diam/2)
                                        wradius = 0.9 * radius
                                        labelS = wradius*2*0.9
                                        centerX = initX + (diam*CGFloat(key))/CGFloat(M_SQRT2) + radius/CGFloat(M_SQRT2)
                                        centerY = initY + (diam*CGFloat(key))/CGFloat(M_SQRT2) + radius/CGFloat(M_SQRT2)
                                        labelF = 7
                                    }
                                }
                                if(i == 4){
                                    if(names[i].count == 1){
                                        centerX = initX + radius + 5
                                        centerY = initY
                                    }
                                    else{
                                        radius = CGFloat(diam/2)
                                        wradius = 0.9 * radius
                                        labelS = wradius*2*0.9
                                        centerX = initX + (diam*CGFloat(key)) + radius
                                        centerY = initY
                                        labelF = 7
                                    }
                                }
                                if(i == 5){
                                    if(names[i].count == 1){
                                        centerX = initX + (radius+5)/CGFloat(M_SQRT2)
                                        centerY = initY - (radius+5)/CGFloat(M_SQRT2)
                                    }
                                    else{
                                        radius = CGFloat(diam/2)
                                        wradius = 0.9 * radius
                                        labelS = wradius*2*0.9
                                        centerX = initX + (diam*CGFloat(key))/CGFloat(M_SQRT2) + radius/CGFloat(M_SQRT2)
                                        centerY = initY - (diam*CGFloat(key))/CGFloat(M_SQRT2) - radius/CGFloat(M_SQRT2)
                                        labelF = 7
                                    }
                                }
                                if(i == 6){
                                    if(names[i].count == 1){
                                        centerX = initX
                                        centerY = initY - radius - 5
                                    }
                                    else{
                                        radius = CGFloat(diam/2)
                                        wradius = 0.9 * radius
                                        labelS = wradius*2*0.9
                                        centerX = initX
                                        centerY = initY - (diam*CGFloat(key)) - radius
                                        labelF = 7
                                    }
                                }
                                if(i == 7){
                                    if(names[i].count == 1){
                                        centerX = initX - (radius+5)/CGFloat(M_SQRT2)
                                        centerY = initY - (radius+5)/CGFloat(M_SQRT2)
                                    }
                                    else{
                                        radius = CGFloat(diam/2)
                                        wradius = 0.9 * radius
                                        labelS = wradius*2*0.9
                                        centerX = initX - (diam*CGFloat(key))/CGFloat(M_SQRT2) - radius/CGFloat(M_SQRT2)
                                        centerY = initY - (diam*CGFloat(key))/CGFloat(M_SQRT2) - radius/CGFloat(M_SQRT2)
                                        labelF = 7
                                    }
                                }


                                let enNameLabel = UILabel(frame: CGRect(x: 0, y: 0, width: labelS, height: labelS))
                                
                                
                                stapath.move(to: CGPoint(x: centerX, y: centerY))
                                stapath.addArc(withCenter: CGPoint(x: centerX, y: centerY), radius: radius, startAngle: start, endAngle: end, clockwise: true) // 円弧
                                stacpath.move(to: CGPoint(x: centerX,y: centerY))
                                stacpath.addArc(withCenter: CGPoint(x: centerX, y: centerY), radius: wradius, startAngle: start, endAngle: end, clockwise: true) // 円弧
                                enNameLabel.layer.position = CGPoint(x: centerX, y:centerY)
                                
                                let staLayer = CAShapeLayer()
                                staLayer.fillColor = UIColor(hex: colors[i][key]).cgColor
                                staLayer.path = stapath.cgPath
                                
                                let staCLayer = CAShapeLayer()
                                staCLayer.fillColor = UIColor.white.cgColor
                                staCLayer.path = stacpath.cgPath
                                
                                enNameLabel.text = names[i][key]
                                enNameLabel.textAlignment = NSTextAlignment.center
                                enNameLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
                                enNameLabel.numberOfLines = 0
                                enNameLabel.font = UIFont.systemFont(ofSize: labelF)
                                enNameLabel.tag = ids[i][key]
                                
                                staLayer.name = String(ids[i][key])
                                staCLayer.name = String(ids[i][key])
                                touchCustomCell.contentView.layer.addSublayer(staLayer)
                                touchCustomCell.contentView.layer.addSublayer(staCLayer)
                                touchCustomCell.contentView.addSubview(enNameLabel)
                                touchCustomCell.contentView.tag = 100020
                            }
                            }
                        }
                        
                        let radiusAnimation = CABasicAnimation(keyPath: "path")
                        
                        // cornerRadius を 0 -> 100 に変化させるよう設定
                        radiusAnimation.fromValue = startPath.cgPath
                        radiusAnimation.toValue = endPath.cgPath
                        
                        // アニメーション
                        radiusAnimation.duration = 0.1
                        
                        // アニメーションが終了した時の状態を維持する
                        radiusAnimation.isRemovedOnCompletion = false
                        radiusAnimation.fillMode = kCAFillModeForwards
                        
                        // アニメーションが終了したらanimationDidStopを呼び出す
                        radiusAnimation.delegate = self
                        
                        // アニメーションの追加
                        self.myLayer.add(radiusAnimation, forKey: "radiusAnimation")
                    }
                    return touchCustomCell
                }
                else if(indexPath.row == 2){
                    let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell2", for: indexPath)
                    // 再利用するCellを取得する.
                    cell.backgroundColor = UIColor.clear

                    cell.contentView.addSubview(self.secTableView)
                    return cell
                }
                else if(indexPath.row == 3){
                    let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell3", for: indexPath)

                    self.moreLabel.layer.position = CGPoint(x: cell.contentView.center.x, y:cell.contentView.center.y+10)

                    var check = false
                    let subviews = cell.contentView.subviews
                    for subview in subviews {
                        if(subview.tag == 100013){
                            check = true
                        }
                    }
                    
                    if(check == false){
                        // 再利用するCellを取得する.
                        cell.backgroundColor = UIColor.clear
                        cell.contentView.addSubview(self.moreLabel)
                    }
                    return cell
                }
                else if(self.detailCheck){
                    if(indexPath.row == 4){
                        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell4", for: indexPath)
                        // 再利用するCellを取得する.
                        cell.backgroundColor = UIColor.clear
                        
                        cell.contentView.addSubview(self.detailTableView)
                        return cell
                        
                    }
                    else if(indexPath.row == 5){
                        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell5", for: indexPath)
                        // 再利用するCellを取得する.
                        cell.backgroundColor = UIColor.clear
                        
                        cell.contentView.addSubview(self.wikiTableView)
                        return cell
                        
                    }
                }else{
                    if(indexPath.row == 4){
                        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell4", for: indexPath)
                        // 再利用するCellを取得する.
                        cell.backgroundColor = UIColor.clear
                        cell.contentView.addSubview(self.wikiTableView)
                        return cell
                        
                    }
                }

                let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath)
                return cell
            }else if(tableView.tag == 100012){
                    let CellName = "CustomCell" + String(indexPath.row)
                    let customCell: CustomCell = tableView.dequeueReusableCell(withIdentifier: CellName, for: indexPath) as! CustomCell
                    if(self.secDataList[indexPath.row]["title"] != ""){
                        var addFlg:Bool = false
                        if(customCell.contentView.subviews.count == 0 && indexPath.row == 1){
                            addFlg = true
                        }
                        else if(self.indexNo == indexPath.row){
                            addFlg = true
                        }
                        customCell.setCell(self.secDataList[indexPath.row]["title"]!, thumbnail: self.secDataList[indexPath.row]["thumbnail"]!, splBt:self.smallPlayButton, spuBt:self.smallPauseButton, addFlg:addFlg)
                        return customCell
                    }else{
                        let topCell = tableView.dequeueReusableCell(withIdentifier: CellName, for: indexPath)
                        return topCell
                    }
            }else if(tableView.tag == 100018){
                if(indexPath.row == 1){
                    let cell = tableView.dequeueReusableCell(withIdentifier: "thiCustomCell", for: indexPath)
                    if(self.wikiTitleLabel != nil){
                        cell.contentView.addSubview(self.wikiTitleLabel)
                    }
                    if(self.wikiDescLabel != nil){
                        cell.contentView.addSubview(self.wikiDescLabel)
                    }
                    if(self.wikiLabel != nil){
                        cell.contentView.addSubview(self.wikiLabel)
                    }
                    if(self.quoteLabel != nil){
                        cell.contentView.addSubview(self.quoteLabel)
                    }
                    return cell
                }
                else{
                    let topCell = tableView.dequeueReusableCell(withIdentifier: "thiCustomCell", for: indexPath)
                    return topCell
                }
            }else if(tableView.tag == 100022){
                if(indexPath.row == 1){
                    let cell = tableView.dequeueReusableCell(withIdentifier: "detailCustomCell", for: indexPath)
                    if(self.detailLabel != nil){
                        cell.contentView.addSubview(self.detailLabel)
                        cell.contentView.addSubview(self.scheduleLabel)
                    }
                    return cell
                }
                else{
                    let topCell = tableView.dequeueReusableCell(withIdentifier: "detailCustomCell", for: indexPath)
                    return topCell
                }
            }
            
            
        }
        return  tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(self.fullscreen == false){
            if(tableView.tag == 100011){
                if(indexPath.row == 0){
                    return self.view.bounds.width*9/16
                }
                if(indexPath.row == 1){
                    return 300
                }
                if(indexPath.row == 2){
                    return self.secHeight*CGFloat(self.secDataList.count-1)
                }
                if(indexPath.row == 3){
                    return self.secHeight
                }
                if(self.detailCheck){
                    if(indexPath.row == 5){
                        return self.thiHeight+100
                    }else if(indexPath.row == 4){
                        return self.detailHeight
                    }
                }
                else{
                    if(indexPath.row == 4){
                        return self.thiHeight+100
                    }
                }
            }
            else if(tableView.tag == 100012){
                if(indexPath.row == 0 || indexPath.row == self.secDataList.count){
                    return 1
                }else{
                    return self.secHeight
                }
            }
            else if(tableView.tag == 100018){
                if(indexPath.row != 1){
                    return 1
                }else{
                    return self.thiHeight
                }
            }
            else if(tableView.tag == 100022){
                if(indexPath.row != 1){
                    return 1
                }else{
                    return self.detailHeight
                }
            }
        }else{
            return self.view.bounds.height
        }
        return 0
    }
    
    
    // 再生ボタンが押された時に呼ばれるメソッド.
    func onButtonClick(_ sender : UIButton){
        
        // 再生時間を最初に戻して再生.
        videoPlayer.seek(to: CMTimeMakeWithSeconds(0, Int32(NSEC_PER_SEC)))
        videoPlayer.play()
        
    }
    
    // シークバーの値が変わった時に呼ばれるメソッド.
    func onSliderValueChange(_ sender : UISlider){
        
        // 動画の再生時間をシークバーとシンクロさせる.
        videoPlayer.seek(to: CMTimeMakeWithSeconds(Float64(seekBar.value), Int32(NSEC_PER_SEC)))
        
    }

    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        let url = request.url!.absoluteString
        if url.hasPrefix("ready://") {
            self.updateEndLabel()
        }
        return true
    }
    
    func updateEndLabel(){
        let time = Float(myWebView.stringByEvaluatingJavaScript(from: "getDuration();")!)
        if((time) != nil){
            self.durtion = Int(time!)
            let h = NSString(format: "%d", self.durtion/60/60) as String
            var m = NSString(format: "%d", (self.durtion/60)%60) as String
            if(Int(h)! > 0){
                m = NSString(format: "%02d", (self.durtion/60)%60) as String
            }else{
                m = NSString(format: "%d", (self.durtion/60)%60) as String
            }
            let s = NSString(format: "%02d", self.durtion%60) as String
                
            if(Int(h)! > 0){
                self.endLabel.text = "\(h):\(m):\(s)"
            }else{
                self.endLabel.text = "\(m):\(s)"
            }
        }
    }
    
    func goFullScreen(_ sender: UIButton? = nil){
        UIDevice.current.setValue(UIInterfaceOrientation.landscapeLeft.rawValue, forKey: "orientation")
        //self.setFullScreen()
    }
    
    func setFullScreen(){
        let myWebView = self.view.viewWithTag(999999) as? UIWebView
        self.fullscreen = true;
        if(myWebView != nil){
        self.cancelBUtton.isHidden = true
        self.likeButton.isHidden = true
        let webHeight = self.screenHeight*9/16
        let webWidth = self.screenHeight
        myWebView?.frame = CGRect(x: 0, y: 0, width: webWidth, height: webHeight)
        myWebView?.layoutMargins = UIEdgeInsets.zero
        myWebView?.scrollView.contentInset = UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0)
            
        if(ipad == true){
            self.view.backgroundColor = UIColor.black
            myWebView?.layer.position = CGPoint(x: webWidth/2, y:self.view.frame.height/2)
        }else{
            myWebView?.layer.position = CGPoint(x: webWidth/2, y:webHeight/2)
        }

        let playbutton: UIButton = self.view.viewWithTag(100001) as! UIButton
        let pausebutton: UIButton = self.view.viewWithTag(100002) as! UIButton
        let forwardbutton: UIButton = self.view.viewWithTag(100017) as! UIButton
        let backbutton: UIButton = self.view.viewWithTag(100016) as! UIButton
        let menubar: UIView = self.view.viewWithTag(100003)!
        let resizeButton: UIButton = self.view.viewWithTag(100004) as! UIButton
        let myslider: UISlider = self.view.viewWithTag(100005) as! UISlider
//        let startLabel:UILabel = self.view.viewWithTag(100006) as! UILabel
        let endLabel: UILabel = self.view.viewWithTag(100007) as! UILabel
        let loadingLabel: UILabel = self.view.viewWithTag(100008) as! UILabel
        let compressButton: UIButton = self.view.viewWithTag(100009) as! UIButton
        let musicTitle: UILabel = self.view.viewWithTag(100021) as! UILabel
        let titleLabel: UILabel = self.view.viewWithTag(100023) as! UILabel
            
        pausebutton.layer.position = CGPoint(x: webWidth/2, y:self.screenWidth/2)
        playbutton.layer.position = CGPoint(x: webWidth/2, y:self.screenWidth/2)
        forwardbutton.layer.position = CGPoint(x: self.buttonSize/1.5/2, y:self.screenWidth/2)
        backbutton.layer.position = CGPoint(x: webWidth-self.buttonSize/1.5/2, y:self.screenWidth/2)
        if(ipad == true){
            menubar.frame = CGRect(x: 0, y: self.view.frame.height-menubar.frame.size.height-UIApplication.shared.statusBarFrame.height, width: self.view.frame.width, height: menubar.frame.height);
        }else{
            menubar.frame = CGRect(x: 0, y: self.screenWidth/2+(webHeight/2)-menubar.frame.size.height, width: self.screenHeight, height: menubar.frame.height);
        }
            
        let sliderWidth = webWidth-(compressButton.frame.size.width+startLabel.frame.width+endLabel.frame.width+20)
        myslider.frame = CGRect(x: 70, y: (menubar.frame.size.height/2)-14, width: sliderWidth, height: 30)
        endLabel.layer.position = CGPoint(x: myslider.layer.position.x + (myslider.frame.size.width/2) + 20, y:menubar.frame.size.height/2)
        resizeButton.layer.position = CGPoint(x: menubar.frame.size.width-25, y:menubar.frame.size.height/2)
        resizeButton.isHidden = true
        compressButton.isHidden = false
        compressButton.layer.position = CGPoint(x: menubar.frame.size.width-(compressButton.frame.size.width*2.5/4), y:menubar.frame.size.height/2)
        loadingLabel.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            
        musicTitle.frame = CGRect(x: 0, y: (self.screenWidth/2)-(webHeight/2), width: self.screenHeight, height: 55);
        musicTitle.font = UIFont.boldSystemFont(ofSize: 18.0)
            
        titleLabel.frame = CGRect(x: 15, y: -5, width: self.view.frame.size.width, height: self.navHeight)
        titleLabel.font = UIFont.boldSystemFont(ofSize: 24.0)
        
        myWebView?.backgroundColor = UIColor.clear
        self.myTableView.frame = CGRect(x: 0, y: UIApplication.shared.statusBarFrame.height,width: self.view.bounds.width,height: self.view.bounds.height)
        
        let myCell:UITableViewCell =  self.view.viewWithTag(100010) as! UITableViewCell
        myCell.bringSubview(toFront: self.myTableView)
        self.myTableView.reloadData()
        }
    }
    
    func outFullScreen(_ sender: UIButton? = nil){
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        //releaseFullScreen()
    }
    
    func releaseFullScreen(){
        if(ipad == true){
            self.view.backgroundColor = UIColor(white: 1, alpha: 0.2)
        }
        //UIDevice.currentDevice().setValue(UIInterfaceOrientation.Portrait.rawValue, forKey: "orientation")
        self.fullscreen = false
        self.cancelBUtton.isHidden = false
        self.likeButton.isHidden = false
        let myWebView = self.view.viewWithTag(999999) as! UIWebView
        myWebView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.width*9/16)
        myWebView.layoutMargins = UIEdgeInsets.zero
        myWebView.scrollView.contentInset = UIEdgeInsets(top: 0,left: 0,bottom: 0,right: 0);
        
        let playbutton: UIButton = self.view.viewWithTag(100001) as! UIButton
        let pausebutton: UIButton = self.view.viewWithTag(100002) as! UIButton
        let menubar: UIView = self.view.viewWithTag(100003)!
        let resizeButton: UIButton = self.view.viewWithTag(100004) as! UIButton
        let myslider: UISlider = self.view.viewWithTag(100005) as! UISlider
        //let startLabel:UILabel = self.view.viewWithTag(100006) as! UILabel
        let endLabel: UILabel = self.view.viewWithTag(100007) as! UILabel
        let loadingLabel: UILabel = self.view.viewWithTag(100008) as! UILabel
        let compressButton: UIButton = self.view.viewWithTag(100009) as! UIButton
        let backbutton: UIButton = self.view.viewWithTag(100017) as! UIButton
        let forwardbutton: UIButton = self.view.viewWithTag(100016) as! UIButton
        let musicTitle: UILabel = self.view.viewWithTag(100021) as! UILabel
        let titleLabel: UILabel = self.view.viewWithTag(100023) as! UILabel
        
        playbutton.layer.position = CGPoint(x: self.myWebView.frame.width/2, y:self.myWebView.frame.height/2)
        pausebutton.layer.position = CGPoint(x: self.myWebView.frame.width/2, y:self.myWebView.frame.height/2)
        
        forwardbutton.layer.position = CGPoint(x: self.myWebView.frame.width-(self.buttonSize/1.5/2), y:self.myWebView.frame.height/2)
        backbutton.layer.position = CGPoint(x: self.buttonSize/1.5/2, y:self.myWebView.frame.height/2)

        let menubarWidth = UIScreen.main.bounds.size.width
        let menubarHeight = UIScreen.main.bounds.size.width*9/(16*4.2)

        menubar.frame = CGRect(x: 0, y: 0, width: menubarWidth, height: menubarHeight)
        menubar.layer.position = CGPoint(x: menubarWidth/2, y:self.myWebView.frame.height-menubarHeight/2)
        
        let sliderWidth = self.view.frame.size.width-(compressButton.frame.size.width+startLabel.frame.width+endLabel.frame.width+20)
        myslider.frame = CGRect(x: 70, y: (menubar.frame.height/2)-14, width: sliderWidth, height: 30)
        endLabel.layer.position = CGPoint(x: myslider.layer.position.x + (myslider.frame.size.width/2) + 20, y:menubar.frame.size.height/2)
        resizeButton.layer.position = CGPoint(x: menubar.frame.size.width-(self.resizeButton.frame.size.width*2.5/4), y:menubar.frame.size.height/2)
        resizeButton.isHidden = false
        compressButton.isHidden = true
        compressButton.layer.position = CGPoint(x: menubar.frame.size.width-25, y:menubar.frame.size.height/2)
        loadingLabel.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.width*9/16)
        musicTitle.frame = CGRect(x: 0, y: 3, width: menubarWidth, height: 22)
        musicTitle.font = UIFont.boldSystemFont(ofSize: 15.0)
        
        titleLabel.frame = CGRect(x: 20, y: -5, width: menubarWidth, height: self.navHeight)
        titleLabel.font = UIFont.boldSystemFont(ofSize: 20.0)
        
        myWebView.backgroundColor = UIColor.clear
        self.myTableView.frame = CGRect(x: 0, y: UIApplication.shared.statusBarFrame.height+self.navHeight,width: self.view.bounds.width,height:self.view.bounds.height-self.navHeight)
        self.myTableView.reloadData();
    }

//    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
//        let orientation: UIInterfaceOrientationMask = [UIInterfaceOrientationMask.Portrait, UIInterfaceOrientationMask.LandscapeLeft, UIInterfaceOrientationMask.LandscapeRight,UIInterfaceOrientationMask.PortraitUpsideDown]
//        return orientation
//    }
    
    override func viewDidAppear(_ animated: Bool) {
        // 端末の向きがかわったらNotificationを呼ばす設定.
    }
    
    // 端末の向きがかわったら呼び出される.
    func onOrientationChange(_ notification: Foundation.Notification){
        // 現在のデバイスの向きを取得.
        let deviceOrientation: UIDeviceOrientation!  = UIDevice.current.orientation
        // 向きの判定.
        if UIDeviceOrientationIsLandscape(deviceOrientation) {
            //横向きの判定.
            setFullScreen()
        } else if UIDeviceOrientationIsPortrait(deviceOrientation){
            //縦向きの判定.
            switch UIDevice.current.orientation{
            case .portrait:
                releaseFullScreen()
            case .portraitUpsideDown:
                break
            default:
                releaseFullScreen()
            }
//            releaseFullScreen()
//            print(deviceOrientation)
            
            
        }
        else{
            if(self.view.frame.size.width <= self.screenWidth){
                //UIDevice.currentDevice().setValue(UIInterfaceOrientation.LandscapeLeft.rawValue, forKey: "orientation")
            }
            else{
                setFullScreen()
            }
        }
    }
    //指定方向に自動的に変更するか？
    override var shouldAutorotate : Bool{
        return true
    }
    
    func play(_ sender: UIButton){
        self.userState = 1
        self.tapTime = Date()
        self.fullscreen = false;
        let myWebView = self.view.viewWithTag(999999) as! UIWebView
        myWebView.stringByEvaluatingJavaScript(from: "pplay();")
        let playbutton: UIButton = self.view.viewWithTag(100001) as! UIButton
        let pausebutton: UIButton = self.view.viewWithTag(100002) as! UIButton
        let smallPlaybutton: UIButton = self.view.viewWithTag(100014) as! UIButton
        let smallPausebutton: UIButton = self.view.viewWithTag(100015) as! UIButton
        let myCell:UITableViewCell =  self.view.viewWithTag(100010) as! UITableViewCell
        myCell.bringSubview(toFront: pausebutton)
        playbutton.isHidden = true
        pausebutton.isHidden = false
        smallPlaybutton.isHidden = true
        smallPausebutton.isHidden = false
    }

    func pause(_ sender: UIButton){
        self.userState = 0
        self.tapTime = Date()
        self.fullscreen = false;
        let myWebView = self.view.viewWithTag(999999) as! UIWebView
        myWebView.stringByEvaluatingJavaScript(from: "ppause();")
        let playbutton: UIButton = self.view.viewWithTag(100001) as! UIButton
        let pausebutton: UIButton = self.view.viewWithTag(100002) as! UIButton
        let smallPlaybutton: UIButton = self.view.viewWithTag(100014) as! UIButton
        let smallPausebutton: UIButton = self.view.viewWithTag(100015) as! UIButton
        let myCell:UITableViewCell =  self.view.viewWithTag(100010) as! UITableViewCell
        myCell.bringSubview(toFront: playbutton)
        pausebutton.isHidden = true
        playbutton.isHidden = false
        smallPausebutton.isHidden = true
        smallPlaybutton.isHidden = false
    }
    
    func forwardFunc(){
        var nextIndexNo = self.indexNo+1
        if(self.secDataList.count == nextIndexNo){
            nextIndexNo = 1
            self.myWebView.stringByEvaluatingJavaScript(from: "playVideoAt(\(0));")
        }else{
            self.myWebView.stringByEvaluatingJavaScript(from: "nextVideo();")
        }
        self.indexNo = nextIndexNo
    }
    
    func forward(_ sender: UIButton){
        self.forwardFunc()
    }
    
    func backFunc(){
        var nextIndexNo = self.indexNo-1
        if(nextIndexNo == 0){
            nextIndexNo = self.secDataList.count-1
            self.myWebView.stringByEvaluatingJavaScript(from: "playVideoAt(\(nextIndexNo-1));")
        }else{
            self.myWebView.stringByEvaluatingJavaScript(from: "previousVideo();")
        }
        
        self.indexNo = nextIndexNo
    }
    func back(_ sender: UIButton){
        self.backFunc()
    }


    internal func endSlider(_ sender : UISlider){
        let value = mySlider.value
        let seekTime = Int((value * Float(self.durtion))/Float(self.mySlider.maximumValue))
        let myWebView = self.view.viewWithTag(999999) as! UIWebView
        myWebView.stringByEvaluatingJavaScript(from: "seek(\(seekTime));")
        var state = 0
        while(state == 3){
            state = Int(myWebView.stringByEvaluatingJavaScript(from: "getStatus();")!)!
        }
        self.touchFlg = false
        self.tapTime = Date()
    }
    
    internal func stopSlider(_ sender : UISlider){
        self.touchFlg = true
        self.tapTime = Date()
    }
    
    func updateSmallButton(){
        for i in 0...self.secDataList.count-1-1 {
            let tmpNextIndexPath = IndexPath(row: i, section: 0)
            let precell = self.secTableView.cellForRow(at: tmpNextIndexPath)  // do not confuse this with the similarly named `tableView:cellForRowAtIndexPath:` method that you've implemented
            let presubviews = precell!.contentView.subviews
            for subview in presubviews {
                if(subview.tag == 100014 || subview.tag == 100015){
                    subview.removeFromSuperview()
                }
            }
        }
        
        let tmpNextIndexPath = IndexPath(row: self.indexNo, section: 0)
        let nextCell = self.secTableView.cellForRow(at: tmpNextIndexPath)  // do not confuse this with the similarly named `tableView:cellForRowAtIndexPath:` method that you've implemented
        let nextsubviews = nextCell!.contentView.subviews
        for subview in nextsubviews {
            if subview.isKind(of: UIImageView.self) {
                self.smallPlayButton.layer.position = CGPoint(x: subview.center.x, y:subview.center.y)
                self.smallPauseButton.layer.position = CGPoint(x: subview.center.x, y:subview.center.y)
            }
        }
        nextCell?.contentView.addSubview(self.smallPlayButton)
        nextCell?.contentView.addSubview(self.smallPauseButton)
        
        self.musicTitelLabel.text =  "              " + (self.secDataList[self.indexNo]["title"])!
    }
    
    func setLoading(_ sender : UISlider){
        let state: String = myWebView.stringByEvaluatingJavaScript(from: "getStatus();")!
        if(state != ""){
            if(Int(state) == 3){
                self.loadingLabel.isHidden = false
                
                let playbutton: UIButton = self.view.viewWithTag(100001) as! UIButton
                let pausebutton: UIButton = self.view.viewWithTag(100002) as! UIButton
                let menubar: UIView = self.view.viewWithTag(100003)!
                let forwardbutton: UIButton = self.view.viewWithTag(100016) as! UIButton
                let backbutton: UIButton = self.view.viewWithTag(100017) as! UIButton
                let musicTitle: UILabel = self.view.viewWithTag(100021) as! UILabel
                let titleLabel: UILabel = self.view.viewWithTag(100023) as! UILabel
                
                pausebutton.isHidden = true
                playbutton.isHidden = true
                menubar.isHidden = true
                forwardbutton.isHidden = true
                backbutton.isHidden = true
                musicTitle.isHidden =  true
                titleLabel.isHidden = false
                self.disp = false
            }else if(Int(state) == -1){
                self.loadingLabel.fadeOut(.fast)
                let titleLabel: UILabel = self.view.viewWithTag(100023) as! UILabel
                titleLabel.isHidden = false
            }else if(Int(state) == 1 || Int(state) == 5) {
                if((self.preState) != nil){
                    self.loadingLabel.fadeOut(.fast)
                    if(self.preState == 3){
                        
                        //self.loadingTimer.invalidate()
                        let menubar: UIView = self.view.viewWithTag(100003)!
                        menubar.fadeIn(.fast)
                        let forwardbutton: UIButton = self.view.viewWithTag(100016) as! UIButton
                        forwardbutton.fadeIn(.fast)
                        let backbutton: UIButton = self.view.viewWithTag(100017) as! UIButton
                        backbutton.fadeIn(.fast)
                        let musicTitle: UILabel = self.view.viewWithTag(100021) as! UILabel
                        musicTitle.fadeIn(.fast)
                        
                        let titleLabel: UILabel = self.view.viewWithTag(100023) as! UILabel
                        titleLabel.fadeOut(.fast)
                        
                        self.disp = true
                        self.tapTime = Date()
                    }
                }
            }else if(Int(state) == 2){
                if((self.preState) != nil){
                    self.loadingLabel.fadeOut(.fast)
                    if(self.preState == 3){
                        //self.loadingTimer.invalidate()
                        let playbutton: UIButton = self.view.viewWithTag(100001) as! UIButton
                        let menubar: UIView = self.view.viewWithTag(100003)!
                        let forwardbutton: UIButton = self.view.viewWithTag(100016) as! UIButton
                        let backbutton: UIButton = self.view.viewWithTag(100017) as! UIButton
                        let musicTitle: UILabel = self.view.viewWithTag(100021) as! UILabel
                        //let titleLabel: UILabel = self.view.viewWithTag(100023) as! UILabel
                        
                        menubar.fadeIn(.normal)
                        playbutton.fadeIn(.normal)
                        forwardbutton.fadeIn(.normal)
                        backbutton.fadeIn(.normal)
                        musicTitle.fadeIn(.normal)
                        //titleLabel.fadeOut(.Fast)
                        
                        self.disp = true
                        self.tapTime = Date()
                    }
                }
            }else if(Int(state) == 0){
                
                //self.loadingTimer.invalidate()
            }
        }
        self.preState = Int(state)
    }
    
    func getLoadingTimer(_ timer : Timer){
        let state: String = myWebView.stringByEvaluatingJavaScript(from: "getStatus();")!
        if(state != ""){
            if(Int(state) == 3){
                self.titleLabel.isHidden = false
                self.loadingLabel.isHidden = false
                let playbutton: UIButton = self.view.viewWithTag(100001) as! UIButton
                let pausebutton: UIButton = self.view.viewWithTag(100002) as! UIButton
                let menubar: UIView = self.view.viewWithTag(100003)!
                let forwardbutton: UIButton = self.view.viewWithTag(100016) as! UIButton
                let backbutton: UIButton = self.view.viewWithTag(100017) as! UIButton
                let musicTitle: UILabel = self.view.viewWithTag(100021) as! UILabel
                let titleLabel: UILabel = self.view.viewWithTag(100023) as! UILabel
                
                pausebutton.isHidden = true
                playbutton.isHidden = true
                menubar.isHidden = true
                forwardbutton.isHidden = true
                backbutton.isHidden = true
                musicTitle.isHidden = true
                titleLabel.isHidden = false
                
                self.disp = false
                self.updateEndLabel()
                
                if(self.preState != nil){
                    if(self.preState == 1){
                        let index: String = myWebView.stringByEvaluatingJavaScript(from: "getIndex();")!
                        if(index != ""){
                            self.indexNo = Int(index)!+1
                            if(self.secDataList.count == self.indexNo){
                                self.indexNo = 1
                                self.myWebView.stringByEvaluatingJavaScript(from: "playVideoAt(\(0));")
                            }
                        }
                    }
                    self.updateSmallButton()
                }
            }else if(Int(state) == -1){
                self.titleLabel.fadeOut(.fast)
                let titleLabel: UILabel = self.view.viewWithTag(100023) as! UILabel
                titleLabel.isHidden = false
            }else if(Int(state) == 1 || Int(state) == 5) {
                self.triger = true
                if((self.preState) != nil){
                    self.loadingLabel.fadeOut(.fast)
                    if(self.preState == 3){
                        self.titleLabel.fadeIn(.slow)
                        //self.loadingTimer.invalidate()
                        let menubar: UIView = self.view.viewWithTag(100003)!
                        menubar.fadeIn(.fast)
                        let forwardbutton: UIButton = self.view.viewWithTag(100016) as! UIButton
                        forwardbutton.fadeIn(.fast)
                        let backbutton: UIButton = self.view.viewWithTag(100017) as! UIButton
                        backbutton.fadeIn(.fast)
                        let musicTitle: UILabel = self.view.viewWithTag(100021) as! UILabel
                        musicTitle.fadeIn(.fast)
                        
                        let titleLabel: UILabel = self.view.viewWithTag(100023) as! UILabel
                        titleLabel.fadeOut(.fast)
                        
                        self.disp = true
                        self.tapTime = Date()
                    }
                }
            }else if(Int(state) == 2){
                if((self.preState) != nil){
                    self.loadingLabel.fadeOut(.fast)
                    if(self.preState == 3){
                        //self.loadingTimer.invalidate()
                        let playbutton: UIButton = self.view.viewWithTag(100001) as! UIButton
                        let menubar: UIView = self.view.viewWithTag(100003)!
                        let forwardbutton: UIButton = self.view.viewWithTag(100016) as! UIButton
                        let backbutton: UIButton = self.view.viewWithTag(100017) as! UIButton
                        let musicTitle: UILabel = self.view.viewWithTag(100021) as! UILabel
                        //let titleLabel: UILabel = self.view.viewWithTag(100023) as! UILabel

                        menubar.fadeIn(.normal)
                        playbutton.fadeIn(.normal)
                        forwardbutton.fadeIn(.normal)
                        backbutton.fadeIn(.normal)
                        musicTitle.fadeIn(.normal)
                        
                        //titleLabel.fadeOut(.Fast)
                        
                        self.disp = true
                        self.tapTime = Date()
                    }
                }
            }else if(Int(state) == 0 && self.triger == true){
                self.triger = false
            }
        }
        self.preState = Int(state)
    }
    
    func gestureRecognizer(
        _ gestureRecognizer: UIGestureRecognizer,
        shouldRecognizeSimultaneouslyWith
        otherGestureRecognizer: UIGestureRecognizer
        ) -> Bool {
            return true
    }
    
    func tapCount(_ timer : Timer){
        let now = Date()
        let time = Float(now.timeIntervalSince(self.tapTime))
        if(time > 1){
            if((self.view.viewWithTag(100001)) != nil){
                let playbutton: UIButton = self.view.viewWithTag(100001) as! UIButton
                playbutton.fadeOut(.normal)
            }
            if(self.view.viewWithTag(100002) != nil){
                let pausebutton: UIButton = self.view.viewWithTag(100002) as! UIButton
                pausebutton.fadeOut(.normal)
            }
            
            if(self.view.viewWithTag(100003) != nil){
                let menubar: UIView = self.view.viewWithTag(100003)!
                menubar.fadeOut(.fast)
            }
            
            if(self.view.viewWithTag(100016) != nil){
                let forawrdbutton: UIButton = self.view.viewWithTag(100016) as! UIButton
                forawrdbutton.fadeOut(.normal)
            }

            if(self.view.viewWithTag(100017) != nil){
                let backbutton: UIButton = self.view.viewWithTag(100017) as! UIButton
                backbutton.fadeOut(.normal)
            }

            if(self.view.viewWithTag(100021) != nil){
                let musicTitle: UILabel = self.view.viewWithTag(100021) as! UILabel
                musicTitle.fadeOut(.normal)
            }
            
            if(self.view.viewWithTag(100023) != nil){
                let titleLabel: UILabel = self.view.viewWithTag(100023) as! UILabel
                if(titleLabel.isHidden == true){
                    titleLabel.fadeIn(.normal)
                }
            }
            

            self.disp = false
        }
    }
    
    func countSlider(_ timer : Timer){
        let time = Float(myWebView.stringByEvaluatingJavaScript(from: "getTime();")!)
        if((time) != nil){
            var itime = Int(time!)
            
            let state = Int(myWebView.stringByEvaluatingJavaScript(from: "getStatus();")!)
            if(state == 0){
                itime = self.durtion
            }
            let h = NSString(format: "%d", itime/60/60) as String
            var m = NSString(format: "%d", (itime/60)%60) as String
            if(Int(h)! > 0){
                m = NSString(format: "%02d", (itime/60)%60) as String
            }else{
                m = NSString(format: "%d", (itime/60)%60) as String
            }
            let s = NSString(format: "%02d", itime%60) as String
            if(Int(h)! > 0){
                if(self.startLabel.text != "\(h):\(m):\(s)"){
                    self.startLabel.text = "\(h):\(m):\(s)"
                }
            }else{
                if(self.startLabel.text != "\(m):\(s)"){
                    self.startLabel.text = "\(m):\(s)"
                }
            }
            
            
            if((self.durtion) != nil && self.touchFlg == false && state != 3){
                var ntime = Float(myWebView.stringByEvaluatingJavaScript(from: "getTime();")!)
                if(state == 0){
                    ntime = Float(self.durtion)
                }
                if((ntime) != nil){
                    self.mySlider.value = Float(Double(Int(ntime!))/Double(self.durtion))*self.mySlider.maximumValue
                }
            }
        }
    }
    
    func viewTap(_ gesture: UIGestureRecognizer? = nil) {
        self.tapTime = Date()
        let myWebView = self.view.viewWithTag(999999) as! UIWebView
        if let theView = gesture!.view {
            if(theView.tag == 999999){
                let state = myWebView.stringByEvaluatingJavaScript(from: "getStatus();")
                //-1 – 未開始
                //0 – 終了
                //1 – 再生中
                //2 – 一時停止
                //3 – バッファリング中
                //5 – 頭出し済み
                if(Int(state!) == 1){
                    let pausebutton: UIButton = self.view.viewWithTag(100002) as! UIButton
                    if(self.disp == false){
                        pausebutton.fadeIn(.fast)
                    }else{
                        pausebutton.fadeOut(.fast)
                    }
                }else if(Int(state!) == 2 || Int(state!) == 5 || Int(state!) == -1 ){
                    let playbutton: UIButton = self.view.viewWithTag(100001) as! UIButton
                    if(self.disp == false){
                        playbutton.fadeIn(.fast)
                    }else{
                        playbutton.fadeOut(.fast)
                    }
                }

                let menubar: UIView = self.view.viewWithTag(100003)!
                if(self.disp == false){
                    menubar.fadeIn(.fast)
                }else{
                    menubar.fadeOut(.fast)
                }

                let forawrdbutton: UIButton = self.view.viewWithTag(100016) as! UIButton
                if(self.disp == false){
                    forawrdbutton.fadeIn(.fast)
                }else{
                    forawrdbutton.fadeOut(.fast)
                }

                let nextbutton: UIButton = self.view.viewWithTag(100017) as! UIButton
                if(self.disp == false){
                    nextbutton.fadeIn(.fast)
                }else{
                    nextbutton.fadeOut(.fast)
                }

                let musicTitle: UILabel = self.view.viewWithTag(100021) as! UILabel
                if(self.disp == false){
                    musicTitle.fadeIn(.fast)
                }else{
                    musicTitle.fadeOut(.fast)
                }

                let titleLabel: UILabel = self.view.viewWithTag(100023) as! UILabel
                if(self.disp == false){
                    titleLabel.fadeOut(.fast)
                }else{
                    titleLabel.fadeIn(.fast)
                }

                if(self.disp == true){
                    self.disp = false
                }else{
                    self.disp = true
                }
            }
        }
    }
    
    func moreLoad(_ gesture: UIGestureRecognizer? = nil) {
        self.moloadFunc();
    }
    
    func moloadFunc(){
        if(self.secDataList.count != self.musicList.count){
            var max = self.secDataList.count+plusNum
            if(self.secDataList.count+plusNum > self.musicList.count){
                max = self.musicList.count
            }
            for i in self.secDataList.count...max-1 {
                self.secDataList.append(self.musicList[i])
            }
            
            self.myTableView.reloadData()
            self.secTableView.reloadData()
            
            if(self.secDataList.count == self.musicList.count){
                self.moreLabel.font = UIFont.boldSystemFont(ofSize: 18.0)
                self.moreLabel.text = NSLocalizedString("Youtube",comment: "")
            }
        }else{
            self.fromGowiki = false
            var encodedString = self.name.escapeStr()
            encodedString = encodedString.replacingOccurrences(of: "%20", with: "+", options: [], range: nil)
            self.url = URL(string: "https://m.youtube.com/results?search_query=" + encodedString + "&sm=1")!
            if #available(iOS 9.0, *) {
                let _brow = SFSafariViewController(url: self.url, entersReaderIfAvailable: false)
                _brow.delegate = self
                _brow.view.tintColor = UIColor(hex: "#222831")
                present(_brow, animated: true, completion: nil)
            } else {
                // Fallback on earlier versions
                performSegue(withIdentifier: "GoOutPageSegue", sender: nil)
            }
        }

    }

    func goWikipedia(_ gesture: UIGestureRecognizer? = nil) {
        self.myWebView.stringByEvaluatingJavaScript(from: "pplay();")
        kCMTextMarkupGenericFontName_ProportionalSansSerif
        self.fromGowiki = true
        if #available(iOS 9.0, *) {
            let _brow = SFSafariViewController(url: self.wikiUrl, entersReaderIfAvailable:false)
            _brow.delegate = self
            _brow.view.tintColor = UIColor(hex: "#222831")
            present(_brow, animated: true, completion: nil)
        } else {
            // Fallback on earlier versions
            performSegue(withIdentifier: "GoOutPageSegue", sender: nil)
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "GoOutPageSegue") {
            let nav: UINavigationController  = segue.destination as! UINavigationController
            let nextVC:OutPageViewController = nav.visibleViewController as! OutPageViewController
            if(self.fromGowiki == true){
                nextVC.url = self.wikiUrl
            }else{
                nextVC.url = self.url
            }
        }else if(segue.identifier == "LoginFromFaboriteSegue"){
            let nextVC: LoginViewController = segue.destination as! LoginViewController
            nextVC.detailViewDelegate = self
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        //self.delegate.didFinished(self.next_id)
    }
    
    func addRemoteControlEvent() {
        self.commandCenter = MPRemoteCommandCenter.shared()
        self.commandCenter.togglePlayPauseCommand.addTarget(self, action: #selector(DetailViewController.remoteTogglePlayPause(_:)))
        self.commandCenter.playCommand.addTarget(self, action: #selector(DetailViewController.remotePlay(_:)))
        self.commandCenter.pauseCommand.addTarget(self, action: #selector(DetailViewController.remotePause(_:)))
        self.commandCenter.nextTrackCommand.addTarget(self, action: #selector(DetailViewController.remoteNextTrack(_:)))
        self.commandCenter.previousTrackCommand.addTarget(self, action: #selector(DetailViewController.remotePrevTrack(_:)))
    }

    func removeRemoteControlEvent() {
        self.commandCenter.togglePlayPauseCommand.removeTarget(self, action: #selector(DetailViewController.remoteTogglePlayPause(_:)))
        self.commandCenter.playCommand.removeTarget(self, action: #selector(DetailViewController.remotePlay(_:)))
        self.commandCenter.pauseCommand.removeTarget(self, action: #selector(DetailViewController.remotePause(_:)))
        self.commandCenter.nextTrackCommand.removeTarget(self, action: #selector(DetailViewController.remoteNextTrack(_:)))
        self.commandCenter.previousTrackCommand.removeTarget(self, action: #selector(DetailViewController.remotePrevTrack(_:)))
    }

    func remoteTogglePlayPause(_ event: MPRemoteCommandEvent) {
        if(self.userState == 1 ){
            self.userState = 0
        }else{
            self.userState = 1
        }
        // イヤホンのセンターボタンを押した時の処理
        // （再生中ならポーズ。停止中なら再生。といった処理を書く）
        // （略）
        
    }
    
    func remotePlay(_ event: MPRemoteCommandEvent) {
        self.userState = 1
        // プレイボタンが押された時の処理
        // （略）
        
    }
    
    func remotePause(_ event: MPRemoteCommandEvent) {
        self.userState = 0
        // ポーズボタンが押された時の処理
        // （略）
    }
    
    func remoteNextTrack(_ event: MPRemoteCommandEvent) {
        var nextIndexNo = self.indexNo+1
//        var preIndexNo:Int = self.indexNo
        if(self.musicList.count == nextIndexNo){
            nextIndexNo = 1
//            preIndexNo = self.secDataList.count-1
            self.myWebView.stringByEvaluatingJavaScript(from: "playVideoAt(\(0));")
        }else{
            self.myWebView.stringByEvaluatingJavaScript(from: "nextVideo();")
        }
                
        self.indexNo = nextIndexNo
        self.userState = 1
    }
    
    func remotePrevTrack(_ event: MPRemoteCommandEvent) {
        var nextIndexNo = self.indexNo-1
//        var preIndexNo:Int = self.indexNo
        if(nextIndexNo == 0){
            nextIndexNo = self.musicList.count-2
//            preIndexNo = 1
            self.myWebView.stringByEvaluatingJavaScript(from: "playVideoAt(\(nextIndexNo));")
        }else{
            self.myWebView.stringByEvaluatingJavaScript(from: "previousVideo();")
        }
        self.indexNo = nextIndexNo
        self.userState = 1
    }
    
    func enterForeground(_ notification: Foundation.Notification){
        self.tapTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(DetailViewController.tapCount(_:)), userInfo: nil, repeats: true)
        self.countTimer =  Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(DetailViewController.countSlider(_:)), userInfo: nil, repeats: true)
        self.loadingTimer =  Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(DetailViewController.getLoadingTimer(_:)), userInfo: nil, repeats: true)

        let index: String = myWebView.stringByEvaluatingJavaScript(from: "getIndex();")!
        if(index != ""){
            self.indexNo = Int(index)!+1
        }
        var nextIndexNo = self.indexNo
        
        if(self.secDataList.count == nextIndexNo){
            nextIndexNo = 0
        }

        if(self.indexNo>self.secDataList.count-1){
            let count = ((self.indexNo+1)/5)-(self.secDataList.count/5)-1
            for _ in 0...count {
                self.moloadFunc()
            }
        }
        
        
        self.updateSmallButton()
        
        if(self.userState == 1){
            self.playPreState = 0
            self.playCheckCount = 0
            self.checkPlayTimer =  Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(DetailViewController.checkPlayTimer(_:)), userInfo: nil, repeats: true)
        }
    }
    
    func checkPlayTimer(_ timer : Timer){
        let state: String = myWebView.stringByEvaluatingJavaScript(from: "getStatus();")!
        if(Int(state) == 2 && self.playPreState == 1){
            self.myWebView.stringByEvaluatingJavaScript(from: "pplay();")
            timer.invalidate()
        }else if(Int(state) == 1 && self.playPreState == 1){
            self.playCheckCount = self.playCheckCount + 1
        }else{
             self.playCheckCount = 0
        }
        if(self.playCheckCount > 50){
            timer.invalidate()
        }
        self.playPreState = Int(state)
    }
    
    func enterBackground(_ notification: Foundation.Notification){
        self.loadingTimer.invalidate()
        self.countTimer.invalidate()
        self.loadingTimer.invalidate()
        if((self.checkPlayTimer) != nil){
            self.checkPlayTimer.invalidate()
        }
    }
}

extension TouchCustomCell{
    internal override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch = touches.first!
        let views = self.subviews // Subviewsの配列を取得
        var next_id = 999999
        var check = false
        for subview in views {
            if(subview.tag == 100020){
                let point = touch.location(in: subview)
                let subs = subview.layer.sublayers
                if((subs) != nil){
                    for sub in subs!{
                        if sub.name != nil {
                        let layear = sub as! CAShapeLayer
                            if (layear.path?.contains(point))! {
                                next_id = Int(sub.name!)!
                                layear.opacity = 0
                                let tempLabel = subview.viewWithTag(Int(sub.name!)!) as! UILabel
                                tempLabel.textColor = UIColor.clear
                                check = true
                            }
                        }
                    }
                }
            }
        }
        if(check){
            let vc  = UIApplication.topViewController() as! DetailViewController
            vc.closeView(next_id)
        }
    }
}
extension String {
    
    func escapeStr() -> String {
        let raw: NSString = self as NSString
        let str = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,raw,"[]." as CFString!,":/?&=;+!@#$()',*" as CFString!,CFStringConvertNSStringEncodingToEncoding(String.Encoding.utf8.rawValue))
        return str as! String
    }
}
extension UIApplication {
    class func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        return base
    }
}
