//
//  SettingLicenceViewController.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2016/02/06.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import UIKit
import SafariServices
import GoogleMobileAds

class SettingLicenceViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, SFSafariViewControllerDelegate, UIActionSheetDelegate,UIAlertViewDelegate, GADBannerViewDelegate {
    
    @IBOutlet var SettingTable: UITableView!
    var table_data = Array<TableData>()
    var url:String!
    
    struct TableData
    {
        var section:Bool = false
        var data = String()
        var icon = String()
        init(){}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let staHeight = UIApplication.shared.statusBarFrame.height
        // Cellのマージン.
        SettingTable.contentInset = UIEdgeInsetsMake(0, 0, staHeight + addHeight + (self.navigationController?.navigationBar.frame.size.height)!,0)
        SettingTable.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, staHeight + addHeight + (self.navigationController?.navigationBar.frame.size.height)!,0)

        let admobView = GADBannerView(adSize:kGADAdSizeSmartBannerPortrait)
        addHeight = (self.view.frame.width * admobView.frame.height)/admobView.frame.width
        admobView.frame.size = CGSize(width: self.view.frame.width, height: addHeight)
        admobView.frame.origin = CGPoint(x: 0, y: self.view.frame.height-addHeight)
        
        admobView.adUnitID =  "ca-app-pub-8256083710740545/3348041715"
        admobView.delegate = self
        admobView.rootViewController = self
        let admobRequest:GADRequest = GADRequest()
        
        if AdMobTest {
            if SimulatorTest {
                admobRequest.testDevices = [kGADSimulatorID]
            }
            else {
                admobRequest.testDevices = [TEST_DEVICE_ID]
            }
            
        }
        admobView.load(admobRequest)

        let v:UIView = UIView(frame: CGRect.zero)
        v.backgroundColor = UIColor.clear
        self.SettingTable.tableFooterView = v

        self.SettingTable.separatorInset = UIEdgeInsets.zero;
        SettingTable.dataSource = self
        SettingTable.delegate = self
        
        var new_elements:TableData
        
        new_elements = TableData()
        new_elements.data = NSLocalizedString("iconmess",comment: "")
        new_elements.section = true
        new_elements.icon = ""
        table_data.append(new_elements)
        
        new_elements = TableData()
        new_elements.data = NSLocalizedString("icons8.com",comment: "")
        new_elements.section = false
        new_elements.icon = "st_licence_Icons8"
        table_data.append(new_elements)
        
        new_elements = TableData()
        new_elements.data = NSLocalizedString("imagemess",comment: "")
        new_elements.section = true
        new_elements.icon = ""
        table_data.append(new_elements)

        new_elements = TableData()
        new_elements.data = NSLocalizedString("flickr - LuxTonnerre",comment: "")
        new_elements.section = false
        new_elements.icon = "st_licence_flickr"
        table_data.append(new_elements)

        new_elements = TableData()
        new_elements.data = NSLocalizedString("flickr - Kevin Utting",comment: "")
        new_elements.section = false
        new_elements.icon = "st_licence_flickr"
        table_data.append(new_elements)

        new_elements = TableData()
        new_elements.data = NSLocalizedString("flickr - twiggy_34",comment: "")
        new_elements.section = false
        new_elements.icon = "st_licence_flickr"
        table_data.append(new_elements)
        
        new_elements = TableData()
        new_elements.data = NSLocalizedString("flickr - jareed",comment: "")
        new_elements.section = false
        new_elements.icon = "st_licence_flickr"
        table_data.append(new_elements)

        new_elements = TableData()
        new_elements.data = NSLocalizedString("flickr - ryo katsuma",comment: "")
        new_elements.section = false
        new_elements.icon = "st_licence_flickr"
        table_data.append(new_elements)

        new_elements = TableData()
        new_elements.data = NSLocalizedString("flickr - Kentaro Ohno",comment: "")
        new_elements.section = false
        new_elements.icon = "st_licence_flickr"
        table_data.append(new_elements)
        
        new_elements = TableData()
        new_elements.data = NSLocalizedString("flickr - Akiko Yokoyama",comment: "")
        new_elements.section = false
        new_elements.icon = "st_licence_flickr"
        table_data.append(new_elements)
//        new_elements = TableData()
//        new_elements.data = NSLocalizedString("citations",comment: "")
//        new_elements.section = true
//        new_elements.icon = ""
//        table_data.append(new_elements)
        
//        new_elements = TableData()
//        new_elements.data = NSLocalizedString("Rolling Stone",comment: "")
//        new_elements.section = false
//        new_elements.icon = "st_licence_outlink"
//        table_data.append(new_elements)

        self.view.addSubview(admobView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(table_data[indexPath.row].section == false){
            let cell = SettingTable.dequeueReusableCell(withIdentifier: "SetingTableViewCell", for: indexPath) as! SettingTableViewCell
            cell.TitleLabel.text = table_data[indexPath.row].data
            cell.TitleLabel.textColor = UIColor(hex: "#222831")
            cell.settingImage.image = UIImage(named:table_data[indexPath.row].icon)
            return cell
        }else{
            let cell = SettingTable.dequeueReusableCell(withIdentifier: "CustomHeaderCell", for: indexPath) as! CustomHeaderCell
            cell.headerLabel.text = table_data[indexPath.row].data
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.headerLabel.textColor = UIColor(hex: "#222831")
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection    section: Int) -> Int
    {
        return table_data.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        if(table_data[indexPath.row].section == false){
        //            return
        //        }else{
        //            return 60
        //        }
        return 55
    }
    
    //    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    //    {
    //        return table_data.count
    //    }
    
    
    //    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //        return 70.0
    //    }
//    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
//        let orientation: UIInterfaceOrientationMask = [UIInterfaceOrientationMask.Portrait]
//        return orientation
//    }
    
    //指定方向に自動的に変更するか？
//    override func shouldAutorotate() -> Bool{
//        return true
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let nv = navigationController {
            // 表示
            nv.setNavigationBarHidden(false, animated: true)
            // タイトルを設定する.
            self.navigationItem.title = NSLocalizedString("License",comment: "")
//            navHeight = (self.navigationController?.navigationBar.frame.size.height)!
        }
    }
    @IBAction func close(_ sender: AnyObject) {
        let newRootVC = UIViewController()
        let navigationController = UINavigationController(rootViewController: newRootVC)
        navigationController.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ table: UITableView, didSelectRowAt indexPath:IndexPath) {
        
        SettingTable?.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row == 1{
            self.openLink("https://icons8.com")
            // SubViewController へ遷移するために Segue を呼び出す
        }else if indexPath.row == 3{
            self.openLink("http://www.flickr.com/photos/luxtonnerre/2765151618/")
        }else if indexPath.row == 4{
            self.openLink("https://www.flickr.com/photos/tallkev/1062784694/")
        }else if indexPath.row == 5{
            self.openLink("https://www.flickr.com/photos/twiggy_34/632316685")
        }else if indexPath.row == 6{
            self.openLink("https://www.flickr.com/photos/jareed/9552506358/")
        }else if indexPath.row == 7{
            self.openLink("https://www.flickr.com/photos/katsuma/3794686199/")
        }else if indexPath.row == 8{
            self.openLink("https://www.flickr.com/photos/inucara/4899181915/")
        }else if indexPath.row == 9{
            self.openLink("https://www.flickr.com/photos/acotie/23696903759/")
        }
//        else if indexPath.row == 9{
//            self.openLink("http://www.rollingstone.com/music/lists/100-greatest-artists-of-all-time-19691231")
//        }
    }

    
//    func showConfirm(title:String, openMes:String, cancelMes:String){
//        if objc_getClass("UIAlertController") != nil {
//            let alertController = UIAlertController(title: "", message: title, preferredStyle: .ActionSheet)
//            
//            let openAction = UIAlertAction(title: openMes, style: .Default) {
//                action in self.openLink("https://icons8.com")
//            }
//            let cancelAction = UIAlertAction(title: cancelMes, style: .Cancel, handler: nil)
//            
//            alertController.addAction(openAction)
//            alertController.addAction(cancelAction)
//            presentViewController(alertController, animated: true, completion: nil)
//        }else{
//            let sheet: UIActionSheet = UIActionSheet()
//            sheet.title  = title
//            //            sheet.delegate = self
//            sheet.addButtonWithTitle(openMes)
//            sheet.addButtonWithTitle(cancelMes)
//            sheet.cancelButtonIndex = 1
//            sheet.showInView(self.view)
//        }
//    }
    
    @IBAction func closeView(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    func actionSheet(_ sheet: UIActionSheet, clickedButtonAt buttonIndex: Int) {
        switch (buttonIndex) {
        case 0:
            self.openLink("https://icons8.com")
            break;
        default:
            break
        }
    }
    
    func openLink(_ url:String){
        if #available(iOS 9.0, *) {
            let _brow = SFSafariViewController(url: URL(string: url)!, entersReaderIfAvailable: false)
            _brow.delegate = self
            _brow.view.tintColor = UIColor(hex: "#222831")
            present(_brow, animated: true, completion: nil)
        } else {
            self.url = url
            // Fallback on earlier versions
            performSegue(withIdentifier: "GoOutPageSegueFromLic", sender: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "GoOutPageSegueFromLic") {
            let nav: UINavigationController  = segue.destination as! UINavigationController
            let nextVC:OutPageViewController = nav.visibleViewController as! OutPageViewController
            nextVC.url = URL(string: self.url)
        }
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        let orientation: UIInterfaceOrientationMask = [UIInterfaceOrientationMask.portrait]
        return orientation
    }
    
    //指定方向に自動的に変更するか？
    override var shouldAutorotate : Bool{
        return true
    }

}
