//
//  SubViewController.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2015/10/15.
//  Copyright © 2015年 大口 尚紀. All rights reserved.
//

import UIKit
import SafariServices
import GoogleMobileAds

class SubViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, SFSafariViewControllerDelegate, GADBannerViewDelegate {
    let subCateogryCollection = SubCategoryManager.sharedInstance
    var myCollectionView : UICollectionView!
    var cateogryId:Int!
    var naviTitle:String!
    var subCategory: Array<SubCategory> = []
    var navHeight:CGFloat = 0.0
    var selSubCategoryId: Int!
    var selSubCategoryName: String!
    var getView = false
    var margin = 3
    var admobViewArr: Array<GADBannerView> = []

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(SubViewController.onOrientationChange(_:)), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        self.navigationItem.hidesBackButton = true
        
        DispatchQueue.main.async {
            SVProgressHUD.show(withStatus: "Loading...", maskType: SVProgressHUDMaskType.black)
        }
        
        let backButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem = backButtonItem

        let callbackCategory = { () -> Void in
            self.subCategory = self.subCateogryCollection.subCategories

            let imageWidth = self.view.bounds.width
            let imageHeight = self.view.bounds.width/1.414
//            
//            for _ in 0...(self.subCategory.count/3) {
//                let admobView = GADBannerView(adSize:kGADAdSizeMediumRectangle)
//                admobView.adUnitID = "ca-app-pub-8256083710740545/6441108916"
//                admobView.delegate = self
//                admobView.rootViewController = self
//                admobView.tag = 99999
//            
//                let admobRequest:GADRequest = GADRequest()
//            
//                if AdMobTest {
//                    if SimulatorTest {
//                        admobRequest.testDevices = [kGADSimulatorID]
//                    }
//                    else {
//                        admobRequest.testDevices = [TEST_DEVICE_ID]
//                    }
//                }
//                admobView.loadRequest(admobRequest)
//
//                if(imageHeight > 250){
//                    admobView.frame.size = CGSizeMake(300, 250)
//                }else{
//                    let addW = 300*imageHeight/250
//                    admobView.frame.size = CGSizeMake(addW, imageHeight)
//                }
//                //admobView.frame.size = CGSizeMake(self.view.frame.width, admobHeight)
//                self.admobViewArr.append(admobView)
//            }
            
            
            // CollectionViewのレイアウトを生成.
            let layout = UICollectionViewFlowLayout()
            
            
            // Cell一つ一つの大きさ.
            layout.itemSize = CGSize(width: imageWidth, height: imageHeight-5)
            
            // ステータスバーの高さを取得
            let statusBarHeight: CGFloat = UIApplication.shared.statusBarFrame.height
            // ナビゲーションバーの高さを取得
            let navBarHeight = self.navigationController?.navigationBar.frame.size.height
            
            let tabHeight = self.tabBarController!.tabBar.frame.size.height
            self.getView = true
            // Cellのマージン.
            layout.sectionInset = UIEdgeInsetsMake(0, 0, statusBarHeight + navBarHeight! + tabHeight + addHeight,0)
            //アイテム同士のマージン
            layout.minimumInteritemSpacing = 0.0
            //セクションとアイテムのマージン
            layout.minimumLineSpacing = 0.0
            // セクション毎のヘッダーサイズ.
            layout.headerReferenceSize = CGSize(width: 0,height: 0)
            
            // CollectionViewを生成.
            self.myCollectionView = UICollectionView(frame: CGRect(x: 0, y: UIApplication.shared.statusBarFrame.height+self.navHeight,width: self.view.bounds.width,height: self.view.bounds.height), collectionViewLayout: layout)
            
            // Cellに使われるクラスを登録.
            for i in 0...(self.subCategory.count + (self.subCategory.count/self.margin)-1) {
                if(i%(self.margin+1) == self.margin){
                    self.myCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "MyCell" + String(i))
                }else{
                    self.myCollectionView.register(CustomUICollectionViewSubCell.self, forCellWithReuseIdentifier: "MyCell" + String(i))
                }
            }
            
            self.myCollectionView.delegate = self
            self.myCollectionView.dataSource = self
            self.myCollectionView.setContentOffset(CGPoint(x: 0, y: self.myCollectionView.contentSize.height - self.myCollectionView.frame.size.height+20), animated: false)
            let colorKey = UIColor(hex: "#EEEEEE")
            
            self.myCollectionView.backgroundColor = colorKey
            self.view.addSubview(self.myCollectionView)
            SVProgressHUD.dismiss()
            
            let noRate = UserDefaults.standard.bool(forKey: "noRate")
            if(noRate != true){
                let noLaunch = UserDefaults.standard.integer(forKey: "noLaunch")
                if(noLaunch == 20 || noLaunch == 40 || noLaunch == 60 || noLaunch == 80 || noLaunch%50 == 0){
                let osVersion = UIDevice.current.systemVersion
                if osVersion > "8.0" {
                    let alertController:MyUIAlertController = MyUIAlertController(title: NSLocalizedString("Rateourapp",comment: ""), message: NSLocalizedString("Pleaserateourapp",comment: ""), preferredStyle: .alert)
                        let nowAction = UIAlertAction(title: NSLocalizedString("Rateapp",comment: ""), style: .default) {
                            action in self.openReview()
                        }
                
                        let laterAction = UIAlertAction(title: NSLocalizedString("Notrateapp",comment: ""), style: .default) {
                            action in UserDefaults.standard.set(true, forKey: "noRate")
                        }
                
                        let cancelAction = UIAlertAction(title: NSLocalizedString("RemindLater",comment: ""), style: .cancel) {
                            action in
                        }
                
                        alertController.addAction(nowAction)
                        alertController.addAction(laterAction)
                        alertController.addAction(cancelAction)
                
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
            }

        }
        
        self.subCateogryCollection.getSubCategories(callbackCategory, categoryId: cateogryId, state:[1,2]);
    }

    func openReview(){
        UserDefaults.standard.set(true, forKey: "noRate")
        let itunesURL:String = "itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=1099070037"
        let url = URL(string:itunesURL)
        let app:UIApplication = UIApplication.shared
        app.openURL(url!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let delayTime = DispatchTime.now() + Double(Int64(2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        }
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
    }
    
    /*
    Cellが選択された際に呼び出される
    */
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let subRow = indexPath.row - (indexPath.row/(self.margin+1))
        if(self.subCategory[subRow].state == 2){
            var name = self.subCategory[subRow].name_sub
            let lanArr = Locale.preferredLanguages[0].components(separatedBy: "-")
            if(Locale.preferredLanguages[0] == "ja-JP" || lanArr.first == "ja"){
                name = self.subCategory[subRow].name
            }
            self.selSubCategoryId = self.subCategory[subRow].sub_ctegory_id
            self.selSubCategoryName = name
            performSegue(withIdentifier: "ViewControllerSegue", sender: nil)
        }
    }
    
    /*
    Cellの総数を返す
    */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.subCategory.count + (self.subCategory.count/margin)
    }
    
    /*
    Cellに値を設定する
    */
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let subRow = indexPath.row - (indexPath.row/(self.margin+1))
        if(indexPath.row%(self.margin+1) == self.margin){
            let addCell:UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCell" + String(indexPath.row), for: indexPath)
            if(addCell.contentView.viewWithTag(99999) == nil){
//                let index = indexPath.row/(self.margin+1)
//                let admobView = admobViewArr[index]
                let admobView = GADBannerView(adSize:kGADAdSizeMediumRectangle)
                admobView.adUnitID = "ca-app-pub-8256083710740545/6441108916"
                admobView.delegate = self
                admobView.rootViewController = self
                admobView.tag = 99999
                
                let imageWidth = self.view.bounds.width
                let imageHeight = self.view.bounds.width/1.414

                let admobRequest:GADRequest = GADRequest()
                
                if AdMobTest {
                    if SimulatorTest {
                        admobRequest.testDevices = [kGADSimulatorID]
                    }
                    else {
                        admobRequest.testDevices = [TEST_DEVICE_ID]
                    }
                }
                admobView.load(admobRequest)
                
                if(imageHeight > 250){
                    admobView.frame.size = CGSize(width: 300, height: 250)
                }else{
                    let addW = 300*imageHeight/250
                    admobView.frame.size = CGSize(width: addW, height: imageHeight)
                }

                admobView.layer.position = CGPoint(x: addCell.contentView.center.x, y:addCell.contentView.center.y)
                addCell.contentView.addSubview(admobView)
                
                let backTextLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.width/1.414))
                backTextLabel.textColor = UIColor.black
                backTextLabel.backgroundColor? =  UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha:0.10)
                addCell.contentView.addSubview(backTextLabel)
                
                let typeLabel = UILabel(frame: CGRect(x: 7, y: 10, width: 65, height: 28))
                typeLabel.textColor = UIColor.white
                typeLabel.numberOfLines = 1
                //単語の途中で改行されないようにする
                typeLabel.font = UIFont.boldSystemFont(ofSize: 18)
                
                typeLabel.layer.cornerRadius = 5
                typeLabel.clipsToBounds = true
                // サイズを自動調整
                //        textLabel.sizeToFit()
                // 文字を詰めて改行する
                typeLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
                typeLabel.textAlignment = NSTextAlignment.center
                
                typeLabel.text = "Ads"
                typeLabel.textColor = UIColor.white
                typeLabel.backgroundColor = UIColor(red: 238.0/255.0, green: 238.0/255.0, blue: 238.0/255.0, alpha:0.50)
                addCell.contentView.addSubview(typeLabel)

            }
            
            return addCell
        }else{
        let cell : CustomUICollectionViewSubCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCell" + String(indexPath.row), for: indexPath) as! CustomUICollectionViewSubCell
        if(cell.textLabel.attributedText == nil){
            if(self.subCategory[subRow].state == 1){
                cell.imageView.alpha = 0.5
                cell.textLabel.alpha = 0.5
                cell.licenceLabel.alpha = 0.5
                
                cell.readyLabel.text =  NSLocalizedString("UnderConstruction",comment: "")
                cell.readyLabel.textColor = UIColor.white
                cell.readyLabel.font = UIFont.systemFont(ofSize: 28)
                cell.readyLabel.alpha = 1.0
                cell.readyLabel.textAlignment = NSTextAlignment.center
                
                cell.backTextLabel.textColor = UIColor.white
                cell.backTextLabel.backgroundColor? =  UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha:0.5)
            }
            loadImage(strageUrl + self.subCategory[subRow].image, recForImageView: cell.imageView)
            
            var categoryName = NSMutableAttributedString(string: self.subCategory[subRow].name_sub)

            let lanArr = Locale.preferredLanguages[0].components(separatedBy: "-")
            if(Locale.preferredLanguages[0] == "ja-JP" || lanArr.first == "ja"){
                categoryName = NSMutableAttributedString(string: self.subCategory[subRow].name)
            }
            cell.textLabel.attributedText = categoryName
            cell.layer.masksToBounds = false
            
            if((self.subCategory[subRow].type) != nil){
                if self.subCategory[subRow].type == 1 {
                    cell.typeLabel.text = NSLocalizedString("basic",comment: "")
                    cell.typeLabel.backgroundColor = UIColor(red: 238.0/255.0, green: 238.0/255.0, blue: 238.0/255.0, alpha:0.50)
                    cell.typeLabel.textColor = UIColor(hex: "#222831")
                }else if self.subCategory[subRow].type == 2 {
                    cell.typeLabel.text = NSLocalizedString("new",comment: "")
                    cell.typeLabel.backgroundColor = UIColor(red: 238.0/255.0, green: 238.0/255.0, blue: 238.0/255.0, alpha:0.50)
                }
            }

            if(self.subCategory[subRow].licence != nil){
                let licence = NSMutableAttributedString(string: self.subCategory[subRow].licence)
                cell.licenceLabel.attributedText = licence
                if let text = cell.textLabel.text {
                    let line:CGFloat = CGFloat(2-self.lineNumber(cell.textLabel, text: text))
                    cell.licenceLabel.layer.position.y = cell.licenceLabel.layer.position.y-(line*13)
                }
                cell.licenceLabel.tag = 100001
            }
        }
        return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        let retSixe = CGSize(width: self.view.bounds.width, height: self.view.bounds.width/1.414)
//        if(indexPath.row%(self.margin+1) == self.margin){
//            print(self.view.bounds.width)
//            print(self.view.bounds.width/1.414)
//            print(self.view.bounds.width*250/300)
//            retSixe = CGSize(width: self.view.bounds.width, height: (self.view.bounds.width*250/300)-1)
//        }
        return retSixe
    }
    
    func lineNumber(_ label: UILabel, text: String) -> Int {
        let oneLineRect  =  "a".boundingRect(with: label.bounds.size, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: label.font], context: nil)
        let boundingRect = text.boundingRect(with: label.bounds.size, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: label.font], context: nil)
        
        return Int(boundingRect.height / oneLineRect.height)
    }


    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let nextVC: ViewController = segue.destination as! ViewController
        nextVC.subCategoryId = self.selSubCategoryId
        nextVC.subCategoryName = self.selSubCategoryName
        nextVC.tabHeight = self.tabBarController!.tabBar.frame.size.height
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // 端末の向きがかわったらNotificationを呼ばす設定.
        
                if let nv = navigationController {
            // 表示
            nv.setNavigationBarHidden(false, animated: true)
            // タイトルを設定する.
            self.navigationItem.title = naviTitle
            
            navHeight = (self.navigationController?.navigationBar.frame.size.height)!
        }
    }
    
    func onOrientationChange(_ notification: Foundation.Notification){
        for view in self.tabBarController!.view.subviews {
            if(view.tag == 123456789){
                if(self.tabBarController!.tabBar.isHidden){
                    view.frame.origin = CGPoint(x: 0, y: self.view.frame.height-view.frame.height-(self.tabBarController!.tabBar.frame.size.height+16)+1)
                }else{
                    view.frame.origin = CGPoint(x: 0, y: self.view.frame.height-view.frame.height-(self.tabBarController!.tabBar.frame.size.height)+1)
                }
            }
        }
        
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        let orientation: UIInterfaceOrientationMask = [UIInterfaceOrientationMask.portrait]
        return orientation
    }
    
    //指定方向に自動的に変更するか？
    override var shouldAutorotate : Bool{
        return true
    }
    
    override func viewWillDisappear(_ animated: Bool) {

    }
    
    func loadImage(_ url:String, recForImageView: UIImageView) {
        // load image
        let image_url:String = url
        let forImageView = recForImageView
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async {
            let url:URL = URL(string:image_url)!
            
            var placeholder = UIImage(named: "noimage.png")! as UIImage
            if((try? Data(contentsOf: url)) != nil){
                let data:Data = try! Data(contentsOf: url)
                placeholder = UIImage(data: data)!
            }

            let categoryImage = ImageTool.cropThumbnailImage(placeholder, w: Int(self.view.bounds.width), h:Int(self.view.bounds.width/1.414+2))
            let ciImage:CIImage = CIImage(image:categoryImage)!
            let ciFilter:CIFilter = CIFilter(name: "CIGammaAdjust")!
            ciFilter.setValue(ciImage, forKey: kCIInputImageKey)
            ciFilter.setValue(1.3, forKey: "inputPower")
            let ciContext:CIContext = ObjcUtility.cicontext(options: nil)
            let cgimg:CGImage = ciContext.createCGImage(ciFilter.outputImage!, from:ciFilter.outputImage!.extent)!
            
            //image2に加工後のUIImage
            let newImage:UIImage = UIImage(cgImage: cgimg, scale: 1.0, orientation:UIImageOrientation.up)
            
            // update ui
            DispatchQueue.main.async {
                forImageView.image = newImage
                forImageView.fadeIn(0.15)
            }
        }
    }
}
