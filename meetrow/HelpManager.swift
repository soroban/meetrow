//
//  HelpManager.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2016/03/06.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import Foundation

//
//  HelpManager.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2016/03/05.
//  Copyright © 2016年 大口 尚紀. All rights reserved.
//

import Foundation

class HelpManager: NSObject {
    
    var helps =  Dictionary<Int, Array<Help>>()
    static let sharedInstance = HelpManager()
    
    var myRootRef: Firebase!
    
    func getList(_ callback: @escaping () -> Void) {
        self.helps = [:]
        // Firebaseへ接続
        self.myRootRef = Firebase(url: backendUrl + "help")
        self.myRootRef.queryOrdered(byChild: "help_id").observeSingleEvent(of: FEventType.value, with: { helps in
            for help in helps?.children.allObjects as! [FDataSnapshot] {
                let value = help.value as? NSDictionary
                if(value != nil ){
                if let title = value!["title"] as? String,
                    let title_en = value!["title_en"] as? String,
                    let message = value!["message"] as? String,
                    let message_en = value!["message_en"] as? String,
                    let help_id = value!["help_id"] as? Int,
                    let help_sub_id = value!["help_sub_id"] as? Int
                {
                    let help = Help(title: title, title_en: title_en, message: message, message_en: message_en, help_id: help_id, help_sub_id:help_sub_id)
                    var tempRes:Array<Help> = []
                    if(self.helps[help_id] != nil) {
                        tempRes = self.helps[help_id]!
                    }
                    tempRes.append(help)
                    self.helps[help_id] = tempRes
                }
                }
            }
            callback()
        })
    }
}
