//
//  ViewController.swift
//  meetrow
//
//  Created by 大口 尚紀 on 2015/09/01.
//  Copyright (c) 2015年 大口 尚紀. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation
import GoogleMobileAds

protocol ViewControllerDelegate{
    func didFinished(_ next: Int)
    func backFromMenu(_ selectedCells:Array<Bool>)
    func transBackFromMenu(_ id:Int)
    //func loginComp()
}


class ViewController: UIViewController , UIScrollViewDelegate, UIViewControllerTransitioningDelegate, ViewControllerDelegate, UIAlertViewDelegate, GADBannerViewDelegate {
    @IBOutlet var myScrollView: UIScrollView!
    let ref = Firebase(url: backendUrl)
    let lineCollection = LineManager.sharedInstance
    let stationCollection = StationManager.sharedInstance
    var favoriteCollection = FavoriteManager.sharedInstance
    let subCateogryCollection = SubCategoryManager.sharedInstance
    let searchCollection = SearchManager.sharedInstance
    
    //var myView:customView!
    var myView:UIView!
    var filtered: Array<Int> = []
    var line: Dictionary<String, Array<Line>> = [:]
    var station: Array<Station> = []
    var favorite: Array<Favorite> = []
    var search: Array<Search> = []
    var subCategory:SubCategory!
    var frameH:CGFloat!
    var frameW:CGFloat!
    var screenWidth:CGFloat!
    var screenHeight:CGFloat!
    var mySacle:CGFloat!
    var tabHeight:CGFloat!
    var minimumZoomScale:CGFloat!
    var defaultZoomScale:CGFloat!
    var maximumZoomScale:CGFloat!
    var labelHeight:CGFloat!
    var crevice:Double!
    var detailId:Int = 0
    var detailName:String = ""
    var navHeight:CGFloat = 0.0
    var subCategoryId:Int!
    var subCategoryName:String!
    var menuButton: UIButton!

    var byDay:Int!
    let interval:Int = 50
    var name_font_size:CGFloat = 60
    //駅の丸の大きさ
    var rounded_size:CGFloat = 110
    let offset:CGFloat = 60
    var lineWidth:CGFloat = 10
    var labelWidth:CGFloat = 220
    var lineWidthLine:CGFloat = 22
    var shift:CGFloat=2000
    var tapPt = 0
    var dayArr:Array<String>=[]
    var ticket:Array<Dictionary<String, Any>>=[]
    var lrAdd:Dictionary<String, Any>=[:]
    var staByDay:[[Int]]!
    var selectedCells:Array<Bool>=[]
    var tagArr: Dictionary<Int, Bool> = [:]
    var nodispArr: Dictionary<Int, Bool> = [:]
    var recAdmobView:GADBannerView!
    var nexMenu: MenuViewController!
    var subCategoryLabel: SubCaLabel!
    var viewHeight: CGFloat!
    var viewWidth: CGFloat!
    var statusHeight: CGFloat!
    var lineRadius: CGFloat = 50
    
    @IBOutlet weak var likeButton: UIBarButtonItem!
    
    var lineMat: Dictionary<String, Dictionary<String, Int>> = [:]

    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.statusHeight = UIApplication.shared.statusBarFrame.height
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.onOrientationChange(_:)), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        
        self.viewHeight = self.view.frame.height;
        self.viewWidth = self.view.frame.width;
        
        self.tabHeight = self.tabHeight+16
        DispatchQueue.main.async {
            SVProgressHUD.show(withStatus: "Loading...", maskType: SVProgressHUDMaskType.black)
        }
        frameH = 55
        frameW = 89
        mySacle = 200
        minimumZoomScale = 0.105
        defaultZoomScale = 0.21
        maximumZoomScale = 0.42
        screenWidth = self.view.bounds.width
        screenHeight = self.view.bounds.height
        self.myScrollView.delegate = self
        self.myScrollView.minimumZoomScale = minimumZoomScale
        self.myScrollView.isScrollEnabled = true
        self.myScrollView.showsVerticalScrollIndicator = false
        self.myScrollView.showsHorizontalScrollIndicator = false
        self.myView = UIView(frame:CGRect(x: 0, y: 0, width: frameW*(mySacle*0.72), height: frameH*mySacle))
        self.myView.tag = 99999
        
        self.myScrollView.minimumZoomScale = self.minimumZoomScale
        self.myScrollView.setZoomScale(self.defaultZoomScale, animated: true)

        self.myScrollView.center = CGPoint(x: self.view.bounds.width / 2, y: self.view.bounds.height / 2);
        
        self.myScrollView.isUserInteractionEnabled = true
        //self.view.addSubview(self.myScrollView)
        var compLine = false
        var compStation = false
        var compSub = false
        var compSearch = false
        var complete = false
        
        self.recAdmobView = GADBannerView()
        self.recAdmobView = GADBannerView(adSize:kGADAdSizeMediumRectangle)
        self.recAdmobView.adUnitID = "ca-app-pub-8256083710740545/1871308519"
        self.recAdmobView.delegate = self
        self.recAdmobView.rootViewController = self
        
        let recAdmobRequest:GADRequest = GADRequest()
        
        if AdMobTest {
            if SimulatorTest {
                recAdmobRequest.testDevices = [kGADSimulatorID]
            }
            else {
                recAdmobRequest.testDevices = [TEST_DEVICE_ID]
            }
            
        }
        self.recAdmobView.load(recAdmobRequest)
        
        let shareButton: UIButton = UIButton(type: UIButtonType.custom)
        shareButton.setImage(UIImage(named: "share.png"), for: UIControlState())
        shareButton.frame = CGRect(x: 0, y: 0,width: 30, height: 30)
        shareButton.addTarget(self, action: #selector(ViewController.tapShareButton), for: UIControlEvents.touchUpInside)
        let shareBarButton = UIBarButtonItem(customView: shareButton)
        self.navigationItem.rightBarButtonItems = [shareBarButton]
        
        
        let callbackSubcategory = { () -> Void in
            let subCategory = self.subCateogryCollection.subCategories
            let lanArr = Locale.preferredLanguages[0].components(separatedBy: "-")
            for sub in subCategory {
                if(Locale.preferredLanguages[0] == "ja-JP" || lanArr.first == "ja"){
                    self.subCategoryName = sub.name
                }else{
                    self.subCategoryName = sub.name_sub
                }
                self.subCategory = sub
                if(sub.day != nil)
                {
                    self.staByDay = [[Int]]()
                    var key = 0;
                    for day in sub.day{
                        self.dayArr.append(day)
                        self.selectedCells.append(true)
                        self.staByDay.append([Int]())
                        
                        if(key == 0){
                            let formatter = DateFormatter()
                            formatter.dateFormat = "yyyy/MM/dd"
                            
                            let date = Date()
                            let calendar = Calendar.current
                            
                            let yearComp = calendar.component(.year, from: date)
                            let monthComp = calendar.component(.month, from: date)
                            let dayComp = calendar.component(.day, from: date)
                            
                            let startStr = "\(yearComp)/\(monthComp)/\(dayComp)";
                            if let startDate = formatter.date(from: startStr){
                                if let date = formatter.date(from: day) {
                                    self.byDay = Int(date.timeIntervalSince(startDate)/60/60/24)
                                }
                            }
                        }
                        key += 1
                    }
                    
                    if(self.byDay < 0){
                        let formatter = DateFormatter()
                        formatter.dateFormat = "yyyy/MM/dd"
                        
                        let date = Date()
                        let calendar = Calendar.current
                        
                        let yearComp = calendar.component(.year, from: date)
                        let monthComp = calendar.component(.month, from: date)
                        let dayComp = calendar.component(.day, from: date)
                        
                        let startStr = "\(yearComp)/\(monthComp)/\(dayComp)";
                        if let startDate = formatter.date(from: startStr){
                            if let date = formatter.date(from: sub.day[sub.day.count-1]) {
                                self.byDay = Int(date.timeIntervalSince(startDate)/60/60/24)
                            }
                        }
                        
                        if(self.byDay >= 0){
                           self.byDay = 0
                        }
                        
                    }
                    
                }
                
//                if(sub.ticket != nil)
//                {
//                    for ticket in sub.ticket{
//                        if(ticket["state"] as! Int == 1){
//                          self.ticket.append(ticket)
//                        }
//                    }
//                }
//                
//                if(sub.lrAdd != nil)
//                {
//                    var lrAdd = sub.lrAdd
//                    if(lrAdd?["state"] as! Int == 1){
//                       self.lrAdd = sub.lrAdd
//                    }
//                }
            }
            compSub = true;
            if(compStation == true && compLine == true && compSub == true && complete == false && compSearch == true){
                complete = true;
                SVProgressHUD.dismiss()
                self.dispMap()
            }
        }

        DispatchQueue.main.async {
            self.subCateogryCollection.getSubCategoriesFromIds(callbackSubcategory, Ids: [self.subCategoryId]);
        }

        
        let callbackLine = { () -> Void in
            self.line = self.lineCollection.lines
            compLine = true;
            if(compStation == true && compLine == true && compSub == true && complete == false && compSearch == true){
                complete = true;
                SVProgressHUD.dismiss()
                self.dispMap()
            }
        }
        
        DispatchQueue.main.async {
            self.lineCollection.getLines(callbackLine, subCategoryId: self.subCategoryId)
        }
        
        let callbackStation = { () -> Void in
            self.station = self.stationCollection.stations
            if(ipad){
                self.defaultZoomScale = 0.30
            }

            if(self.station.count <= 100){
                self.labelWidth = self.labelWidth * 1.63
                self.name_font_size = self.name_font_size*1.72
                self.rounded_size = self.rounded_size*1.6
                self.lineWidthLine = self.lineWidthLine * 1.71
                self.lineWidth = self.lineWidth * 1.6
                
                self.minimumZoomScale = 0.10 * (0.62)
                self.defaultZoomScale = 0.20 * (0.62)
                if(ipad){
                    self.defaultZoomScale = 0.18
                }

                self.maximumZoomScale = 0.40 * (0.62)
                self.shift = self.shift*1.6
                self.mySacle = self.mySacle*0.25
                self.myView.frame = CGRect(x: 0, y: 0, width: self.frameW*(self.mySacle*0.70), height: self.frameH*self.mySacle)
                
                self.myScrollView.minimumZoomScale = self.minimumZoomScale
                self.lineRadius = self.lineRadius * 1.2
            }
            self.myScrollView.setZoomScale(self.defaultZoomScale, animated: true)

            compStation = true;
            if(compStation == true && compLine == true && compSub == true && complete == false && compSearch == true){
                complete = true;
                SVProgressHUD.dismiss()
                self.dispMap()
            }
        }
        stationCollection.getStations(callback: callbackStation,subCategoryId: subCategoryId)

        let callbackSearch = { () -> Void in
            self.search = self.searchCollection.searches
            compSearch = true;
            
            if(compStation == true && compLine == true && compSub == true && complete == false && compSearch == true){
                complete = true;
                SVProgressHUD.dismiss()
                self.dispMap()
            }
        }
        searchCollection.getSearches(callbackSearch, subCategoryId: self.subCategoryId)
        

//        if(self.ref.authData != nil){
//            let callbackFavorite = { () -> Void in
//                self.favorite = sCollection.favorites
//                compFavorite = true;
//                for favorite in self.favorite{
//                    self.filtered = favorite.sub_ctegory_id.filter { $0 == self.subCategoryId }
//                }
//                
////                if(self.filtered.count > 0){
////                    self.filledLikeButton()
////                }else{
////                    self.normalLikeButton()
////                }
//                
//                if(compStation == true && compLine == true && compFavorite == true && compSub == true && complete == false && compSearch == true){
//                    complete = true;
//                    SVProgressHUD.dismiss()
//                    self.dispMap()
//                }
//            }
//            favoriteCollection.getFavorite(callbackFavorite, uid: self.ref.authData.uid)
//        }else{
//            self.normalLikeButton()
//            compFavorite = true;
//        }

        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        for view in self.tabBarController!.view.subviews {
            if(view.tag == 123456789){
                view.isHidden = true
                view.frame.origin = CGPoint(x: 0, y: self.viewHeight-view.frame.height-(self.tabHeight)+1)
            }
        }
        if let nv = navigationController {
            // 表示
            nv.setNavigationBarHidden(false, animated: true)
            // タイトルを設定する.
            navHeight = (self.navigationController?.navigationBar.frame.size.height)!
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        for view in self.tabBarController!.view.subviews {
            if(view.tag == 123456789){
                view.frame.origin = CGPoint(x: 0, y: self.viewHeight-view.frame.height-self.tabBarController!.tabBar.frame.size.height)
            }
        }

//                for station in self.station{
//                    print(1000000 + station.music_id)
//                    if(self.myView.viewWithTag(1000000 + station.music_id) != nil){
//                        let oldLabel: UILabel = self.myView.viewWithTag(1000000 + station.music_id) as! UILabel
//                        oldLabel.removeFromSuperview()
//                    }
//                }

    }
    
    override func didMove(toParentViewController parent: UIViewController?) {
    }
    
    func openMenu(){
        self.tapPt = 1
        
//        self.nexMenu.dayArr = self.dayArr
//        self.nexMenu.selectedCells = self.selectedCells
//        self.nexMenu.modalPresentationStyle = .Custom
//        self.nexMenu.transitioningDelegate = self
//        self.nexMenu.searches = self.search
//        self.nexMenu.delegate = self
        
        self.present(self.nexMenu, animated: true, completion: {
        })
    }
    
    func dispMap(){
        if(self.byDay != nil){
        if(self.byDay >= 0){
            var byDayStr = "\(self.byDay!) days to go!";
            var startNo = 0;
            var endNo = byDayStr.characters.count-12;
            
            if(self.byDay <= 1){
                byDayStr = "\(self.byDay!) day to go!";
                startNo = 0;
                endNo = byDayStr.characters.count-11;
            }

            let lanArr = Locale.preferredLanguages[0].components(separatedBy: "-")
            if(Locale.preferredLanguages[0] == "ja-JP" || lanArr.first == "ja"){
                byDayStr = "あと\(self.byDay!)日!";
                startNo = 2;
                endNo = byDayStr.characters.count-4;
            }
            let attrText = NSMutableAttributedString(string: byDayStr)
            attrText.addAttribute(NSForegroundColorAttributeName,
                                  value: UIColor(hex: "#ef8376"),
                                  range: NSMakeRange(startNo, endNo))
            
            attrText.addAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 55)], range: NSMakeRange(startNo, endNo))
            
            let byDayLabel = UILabel();
            byDayLabel.numberOfLines = 1
            byDayLabel.font = UIFont.systemFont(ofSize: 20)
            byDayLabel.adjustsFontSizeToFitWidth = true
            byDayLabel.minimumScaleFactor = 1
            byDayLabel.attributedText = attrText
            let maxSize: CGSize = CGSize(width: self.view.bounds.width,height: self.view.bounds.height)
            let maxWidth = byDayLabel.sizeThatFits(maxSize)

            byDayLabel.frame = CGRect(x:10, y: 10+self.navHeight+self.statusHeight, width: maxWidth.width, height: 50)
            byDayLabel.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha:0.8)
            self.view.addSubview(byDayLabel);
        }
        }

        self.nexMenu = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        self.nexMenu.menuRecBanner = self.recAdmobView
        self.nexMenu.menuRecBanner.frame = CGRect(x: 0.0, y:UIApplication.shared.statusBarFrame.height, width: 258,height: 208)
        self.nexMenu.dayArr = self.dayArr
        self.nexMenu.selectedCells = self.selectedCells
        self.nexMenu.modalPresentationStyle = .custom
        self.nexMenu.transitioningDelegate = self
        self.nexMenu.searches = self.search
        self.nexMenu.delegate = self
        self.nexMenu.view.addSubview(nexMenu.menuRecBanner)
        
        for view in self.tabBarController!.view.subviews {
            if(view.tag == 123456789){
                view.isHidden = false
            }
        }

        let imageWidth = self.viewWidth
        self.labelHeight = self.tabHeight
        
        self.subCategoryLabel = SubCaLabel(frame: CGRect(x: 0, y: 0, width: imageWidth!, height: self.labelHeight))
        self.subCategoryLabel.padding = UIEdgeInsets(top: 5, left: 75, bottom: 5, right: 10)
        self.subCategoryLabel.textColor = UIColor(hex: "#222831")
//        if(imageWidth < 375){
//            self.subCategoryLabel.font = UIFont.boldSystemFontOfSize(18)
//        }else{
            self.subCategoryLabel.font = UIFont.boldSystemFont(ofSize: 21)
//        }
        self.subCategoryLabel.numberOfLines = 2
        self.subCategoryLabel.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha:0.99)
        
        self.subCategoryLabel.layer.position = CGPoint(x: (self.viewWidth/2), y:self.viewHeight-(self.labelHeight/2))
        self.subCategoryLabel.text = self.subCategoryName
        self.subCategoryLabel.textAlignment = .center

        self.subCategoryLabel.adjustsFontSizeToFitWidth = true
        self.subCategoryLabel.minimumScaleFactor = 0.6
        
        self.view.addSubview(subCategoryLabel)
        
//        if (self.dayArr.count > 0){
            let image = UIImage(named: "menu.png")! as UIImage
            self.menuButton = UIButton()
            self.menuButton.tag = 4
            self.menuButton.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
            self.menuButton.layer.position = CGPoint(x: 40, y:self.viewHeight-(self.labelHeight/2)+1)
            self.menuButton.setImage(image, for: UIControlState())
            // 枠を丸くする.
            self.menuButton.layer.masksToBounds = true
            //self.menuButton.layer.cornerRadius = 30
            self.menuButton.backgroundColor = UIColor.white
            self.view.addSubview(self.menuButton)
            // イベントを追加する.
            self.menuButton.addTarget(self, action: #selector(self.openMenu), for: .touchUpInside)
//        }

        // Screen Size の取得
        let size = CGSize(width: screenWidth, height: screenHeight)
        crevice = 7

        // CoreGraphicsで描画する
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        
        for (color,lineArr) in self.line {
            //線路を描写
            let lineShapeLayer = CAShapeLayer()
            let myUIcolor = UIColor(hex: color)
            lineShapeLayer.strokeColor = myUIcolor.cgColor
            lineShapeLayer.fillColor = UIColor.clear.cgColor
            lineShapeLayer.lineWidth = lineWidthLine

            //let radius:CGFloat = 80;
            let cgPath:CGMutablePath = CGMutablePath()

            for i in 0 ..< lineArr.count-1 {
                let staM = lineArr[i].im
                let staL = lineArr[i].il
                
                if(i == 0 ){
//                    CGPathMoveToPoint(cgPath, nil, CGFloat(staM)+self.shift, CGFloat(staL)+self.shift)
                    cgPath.move(to: CGPoint(x:CGFloat(staM!)+self.shift, y:CGFloat(staL!)+self.shift))
                }
                else{
                    if(lineArr[i+1].direct != lineArr[i].direct){
                        let nextStaM = lineArr[i+1].im
                        let nextStaL = lineArr[i+1].il
                        let nextEndM = lineArr[i+1].jm
                        let nextEndL = lineArr[i+1].jl

                        //CGPathAddArcToPoint(cgPath, nil, CGFloat(nextStaM)+self.shift,CGFloat(nextStaL)+self.shift,CGFloat(nextEndM)+self.shift,CGFloat(nextEndL)+self.shift, self.lineRadius);
                        cgPath.addArc(tangent1End: CGPoint(x: CGFloat(nextStaM!)+self.shift,y:CGFloat(nextStaL!)+self.shift), tangent2End: CGPoint(x: CGFloat(nextEndM!)+self.shift,y:CGFloat(nextEndL!)+self.shift), radius: self.lineRadius)
                    }else if(lineArr[i-1].direct == lineArr[i].direct){
                        cgPath.addLine(to: CGPoint(x:CGFloat(staM!)+self.shift, y:CGFloat(staL!)+self.shift))
//                        CGPathAddLineToPoint(cgPath, nil, CGFloat(staM)+self.shift, CGFloat(staL)+self.shift)
                    }
                }
            }
            cgPath.addLine(to: CGPoint(x:CGFloat(lineArr[lineArr.count-1].jm)+self.shift,y:CGFloat(lineArr[lineArr.count-1].jl)+self.shift))
            //CGPathAddLineToPoint(cgPath, nil, CGFloat(lineArr[lineArr.count-1].jm)+self.shift, CGFloat(lineArr[lineArr.count-1].jl)+self.shift)
            
            let path = UIBezierPath(cgPath: cgPath)
            lineShapeLayer.path = path.cgPath
            // viewのlayerに描画したものをセットする
            self.myView.layer.addSublayer(lineShapeLayer)
        }
        
//駅を描写
        
        var startX:CGFloat = 0
        var startY:CGFloat = 0
        
        var dayCheck = false
        if(dayArr.count > 0){
           dayCheck = true
        }

        var checkM: Array<Int> = []
        var checkL: Array<Int> = []
        
        for station in self.station{
            if(station.music_id == self.subCategory.center_id){
                startX = (CGFloat(station.m)+self.shift+CGFloat(offset))*defaultZoomScale
                startY = (CGFloat(station.l)+self.shift)*defaultZoomScale
            }

            var labelX = CGFloat(station.m)+offset+self.shift
            var labelY = CGFloat(station.l)-(offset/2)+self.shift
            
            var a_90:Bool = true;
            var a_135:Bool = true;
            var a_180:Bool = true;
            var a_m135:Bool = true;
            var a_m90:Bool = true;
            var a_m45:Bool = true;
            var a_0:Bool = true;
            var a_45:Bool = true;
            
            let fullArr = station.full
                        
            // Labelを作成.
            let myLabel: UILabel = UILabel(frame: CGRect(x: 0,y: 0,width: labelWidth,height: labelWidth))

            let tempCount = Int((fullArr?.count)!)
            for i in 0 ..< tempCount {
                if(String(describing: fullArr?[i]) == "90"){
                    a_90 = false
                }
                else if(fullArr?[i] == "135"){
                    a_135 = false
                }
                else if(fullArr?[i] == "180"){
                    a_180 = false
                }
                else if(fullArr?[i] == "m135"){
                    a_m135 = false
                }
                else if(fullArr?[i] == "m90"){
                    a_m90 = false
                }
                else if(fullArr?[i] == "m45"){
                    a_m45 = false
                }
                else if(fullArr?[i] == "0"){
                    a_0 = false
                }
                else if(fullArr?[i] == "45"){
                    a_45 = false
                }
            }
            
            // Labelに文字を代入.
            myLabel.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha:0.5)
            let lanArr = Locale.preferredLanguages[0].components(separatedBy: "-")
            if((Locale.preferredLanguages[0] == "ja-JP" || lanArr.first == "ja") && station.jp == true){
                myLabel.text = station.jp_name
            }else{
                myLabel.text = station.en_name
            }
            myLabel.tag = 1000000 + station.music_id
            myLabel.font = UIFont.boldSystemFont(ofSize: name_font_size)
            myLabel.isUserInteractionEnabled = true;
            // Textを中央寄せにする.
            myLabel.textAlignment = NSTextAlignment.center
            myLabel.numberOfLines = 3
            // サイズを自動調整
            myLabel.sizeToFit()
            // 文字を詰めて改行する
            var x1:CGFloat = 45
            var x2:CGFloat = 45
            var y3:CGFloat = 30
            var y1:CGFloat = 0
            var lCornerRadius:CGFloat = 30
            if(self.station.count < 100){
                x1 = 40 * 2
                x2 = 20 * 2
                y3 = 30 * 2
                y1 = 10 * 2
                lCornerRadius = lCornerRadius*2
            }

            myLabel.lineBreakMode = NSLineBreakMode.byTruncatingTail
            myLabel.layer.cornerRadius = lCornerRadius
            myLabel.clipsToBounds = true
            myLabel.adjustsFontSizeToFitWidth = true
            myLabel.minimumScaleFactor = 0.75
            
            if(a_90 == true){
                let maxSize: CGSize = CGSize(width: self.view.bounds.width,height: self.view.bounds.height)
                let labelH = myLabel.sizeThatFits(maxSize)
                let labelX = CGFloat(station.m)+x1+(offset/2)
                let labelY = CGFloat(station.l) - (labelH.height/2)
                myLabel.frame.origin  = CGPoint(x: labelX+self.shift, y: labelY+self.shift)
            }else if(a_45 == true){
                labelX = CGFloat(station.m)+x2
                labelY = CGFloat(station.l)+offset/2+y1
                myLabel.frame.origin = CGPoint(x: labelX+self.shift, y: labelY+self.shift)
            }else if(a_135 == true){
                let maxSize: CGSize = CGSize(width: self.view.bounds.width,height: self.view.bounds.height)
                let labelH = myLabel.sizeThatFits(maxSize)
                labelX = CGFloat(station.m)+offset
                labelY = CGFloat(station.l)-(offset/2)-labelH.height+y3
                myLabel.frame.origin = CGPoint(x: labelX+self.shift, y: labelY+self.shift)
            }else if(a_180 == true){
                let maxSize: CGSize = CGSize(width: self.view.bounds.width,height: self.view.bounds.height)
                let labelH = myLabel.sizeThatFits(maxSize)
                labelX = CGFloat(station.m)//+(offset/2)-(labelH.width/2)
                labelY = CGFloat(station.l)-offset-(labelH.height)
                myLabel.layer.position  = CGPoint(x:labelX+self.shift, y:labelY+self.shift)
            }else if(a_0 == true){
                let maxSize: CGSize = CGSize(width: self.view.bounds.width,height: self.view.bounds.height)
                let labelH = myLabel.sizeThatFits(maxSize)
                labelX = CGFloat(station.m)//+(offset/2)-(labelH.width/2)
                labelY = CGFloat(station.l)+(offset)+(labelH.height)
                myLabel.layer.position  = CGPoint(x:labelX+self.shift, y:labelY+self.shift)
            }else{
                labelX = CGFloat(station.m)+offset+(labelWidth/2)
                labelY = CGFloat(station.l)-(offset/2)
                myLabel.layer.position  = CGPoint(x:labelX+self.shift, y:labelY+self.shift)
            }
            
            if(self.myView.viewWithTag(1000000 + station.music_id) != nil){
                let oldLabel: UILabel = self.myView.viewWithTag(1000000 + station.music_id) as! UILabel
                oldLabel.removeFromSuperview()
            }
            
            if(station.no_disp != nil){
                if(station.no_disp == true){
                    self.tagArr[myLabel.tag] = false
                    self.nodispArr[myLabel.tag] = true
                    myLabel.alpha = 0.3
                }
            }else{
                self.tagArr[myLabel.tag] = true
            }
            var baseKey = 0
            if(dayCheck){
                for baseDay in dayArr{
                    for staDay in station.day {
                        if(baseDay == staDay){
                            self.staByDay[baseKey].append(myLabel.tag)
                        }
                    }
                    baseKey += 1;
                }
            }
            
            // 配置する座標を設定する.
            let shapeLayer = CAShapeLayer()
            // 円のCALayer作成
            shapeLayer.strokeColor = UIColor.gray.cgColor
            shapeLayer.fillColor = UIColor.white.cgColor
            shapeLayer.lineWidth = lineWidth  // 輪郭の線の太さは1.0pt
            
            // 図形は円形
            let rectWidth:CGFloat = CGFloat(station.m)-CGFloat(rounded_size/2)+self.shift
            let rectHeight:CGFloat = CGFloat(station.l)-CGFloat((rounded_size)/2)+self.shift
            shapeLayer.path = UIBezierPath(roundedRect: CGRect(x: rectWidth,y: rectHeight, width: rounded_size, height: rounded_size), cornerRadius: rectWidth).cgPath
            shapeLayer.name = String(station.music_id)
            // 作成したCALayerを画面に追加
            // ViewにLabelを追加.
            self.myView.layer.addSublayer(shapeLayer)
            self.myView.addSubview(myLabel)
            let tap = UITapGestureRecognizer(target:self, action: #selector(ViewController.goDetail(_:)))
            tap.cancelsTouchesInView = false
            myLabel.addGestureRecognizer(tap)

            checkM.append(station.m/100)
            checkL.append(station.l/100)
        }

        for subview in self.myView.subviews {
            if (subview.tag > 1000000){
                self.myView.bringSubview(toFront: subview)
            }
        }
        
        UIGraphicsEndImageContext();
        // スクロールビューの設定
        self.myScrollView.addSubview(self.myView)
        self.myView.isUserInteractionEnabled = true
        let doubleTapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self
            , action:#selector(ViewController.doubleTap(_:)))
        doubleTapGesture.numberOfTapsRequired = 2
        self.myView.addGestureRecognizer(doubleTapGesture)
        
        let newContentOffsetX:CGFloat = CGFloat(startX+20)-(self.myScrollView.frame.size.width/2)
        let newContentOffsetY:CGFloat = CGFloat(startY+40)-(self.myScrollView.frame.size.height/2)
        self.myScrollView.contentOffset = CGPoint(x: newContentOffsetX,y: newContentOffsetY)
    }
    // ピンチイン・ピンチアウト
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.myView
    }
    
    // ダブルタップ
    func doubleTap(_ gesture: UITapGestureRecognizer) -> Void {
        if ( self.myScrollView.zoomScale < maximumZoomScale ) {
            let newScale:CGFloat = self.myScrollView.zoomScale*2
            let zoomRect:CGRect = self.zoomRectForScale(newScale, center: gesture.location(in: gesture.view))
            self.myScrollView.zoom(to: zoomRect, animated: true)
        } else {
            self.myScrollView.setZoomScale(minimumZoomScale, animated: true)
        }
        //        self.dispStation()
    }
    // 領域
    func zoomRectForScale(_ scale:CGFloat, center: CGPoint) -> CGRect{
        var zoomRect: CGRect = CGRect()
        zoomRect.size.height = self.myScrollView.frame.size.height / scale
        zoomRect.size.width = self.myScrollView.frame.size.width / scale
        
        zoomRect.origin.x = center.x - zoomRect.size.width / 2.0
        zoomRect.origin.y = center.y - zoomRect.size.height / 2.0
        
        return zoomRect
    }
    
     func removeAllSubviews(_ parentView: UIView){
        let subviews = parentView.subviews
        for subview in subviews {
            subview.removeFromSuperview()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if(segue.identifier == "LoginFromFaboriteSegue") {
//            let nextVC: LoginViewController = segue.destinationViewController as! LoginViewController
//            nextVC.viewDelegate = self
//        }
    }

    
    func goDetail(_ gesture: UIGestureRecognizer) {
        var layear = CAShapeLayer()
        if let theView = gesture.view {
            let views = self.myScrollView.subviews
            for subview in views {
                if subview.tag == 99999 {
                    let subs = subview.layer.sublayers
                    for sub in subs!{
                        if((sub.name) != nil){
                            if(theView.tag - 1000000 == Int(sub.name!)!){
                                layear = sub as! CAShapeLayer
                            }
                        }
                    }
                }
            }
            
            if((self.tagArr[theView.tag]) == true){
                let selLabel: UILabel = self.view.viewWithTag(theView.tag) as! UILabel
                layear.opacity = 0
                selLabel.textColor = UIColor.clear
        
                let newContentOffsetX:CGFloat = ((theView.frame.origin.x+200)*self.myScrollView.zoomScale)-(self.myScrollView.frame.size.width/2)
                let newContentOffsetY:CGFloat = ((theView.frame.origin.y+100)*self.myScrollView.zoomScale)-(self.myScrollView.frame.size.height/2)

                UIView.animate(withDuration: 0.15, delay: 0, options: UIViewAnimationOptions() ,
                                           animations: {
                                            self.myScrollView.contentOffset = CGPoint(x: newContentOffsetX,y: newContentOffsetY)
                    },
                completion:{ Void in
                    // 行番号を取得
                    self.detailId = theView.tag - 1000000
                    self.detailName = selLabel.text!
                    let nex : DetailViewController = self.storyboard!.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
                    nex.id = self.detailId
                    nex.subCategoryId = self.subCategoryId
                    nex.navHeight = self.navHeight
                    nex.modalPresentationStyle = .custom
                    nex.transitioningDelegate = self
                    nex.delegate = self
                    nex.tagArr = self.tagArr
                    self.tapPt = 0;
                    self.present(nex, animated: true, completion: {})
                    selLabel.textColor = UIColor.black
                    layear.opacity = 1.0
                }
                )
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // 端末の向きがかわったらNotificationを呼ばす設定.
    }

    func onOrientationChange(_ notification: Foundation.Notification){
        let deviceOrientation: UIDeviceOrientation!  = UIDevice.current.orientation
    
        if(self.tabBarController != nil){
            // 向きの判定.
            if UIDeviceOrientationIsLandscape(deviceOrientation) {
                if(self.subCategoryLabel != nil){
                    self.subCategoryLabel.isHidden = true
                }
                if(self.menuButton != nil){
                    self.menuButton.isHidden = true
                }

            
                for view in self.tabBarController!.view.subviews {
                    if(view.tag == 123456789){
                        view.layer.position = CGPoint(x: (self.view.frame.width/2), y:self.view.frame.height-(view.frame.height/2))
                    }
                }
            } else if UIDeviceOrientationIsPortrait(deviceOrientation){
                if(self.subCategoryLabel != nil){
                    self.subCategoryLabel.layer.position = CGPoint(x: (self.viewWidth/2), y:self.viewHeight-(self.labelHeight/2))
                    self.subCategoryLabel.isHidden = false
                }
                if(self.menuButton != nil){
                    self.menuButton.layer.position = CGPoint(x: 40, y:self.viewHeight-(self.labelHeight/2)+1)
                    self.menuButton.isHidden = false
                }
            
                for view in self.tabBarController!.view.subviews {
                    if(view.tag == 123456789){
                        view.frame.origin = CGPoint(x: 0, y: self.viewHeight-view.frame.height-(tabHeight)+1)
                    }
                }

                //縦向きの判定.
            }
            
        }
    }
    
    
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        
        if(tapPt == 0){
            return CustomPresentationController(presentedViewController: presented, presenting: presenting)
        }else{
            return CustomSidePresentationController(presentedViewController: presented, presenting: presenting)
        }
    }

    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        if (tapPt == 0){
            return CustomAnimatedTransitioning(isPresent: true)
        }else{
            return CustomSideAnimatedTransitioning(isPresent: true)
        }
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        if (tapPt == 0){
            return CustomAnimatedTransitioning(isPresent: false)
        }else{
            return CustomSideAnimatedTransitioning(isPresent: false)
        }
    }

    func transBackFromMenu(_ id:Int){
        let theView: UILabel = self.view.viewWithTag(id + 1000000) as! UILabel
        
        let newContentOffsetX:CGFloat = ((theView.frame.origin.x+200)*self.myScrollView.zoomScale)-(self.myScrollView.frame.size.width/2)
        let newContentOffsetY:CGFloat = ((theView.frame.origin.y+100)*self.myScrollView.zoomScale)-(self.myScrollView.frame.size.height/2)
        
        UIView.animate(withDuration: 0.25, delay: 0, options: UIViewAnimationOptions() ,
                                   animations: {
                                    self.myScrollView.contentOffset = CGPoint(x: newContentOffsetX,y: newContentOffsetY)
            },
                                   completion:{ Void in
        })
    }
    
    func backFromMenu(_ selectedCells:Array<Bool>){
        if (self.dayArr.count > 0){
        for (tagKey, _)in self.tagArr {
            self.tagArr[tagKey] = false;
        }
        
        var key = 0;
        for res in selectedCells{
            if(res){
                for tag in self.staByDay[key]{
                    self.tagArr[tag] = true
                }
            }
            key += 1
        }

        for (tagKey, _)in self.nodispArr {
            self.tagArr[tagKey] = false;
        }
        
        for (tagKey, tagRes)in self.tagArr {
            if(tagRes){
                let tempView = self.myView.viewWithTag(tagKey) as! UILabel
                tempView.alpha = 1.0
            }else{
                let tempView = self.myView.viewWithTag(tagKey) as! UILabel
                tempView.alpha = 0.3
            }
        }

        
        self.selectedCells = selectedCells
        }
    }
    
    func didFinished(_ next:Int){
        if(next != 999999){
            let theView = self.myView.viewWithTag(next + 1000000) as! UILabel
            var layear = CAShapeLayer()
            let subs = self.myView.layer.sublayers
            for sub in subs!{
                if((sub.name) != nil){
                    if(theView.tag - 1000000 == Int(sub.name!)!){
                        layear = sub as! CAShapeLayer
                    }
                }
            }

            let newContentOffsetX:CGFloat = ((theView.frame.origin.x+200)*self.myScrollView.zoomScale)-(self.myScrollView.frame.size.width/2)
            let newContentOffsetY:CGFloat = ((theView.frame.origin.y+100)*self.myScrollView.zoomScale)-(self.myScrollView.frame.size.height/2)

            theView.textColor = UIColor.clear
            layear.opacity = 0

            UIView.animate(withDuration: 0.25, delay: 0, options: UIViewAnimationOptions() ,
                animations: {
                    self.myScrollView.contentOffset = CGPoint(x: newContentOffsetX,y: newContentOffsetY)
                },
                completion:{ Void in
//                    // 行番号を取得
                    self.detailId = next
                    self.detailName = theView.text!
                    let nex : DetailViewController = self.storyboard!.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
                    nex.id = self.detailId
                    nex.subCategoryId = self.subCategoryId
                    nex.navHeight = self.navHeight
                    nex.modalPresentationStyle = .custom
                    nex.transitioningDelegate = self
                    nex.delegate = self
                    nex.tagArr = self.tagArr
                    theView.textColor = UIColor.black
                    layear.opacity = 1
                    self.tapPt=0
                    self.present(nex, animated: true, completion: {})
                }
            )
        }
    }
        
    func tapShareButton() {
        let shareText = subCategoryName + "のまとめ! " + NSLocalizedString("hashtag",comment: "")
        let shareWebsite = URL(string: "https://itunes.apple.com/jp/app/metoro-u-fesunomatome-rokkinjapan/id1099070037?mt=8")!
        let activityItems = [shareText, shareWebsite/*, shareImage*/] as [Any]
        
        // 初期化処理
        let activityVC = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = self.view
        
        // 使用しないアクティビティタイプ
        let excludedActivityTypes = [
            // テキスト復数は連結。
            // NSURLと画像が含まれている場合、画像投稿と見なされURLはテキストリンクで文中に
//            UIActivityTypePostToFacebook,
            // Twitter：画像は１枚のみ。テキスト復数は連結。URLは無視される。
//            UIActivityTypePostToTwitter,
            // weibo
//            UIActivityTypePostToWeibo,
            // デフォで入ってるメッセージ
//            UIActivityTypeMessage,
            // デフォで入ってるMail
//            UIActivityTypeMail,
            // テキストをクリップボードにコピー、画像はコピー出来ない
//            UIActivityTypeCopyToPasteboard,
            // アドレス帖が起動、選択した人のアバター登録、テキストのみの場合アイコン出ない
//            UIActivityTypeAssignToContact,
            // 画像がアルバムに保存される、テキストのみの場合アイコン出ない
//            UIActivityTypeSaveToCameraRoll,
            // Safariのリーディングリストに追加。NSURLが含まれている場合のみ
//            UIActivityTypeAddToReadingList,
            // Frickr：試してない
//            UIActivityTypePostToFlickr,
            // Vimeo：試してない
//            UIActivityTypePostToVimeo,
            // Weiboがやってるマイクロブログ
//            UIActivityTypePostToTencentWeibo,
            // Airdrop：URL,画像,テキスト。
            // 復数指定可能（txtx,txt,img または url, url, txt）でテキストの優先順位は低い。
            // 画像とURLが混在していると"cannot receive"となり送れない
//            UIActivityTypeAirDrop
        ] as NSArray
        
        //activityVC.excludedActivityTypes = excludedActivityTypes as! [UIActivityType]?
        
        // UIActivityViewControllerを表示
        self.present(activityVC, animated: true, completion: nil)
    }
    
    func lineNumber(_ label: UILabel, text: String) -> Int {
        let oneLineRect  =  "a".boundingRect(with: label.bounds.size, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: label.font], context: nil)
        let boundingRect = text.boundingRect(with: label.bounds.size, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: label.font], context: nil)
        
        return Int(boundingRect.height / oneLineRect.height)
    }

//    @IBAction func tapLikeButton(sender: AnyObject) {
//        if(self.ref.authData != nil){
//            self.filledLikeButton()
//            self.favoriteCollection.updateFavorite(self.ref.authData.uid, SubCategoryId: self.subCategoryId)
//        }else{
//            performSegueWithIdentifier("LoginFromFaboriteSegue", sender: nil)
//        }
//    }
}

extension UIScrollView {
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch: UITouch = touches.first!
        let views = self.subviews // Subviewsの配列を取得
        for subview in views {
            if subview.tag == 99999 {
                let point = touch.location(in: subview)
                let subs = subview.layer.sublayers
                for sub in subs!{
                    if((sub.name) != nil){
                        let layear = sub as! CAShapeLayer
                        if (layear.path?.contains(point))!{
                            let vc  = UIApplication.topViewController() as! ViewController
                            if((vc.tagArr[Int(sub.name!)! + 1000000]) == true){
                            
                            let theView = self.viewWithTag(Int(sub.name!)! + 1000000) as! UILabel
                            theView.textColor = UIColor.clear
                            layear.opacity = 0

                            //var locaton = gesture.locationInView(self.myScrollView)
                            let newContentOffsetX:CGFloat = ((theView.frame.origin.x+200)*self.zoomScale)-(self.frame.size.width/2)
                            let newContentOffsetY:CGFloat = ((theView.frame.origin.y+100)*self.zoomScale)-(self.frame.size.height/2)
                            UIView.animate(withDuration: 0.15, delay: 0, options: UIViewAnimationOptions() ,
                                animations: {
                                    self.contentOffset = CGPoint(x: newContentOffsetX,y: newContentOffsetY)
                                },
                                completion:{ Void in
                                    // 行番号を取得
                                    vc.detailId = Int(sub.name!)!
                                    let selLabel: UILabel = vc.view.viewWithTag(theView.tag) as! UILabel
                                    vc.detailName = selLabel.text!
                                    let nex : DetailViewController = vc.storyboard!.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
                                    nex.id = vc.detailId
                                    nex.delegate = vc.self
                                    nex.subCategoryId = vc.subCategoryId
                                    nex.navHeight = vc.navHeight
                                    nex.modalPresentationStyle = .custom
                                    nex.transitioningDelegate = vc
                                    theView.textColor = UIColor.black
                                    layear.opacity = 1
                                    vc.tapPt = 0
                                    nex.tagArr = vc.tagArr
                                    vc.present(nex, animated: true, completion: {})
                                }
                            )
                            }
                        }

                        
                    }
                }
            }
        }
    }
}

class SubCaLabel: UILabel {
    
    // paddingの値
    var padding = UIEdgeInsets(top: 5, left: 70, bottom: 5, right: 20)
    
    override func drawText(in rect: CGRect) {
        let newRect = UIEdgeInsetsInsetRect(rect, padding)
        super.drawText(in: newRect)
    }
    
    override var intrinsicContentSize : CGSize {
        var intrinsicContentSize = super.intrinsicContentSize
        intrinsicContentSize.height += padding.top + padding.bottom
        intrinsicContentSize.width += padding.left + padding.right
        return intrinsicContentSize
    }
}

